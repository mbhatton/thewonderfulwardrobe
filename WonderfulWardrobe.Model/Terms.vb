﻿Imports System.ComponentModel.DataAnnotations

Public Class Terms
    <Required> _
    Public Property TermId() As Integer
        Get
            Return m_TermId
        End Get
        Set(value As Integer)
            m_TermId = value
        End Set
    End Property
    Private m_TermId As Integer

    <Required> _
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String

    <Required> _
    Public Property DiscountAmount() As Decimal
        Get
            Return m_DiscountAmount
        End Get
        Set(value As Decimal)
            m_DiscountAmount = value
        End Set
    End Property
    Private m_DiscountAmount As Decimal


    <Required> _
    Public Property TotalAmount() As String
        Get
            Return m_TotalAmount
        End Get
        Set(value As String)
            m_TotalAmount = value
        End Set
    End Property
    Private m_TotalAmount As String

    <Required> _
    Public Property DiscountPercentage() As Decimal
        Get
            Return m_DiscountPercentage
        End Get
        Set(value As Decimal)
            m_DiscountPercentage = value
        End Set
    End Property
    Private m_DiscountPercentage As Decimal

    'Public Property StartDate() As DateTime
    '    Get
    '        Return m_StartDate
    '    End Get
    '    Set(value As DateTime)
    '        m_StartDate = value
    '    End Set
    'End Property
    'Private m_StartDate As DateTime
    'Public Property EndDate() As DateTime
    '    Get
    '        Return m_EndDate
    '    End Get
    '    Set(value As DateTime)
    '        m_EndDate = value
    '    End Set
    'End Property
    'Private m_EndDate As DateTime
    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean
End Class
