﻿Imports System.ComponentModel.DataAnnotations

Public Class Venue
    <Required> _
    Public Property VenueId() As Integer
        Get
            Return m_VenueId
        End Get
        Set(value As Integer)
            m_VenueId = value
        End Set
    End Property
    Private m_VenueId As Integer

    <Required> _
    Public Property Day() As String
        Get
            Return m_Day
        End Get
        Set(value As String)
            m_Day = value
        End Set
    End Property
    Private m_Day As String

    <Required> _
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String

    <Required> _
    Public Property Logo() As String
        Get
            Return m_Logo
        End Get
        Set(value As String)
            m_Logo = value
        End Set
    End Property
    Private m_Logo As String

    <Required> _
    Public Property Address1() As String
        Get
            Return m_Address1
        End Get
        Set(value As String)
            m_Address1 = value
        End Set
    End Property
    Private m_Address1 As String
    <Required> _
    Public Property Address2() As String
        Get
            Return m_Address2
        End Get
        Set(value As String)
            m_Address2 = value
        End Set
    End Property
    Private m_Address2 As String
    <Required> _
    Public Property Address3() As String
        Get
            Return m_Address3
        End Get
        Set(value As String)
            m_Address3 = value
        End Set
    End Property
    Private m_Address3 As String

    <Required> _
    Public Property Longitude() As String
        Get
            Return m_Longitude
        End Get
        Set(value As String)
            m_Longitude = value
        End Set
    End Property
    Private m_Longitude As String

    <Required> _
    Public Property Latitude() As String
        Get
            Return m_Latitude
        End Get
        Set(value As String)
            m_Latitude = value
        End Set
    End Property
    Private m_Latitude As String

    <Required> _
    Public Property City() As String
        Get
            Return m_City
        End Get
        Set(value As String)
            m_City = value
        End Set
    End Property
    Private m_City As String

    <Required> _
    Public Property PostalCode() As String
        Get
            Return m_PostalCode
        End Get
        Set(value As String)
            m_PostalCode = value
        End Set
    End Property
    Private m_PostalCode As String
    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean
End Class
