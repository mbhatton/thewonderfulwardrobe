﻿Imports System.ComponentModel.DataAnnotations

Public Class PartyEnquiry

    <Required> _
    Public Property EnquiryId() As Integer
        Get
            Return m_EnquiryId
        End Get
        Set(value As Integer)
            m_EnquiryId = value
        End Set
    End Property
    Private m_EnquiryId As Integer

    <Required> _
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String

    <Required> _
    Public Property Email() As String
        Get
            Return m_Email
        End Get
        Set(value As String)
            m_Email = value
        End Set
    End Property
    Private m_Email As String

    <Required> _
    Public Property PhoneNumber() As String
        Get
            Return m_PhoneNumber
        End Get
        Set(value As String)
            m_PhoneNumber = value
        End Set
    End Property
    Private m_PhoneNumber As String

    <Required> _
    Public Property PreferedDate1() As DateTime
        Get
            Return m_PreferedDate1
        End Get
        Set(value As DateTime)
            m_PreferedDate1 = value
        End Set
    End Property
    Private m_PreferedDate1 As DateTime
    <Required> _
    Public Property PreferedDate2() As DateTime
        Get
            Return m_PreferedDate2
        End Get
        Set(value As DateTime)
            m_PreferedDate2 = value
        End Set
    End Property
    Private m_PreferedDate2 As DateTime

    <Required> _
    Public Property PreferedStartTime() As String
        Get
            Return m_PreferedStartTime
        End Get
        Set(value As String)
            m_PreferedStartTime = value
        End Set
    End Property
    Private m_PreferedStartTime As String

    <Required> _
    Public Property Duration() As String
        Get
            Return m_Duration
        End Get
        Set(value As String)
            m_Duration = value
        End Set
    End Property
    Private m_Duration As String


    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

End Class
