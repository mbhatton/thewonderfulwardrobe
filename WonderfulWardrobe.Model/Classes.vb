﻿Imports System.ComponentModel.DataAnnotations

Public Class Classes

    <Required> _
    Public Property ClassId() As Integer
        Get
            Return m_ClassId
        End Get
        Set(value As Integer)
            m_ClassId = value
        End Set
    End Property
    Private m_ClassId As Integer


    <Required> _
    Public Property Duration() As String
        Get
            Return m_Duration
        End Get
        Set(value As String)
            m_Duration = value
        End Set
    End Property
    Private m_Duration As String

    <Required> _
    Public Property StartDateTime() As DateTime
        Get
            Return m_StartDateTime
        End Get
        Set(value As DateTime)
            m_StartDateTime = value
        End Set
    End Property
    Private m_StartDateTime As DateTime

    <Required> _
    Public Property Price() As Decimal
        Get
            Return m_Price
        End Get
        Set(value As Decimal)
            m_Price = value
        End Set
    End Property
    Private m_Price As Decimal

    <Required> _
    Public Property Capacity() As Integer
        Get
            Return m_Capacity
        End Get
        Set(value As Integer)
            m_Capacity = value
        End Set
    End Property
    Private m_Capacity As Integer

    <Required> _
    Public Property Weeks() As Integer
        Get
            Return m_Weeks
        End Get
        Set(value As Integer)
            m_Weeks = value
        End Set
    End Property
    Private m_Weeks As Integer

    <Required> _
    Public Property TermId() As Integer
        Get
            Return m_TermId
        End Get
        Set(value As Integer)
            m_TermId = value
        End Set
    End Property
    Private m_TermId As Integer

    <Required> _
    Public Property Term() As String
        Get
            Return m_Term
        End Get
        Set(value As String)
            m_Term = value
        End Set
    End Property
    Private m_Term As String

    <Required> _
    Public Property VenueId() As Integer
        Get
            Return m_VenueId
        End Get
        Set(value As Integer)
            m_VenueId = value
        End Set
    End Property
    Private m_VenueId As Integer

    <Required> _
    Public Property Venue() As String
        Get
            Return m_Venue
        End Get
        Set(value As String)
            m_Venue = value
        End Set
    End Property
    Private m_Venue As String

    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime

    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

End Class
