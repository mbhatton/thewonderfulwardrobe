﻿Imports System.ComponentModel.DataAnnotations

Public Class BookedClass

    <Required> _
    Public Property BookingId() As Integer
        Get
            Return m_BookingId
        End Get
        Set(value As Integer)
            m_BookingId = value
        End Set
    End Property
    Private m_BookingId As Integer
    <Required> _
    Public Property ChildId() As Integer
        Get
            Return m_ChildId
        End Get
        Set(value As Integer)
            m_ChildId = value
        End Set
    End Property
    Private m_ChildId As Integer
    <Required> _
    Public Property ClassId() As Integer
        Get
            Return m_ClassId
        End Get
        Set(value As Integer)
            m_ClassId = value
        End Set
    End Property
    Private m_ClassId As Integer
    <Required> _
    Public Property OrderId() As Integer
        Get
            Return m_OrderId
        End Get
        Set(value As Integer)
            m_OrderId = value
        End Set
    End Property
    Private m_OrderId As Integer

  
End Class
