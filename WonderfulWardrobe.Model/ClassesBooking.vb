﻿Imports System.ComponentModel.DataAnnotations

Public Class ClassesBooking
    <Required> _
    Public Property ClassId() As Integer

        Get
            Return m_ClassId

        End Get
        Set(value As Integer)
            m_ClassId = value
        End Set
    End Property
    Private m_ClassId As Integer

    <Required> _
    Public Property StartDateTime() As DateTime

        Get
            Return m_StartDateTime

        End Get
        Set(value As DateTime)
            m_StartDateTime = value
        End Set
    End Property
    Private m_StartDateTime As DateTime
    <Required> _
    Public Property Duration() As String

        Get
            Return m_Duration

        End Get
        Set(value As String)
            m_Duration = value
        End Set
    End Property
    Private m_Duration As String
    <Required> _
    Public Property Price() As Decimal

        Get
            Return m_Price

        End Get
        Set(value As Decimal)
            m_Price = value
        End Set
    End Property
    Private m_Price As Decimal

    <Required> _
    Public Property VenueId() As Integer

        Get
            Return m_VenueId

        End Get
        Set(value As Integer)
            m_VenueId = value
        End Set
    End Property
    Private m_VenueId As Integer

    <Required> _
    Public Property Day() As String

        Get
            Return m_Day

        End Get
        Set(value As String)
            m_Day = value
        End Set
    End Property
    Private m_Day As String

    <Required> _
    Public Property VenueName() As String

        Get
            Return m_VenueName

        End Get
        Set(value As String)
            m_VenueName = value
        End Set
    End Property
    Private m_VenueName As String

    <Required> _
    Public Property Logo() As String

        Get
            Return m_Logo

        End Get
        Set(value As String)
            m_Logo = value
        End Set
    End Property
    Private m_Logo As String

    <Required> _
    Public Property Address1() As String
        Get
            Return m_Address1
        End Get
        Set(value As String)
            m_Address1 = value
        End Set
    End Property
    Private m_Address1 As String
    <Required> _
    Public Property Address2() As String
        Get
            Return m_Address2
        End Get
        Set(value As String)
            m_Address2 = value
        End Set
    End Property
    Private m_Address2 As String
    <Required> _
    Public Property Address3() As String
        Get
            Return m_Address3
        End Get
        Set(value As String)
            m_Address3 = value
        End Set
    End Property
    Private m_Address3 As String

    <Required> _
    Public Property City() As String
        Get
            Return m_City
        End Get
        Set(value As String)
            m_City = value
        End Set
    End Property
    Private m_City As String

    <Required> _
    Public Property TermId() As Integer
        Get
            Return m_TermId
        End Get
        Set(value As Integer)
            m_TermId = value
        End Set
    End Property
    Private m_TermId As Integer

    <Required> _
    Public Property TermName() As String
        Get
            Return m_TermName
        End Get
        Set(value As String)
            m_TermName = value
        End Set
    End Property
    Private m_TermName As String

    <Required> _
    Public Property DiscountAmount() As Decimal
        Get
            Return m_DiscountAmount
        End Get
        Set(value As Decimal)
            m_DiscountAmount = value
        End Set
    End Property
    Private m_DiscountAmount As Decimal

End Class
