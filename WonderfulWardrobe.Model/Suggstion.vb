﻿Imports System.ComponentModel.DataAnnotations

Public Class Suggstion

    <Required> _
    Public Property EnquiryId() As Integer
        Get
            Return m_EnquiryId
        End Get
        Set(value As Integer)
            m_EnquiryId = value
        End Set
    End Property
    Private m_EnquiryId As Integer

    <Required> _
    Public Property SuggestedDate() As DateTime
        Get
            Return m_SuggestedDate
        End Get
        Set(value As DateTime)
            m_SuggestedDate = value
        End Set
    End Property
    Private m_SuggestedDate As DateTime

    <Required> _
    Public Property SuggestedTime() As String
        Get
            Return m_SuggestedTime
        End Get
        Set(value As String)
            m_SuggestedTime = value
        End Set
    End Property
    Private m_SuggestedTime As String

    <Required> _
    Public Property SuggestedDuration() As String
        Get
            Return m_SuggestedDuration
        End Get
        Set(value As String)
            m_SuggestedDuration = value
        End Set
    End Property
    Private m_SuggestedDuration As String

    <Required> _
    Public Property SuggestedPrice() As String
        Get
            Return m_SuggestedPrice
        End Get
        Set(value As String)
            m_SuggestedPrice = value
        End Set
    End Property
    Private m_SuggestedPrice As String

    <Required> _
    Public Property Price() As Decimal
        Get
            Return m_Price
        End Get
        Set(value As Decimal)
            m_Price = value
        End Set
    End Property
    Private m_Price As Decimal

    <Required> _
    Public Property RequiredAmount() As Decimal
        Get
            Return m_RequiredAmount
        End Get
        Set(value As Decimal)
            m_RequiredAmount = value
        End Set
    End Property
    Private m_RequiredAmount As Decimal
    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean
End Class
