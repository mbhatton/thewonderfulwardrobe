﻿Imports System.ComponentModel.DataAnnotations

Public Class ClassDetails

    Public Property Id() As Integer

        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer

    <Required> _
    Public Property ClassId() As Integer

        Get
            Return m_ClassId

        End Get
        Set(value As Integer)
            m_ClassId = value
        End Set
    End Property
    Private m_ClassId As Integer

    <Required> _
    Public Property Time() As String

        Get
            Return m_Time
        End Get
        Set(value As String)
            m_Time = value
        End Set
    End Property
    Private m_Time As String

    <Required> _
    Public Property Day() As String
        Get
            Return m_Day
        End Get
        Set(value As String)
            m_Day = value
        End Set
    End Property
    Private m_Day As String

    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

End Class
