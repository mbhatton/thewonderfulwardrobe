﻿Imports System.ComponentModel.DataAnnotations

Public Class Contacts
    <Required> _
    Public Property ContactId() As Integer
        Get
            Return m_ContactId
        End Get
        Set(value As Integer)
            m_ContactId = value
        End Set
    End Property
    Private m_ContactId As Integer

    <Required> _
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String

    <Required> _
    Public Property Email() As String
        Get
            Return m_Email
        End Get
        Set(value As String)
            m_Email = value
        End Set
    End Property
    Private m_Email As String

    <Required> _
    Public Property TelePhone() As String
        Get
            Return m_TelePhone
        End Get
        Set(value As String)
            m_TelePhone = value
        End Set
    End Property
    Private m_TelePhone As String
    <Required> _
    Public Property Message() As String
        Get
            Return m_Message
        End Get
        Set(value As String)
            m_Message = value
        End Set
    End Property
    Private m_Message As String
    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime

    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

End Class
