﻿Imports System.ComponentModel.DataAnnotations

Public Class Users
    <Required> _
    Public Property UserId() As Guid
        Get
            Return m_UserId
        End Get
        Set(value As Guid)
            m_UserId = value
        End Set
    End Property
    Private m_UserId As Guid

    <Required> _
    Public Property FirstName() As String
        Get
            Return m_FirstName
        End Get
        Set(value As String)
            m_FirstName = value
        End Set
    End Property
    Private m_FirstName As String

    <Required> _
    Public Property EmailId() As String
        Get
            Return m_EmailId
        End Get
        Set(value As String)
            m_EmailId = value
        End Set
    End Property
    Private m_EmailId As String
End Class
