﻿Imports System.ComponentModel.DataAnnotations

Public Class Gallery

    <Required> _
    Public Property Id() As Integer
        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer

    <Required> _
    Public Property ImageName() As String
        Get
            Return m_ImageName
        End Get
        Set(value As String)
            m_ImageName = value
        End Set
    End Property
    Private m_ImageName As String

    <Required> _
    Public Property Location() As String
        Get
            Return m_Location
        End Get
        Set(value As String)
            m_Location = value
        End Set
    End Property
    Private m_Location As String

    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime

    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean
End Class
