﻿Imports System.ComponentModel.DataAnnotations

Public Class PartyChildrenDetails

    <Required> _
    Public Property Id() As Integer
        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer

    <Required> _
    Public Property OrderId() As Integer
        Get
            Return m_OrderId
        End Get
        Set(value As Integer)
            m_OrderId = value
        End Set
    End Property
    Private m_OrderId As Integer

    <Required> _
    Public Property ChildId() As Integer
        Get
            Return m_ChildId
        End Get
        Set(value As Integer)
            m_ChildId = value
        End Set
    End Property
    Private m_ChildId As Integer
 
End Class
