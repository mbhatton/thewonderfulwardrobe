﻿Imports System.ComponentModel.DataAnnotations

Public Class PartyHostDetails

    <Required> _
    Public Property Id() As Integer
        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer

    <Required> _
    Public Property HostId() As String
        Get
            Return m_HostId
        End Get
        Set(value As String)
            m_HostId = value
        End Set
    End Property
    Private m_HostId As String

    <Required> _
    Public Property FirstName() As String

        Get
            Return m_FirstName

        End Get
        Set(value As String)
            m_FirstName = value
        End Set
    End Property
    Private m_FirstName As String

    <Required> _
    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(value As String)
            m_LastName = value
        End Set
    End Property
    Private m_LastName As String

    <Required> _
    Public Property Address1() As String
        Get
            Return m_Address1
        End Get
        Set(value As String)
            m_Address1 = value
        End Set
    End Property
    Private m_Address1 As String
    <Required> _
    Public Property Address2() As String
        Get
            Return m_Address2
        End Get
        Set(value As String)
            m_Address2 = value
        End Set
    End Property
    Private m_Address2 As String
    <Required> _
    Public Property Address3() As String
        Get
            Return m_Address3
        End Get
        Set(value As String)
            m_Address3 = value
        End Set
    End Property
    Private m_Address3 As String

    <Required> _
    Public Property PostalCode() As String
        Get
            Return m_PostalCode
        End Get
        Set(value As String)
            m_PostalCode = value
        End Set
    End Property
    Private m_PostalCode As String
    <Required> _
    Public Property TelePhone() As String
        Get
            Return m_TelePhone
        End Get
        Set(value As String)
            m_TelePhone = value
        End Set
    End Property
    Private m_TelePhone As String


    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean
End Class
