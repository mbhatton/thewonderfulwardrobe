﻿Imports System.ComponentModel.DataAnnotations

Public Class NewsLetterDetail

    <Required> _
    Public Property Id() As Integer
        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer
    Public Property content() As String
        Get
            Return m_content
        End Get
        Set(value As String)
            m_content = value
        End Set
    End Property
    Private m_content As String

    Public Property Title() As String
        Get
            Return m_Title
        End Get
        Set(value As String)
            m_Title = value
        End Set
    End Property
    Private m_Title As String
    Public Property IsPublish() As Boolean
        Get
            Return m_IsPublish
        End Get
        Set(value As Boolean)
            m_IsPublish = value
        End Set
    End Property
    Private m_IsPublish As Boolean
    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property CreatedBy() As Guid
        Get
            Return m_CreatedBy
        End Get
        Set(value As Guid)
            m_CreatedBy = value
        End Set
    End Property
    Private m_CreatedBy As Guid

    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean
End Class
