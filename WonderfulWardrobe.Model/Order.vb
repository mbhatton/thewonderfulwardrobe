﻿Imports System.ComponentModel.DataAnnotations

Public Class Order
    <Required> _
    Public Property OrderId() As Integer
        Get
            Return m_OrderId
        End Get
        Set(value As Integer)
            m_OrderId = value
        End Set
    End Property
    Private m_OrderId As Integer

    <Required> _
    Public Property PaymentId() As Integer
        Get
            Return m_PaymentId
        End Get
        Set(value As Integer)
            m_PaymentId = value
        End Set
    End Property
    Private m_PaymentId As Integer


    <Required> _
    Public Property OrderDate() As DateTime
        Get
            Return m_OrderDate
        End Get
        Set(value As DateTime)
            m_OrderDate = value
        End Set
    End Property
    Private m_OrderDate As DateTime

    <Required> _
    Public Property UserId() As Guid
        Get
            Return m_UserId
        End Get
        Set(value As Guid)
            m_UserId = value
        End Set
    End Property
    Private m_UserId As Guid

End Class
