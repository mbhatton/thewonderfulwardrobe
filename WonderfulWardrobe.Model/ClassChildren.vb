﻿Imports System.ComponentModel.DataAnnotations

Public Class ClassChildren
    <Required> _
    Public Property ChildId() As Integer
        Get
            Return m_ChildId
        End Get
        Set(value As Integer)
            m_ChildId = value
        End Set
    End Property
    Private m_ChildId As Integer

    <Required> _
    Public Property RegisteredClassId() As Integer
        Get
            Return m_RegisteredClassId
        End Get
        Set(value As Integer)
            m_RegisteredClassId = value
        End Set
    End Property
    Private m_RegisteredClassId As Integer

    <Required> _
    Public Property ClassId() As Integer
        Get
            Return m_ClassId
        End Get
        Set(value As Integer)
            m_ClassId = value
        End Set
    End Property
    Private m_ClassId As Integer

    <Required> _
    Public Property UserId() As Guid

        Get
            Return m_UserId

        End Get
        Set(value As Guid)
            m_UserId = value
        End Set
    End Property
    Private m_UserId As Guid

    <Required> _
    Public Property FirstName() As String

        Get
            Return m_FirstName

        End Get
        Set(value As String)
            m_FirstName = value
        End Set
    End Property
    Private m_FirstName As String

    <Required> _
    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(value As String)
            m_LastName = value
        End Set
    End Property
    Private m_LastName As String

    <Required> _
    Public Property DOB() As DateTime
        Get
            Return m_DOB
        End Get
        Set(value As DateTime)
            m_DOB = value
        End Set
    End Property
    Private m_DOB As DateTime

    <Required> _
    Public Property Gender() As String
        Get
            Return m_Gender
        End Get
        Set(value As String)
            m_Gender = value
        End Set
    End Property
    Private m_Gender As String


    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean
End Class
