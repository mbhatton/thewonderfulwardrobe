﻿Public Class PayPalViewData
    Public Property JsonRequest() As String
        Get
            Return m_JsonRequest
        End Get
        Set(value As String)
            m_JsonRequest = value
        End Set
    End Property
    Private m_JsonRequest As String
    Public Property JsonResponse() As String
        Get
            Return m_JsonResponse
        End Get
        Set(value As String)
            m_JsonResponse = value
        End Set
    End Property
    Private m_JsonResponse As String
    Public Property ErrorMessage() As String
        Get
            Return m_ErrorMessage
        End Get
        Set(value As String)
            m_ErrorMessage = value
        End Set
    End Property
    Private m_ErrorMessage As String
End Class