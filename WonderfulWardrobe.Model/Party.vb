﻿Imports System.ComponentModel.DataAnnotations

Public Class Party

    <Required> _
    Public Property PartyId() As Integer
        Get
            Return m_PartyId
        End Get
        Set(value As Integer)
            m_PartyId = value
        End Set
    End Property
    Private m_PartyId As Integer
    <Required> _
    Public Property OrderId() As Integer
        Get
            Return m_OrderId
        End Get
        Set(value As Integer)
            m_OrderId = value
        End Set
    End Property
    Private m_OrderId As Integer

    <Required> _
    Public Property HostId() As Guid
        Get
            Return m_HostId
        End Get
        Set(value As Guid)
            m_HostId = value
        End Set
    End Property
    Private m_HostId As Guid

    <Required> _
    Public Property Duration() As String
        Get
            Return m_Duration
        End Get
        Set(value As String)
            m_Duration = value
        End Set
    End Property
    Private m_Duration As String

    <Required> _
    Public Property UserName() As String
        Get
            Return m_UserName
        End Get
        Set(value As String)
            m_UserName = value
        End Set
    End Property
    Private m_UserName As String


    <Required> _
    Public Property Time() As String
        Get
            Return m_Time
        End Get
        Set(value As String)
            m_Time = value
        End Set
    End Property
    Private m_Time As String

    <Required> _
    Public Property PartyDate() As DateTime
        Get
            Return m_PartyDate
        End Get
        Set(value As DateTime)
            m_PartyDate = value
        End Set
    End Property
    Private m_PartyDate As DateTime

    <Required> _
    Public Property Price() As Decimal
        Get
            Return m_Price
        End Get
        Set(value As Decimal)
            m_Price = value
        End Set
    End Property
    Private m_Price As Decimal

    <Required> _
    Public Property PaidAmount() As Decimal
        Get
            Return m_PaidAmount
        End Get
        Set(value As Decimal)
            m_PaidAmount = value
        End Set
    End Property
    Private m_PaidAmount As Decimal


    <Required> _
    Public Property Reason() As String
        Get
            Return m_Reason
        End Get
        Set(value As String)
            m_Reason = value
        End Set
    End Property
    Private m_Reason As String
    <Required> _
    Public Property Location() As String
        Get
            Return m_Location
        End Get
        Set(value As String)
            m_Location = value
        End Set
    End Property
    Private m_Location As String

    <Required> _
    Public Property NoOfChildren() As Integer
        Get
            Return m_NoOfChildren
        End Get
        Set(value As Integer)
            m_NoOfChildren = value
        End Set
    End Property
    Private m_NoOfChildren As Integer


    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime

    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean
End Class
