﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class Family
    <Required> _
    Public Property Id() As Integer
        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = Value
        End Set
    End Property
    Private m_Id As Integer

    <Required> _
    Public Property userId() As String
        Get
            Return m_userId
        End Get
        Set(value As String)
            m_userId = Value
        End Set
    End Property
    Private m_userId As String

    <Required> _
    Public Property FirstName() As String
        Get
            Return m_FirstName
        End Get
        Set(value As String)
            m_FirstName = Value
        End Set
    End Property
    Private m_FirstName As String
    <Required> _
    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(value As String)
            m_LastName = Value
        End Set
    End Property
    Private m_LastName As String
    <Required> _
    Public Property DOB() As DateTime
        Get
            Return m_DOB
        End Get
        Set(value As DateTime)
            m_DOB = Value
        End Set
    End Property
    Private m_DOB As DateTime

    <Required> _
    Public Property Gender() As String
        Get
            Return m_Gender
        End Get
        Set(value As String)
            m_Gender = Value
        End Set
    End Property
    Private m_Gender As String


    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

End Class
