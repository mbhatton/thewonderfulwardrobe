﻿Imports System.ComponentModel.DataAnnotations

Public Class CreatedOrders

    <Required> _
    Public Property OrderId() As Integer
        Get
            Return m_OrderId
        End Get
        Set(value As Integer)
            m_OrderId = value
        End Set
    End Property
    Private m_OrderId As Integer

    <Required> _
    Public Property OrderDate() As DateTime
        Get
            Return m_OrderDate
        End Get
        Set(value As DateTime)
            m_OrderDate = value
        End Set
    End Property
    Private m_OrderDate As DateTime

    <Required> _
    Public Property UserName() As String
        Get
            Return m_UserName
        End Get
        Set(value As String)
            m_UserName = value
        End Set
    End Property
    Private m_UserName As String

    <Required> _
    Public Property Childs() As String
        Get
            Return m_Childs
        End Get
        Set(value As String)
            m_Childs = value
        End Set
    End Property
    Private m_Childs As String

End Class
