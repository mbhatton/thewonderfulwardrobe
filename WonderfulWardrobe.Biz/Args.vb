﻿Imports System.ComponentModel.DataAnnotations

Public Class Args
    <Required> _
    Public Property UserList() As List(Of Model.NewsLetter)
        Get
            Return m_UserList
        End Get
        Set(value As List(Of Model.NewsLetter))
            m_UserList = value
        End Set
    End Property
    Private m_UserList As List(Of Model.NewsLetter)

    Public Property NewsLetter() As Model.NewsLetterDetail
        Get
            Return m_NewsLetter
        End Get
        Set(value As Model.NewsLetterDetail)
            m_NewsLetter = value
        End Set
    End Property
    Private m_NewsLetter As Model.NewsLetterDetail

    Public Property Id() As Integer
        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer
End Class
