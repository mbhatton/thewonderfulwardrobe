﻿Public Class AccountViewModel

    Public Property ParentId() As Integer
        Get
            Return m_ParentId
        End Get
        Set(value As Integer)
            m_ParentId = value
        End Set
    End Property
    Private m_ParentId As Integer

    Public Property UserId() As Guid
        Get
            Return m_UserId
        End Get
        Set(value As Guid)
            m_UserId = value
        End Set
    End Property
    Private m_UserId As Guid

    Public Property FirstName() As String
        Get
            Return m_FirstName
        End Get
        Set(value As String)
            m_FirstName = value
        End Set
    End Property
    Private m_FirstName As String
    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(value As String)
            m_LastName = value
        End Set
    End Property
    Private m_LastName As String
    Public Property EmailId() As String
        Get
            Return m_EmailId
        End Get
        Set(value As String)
            m_EmailId = value
        End Set
    End Property
    Private m_EmailId As String

    Public Property Password() As String
        Get
            Return m_Password
        End Get
        Set(value As String)
            m_Password = value
        End Set
    End Property
    Private m_Password As String

    Public Property PasswordHint() As String
        Get
            Return m_PasswordHint
        End Get
        Set(value As String)
            m_PasswordHint = value
        End Set
    End Property
    Private m_PasswordHint As String
    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime

    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

    Public Property family() As List(Of FamilyViewModel)
        Get
            Return m_family
        End Get
        Set(value As List(Of FamilyViewModel))
            m_family = value
        End Set
    End Property
    Private m_family As List(Of FamilyViewModel)

   

    Public Property AdminList() As List(Of AccountViewModel)
        Get
            Return m_AdminList
        End Get
        Set(value As List(Of AccountViewModel))
            m_AdminList = value
        End Set
    End Property
    Private m_AdminList As List(Of AccountViewModel)

End Class
