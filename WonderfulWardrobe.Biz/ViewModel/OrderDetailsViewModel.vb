﻿Imports System.ComponentModel.DataAnnotations

Public Class OrderDetailsViewModel
    <Required> _
    Public Property OrderId() As Integer
        Get
            Return m_OrderId
        End Get
        Set(value As Integer)
            m_OrderId = value
        End Set
    End Property
    Private m_OrderId As Integer

    <Required> _
    Public Property FirstName() As String
        Get
            Return m_FirstName
        End Get
        Set(value As String)
            m_FirstName = value
        End Set
    End Property
    Private m_FirstName As String
    <Required> _
    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(value As String)
            m_LastName = value
        End Set
    End Property
    Private m_LastName As String

    <Required> _
    Public Property Duration() As String
        Get
            Return m_Duration
        End Get
        Set(value As String)
            m_Duration = value
        End Set
    End Property
    Private m_Duration As String

    <Required> _
    Public Property StartDateTime() As DateTime
        Get
            Return m_StartDateTime
        End Get
        Set(value As DateTime)
            m_StartDateTime = value
        End Set
    End Property
    Private m_StartDateTime As DateTime

    <Required> _
    Public Property Term() As String
        Get
            Return m_Term
        End Get
        Set(value As String)
            m_Term = value
        End Set
    End Property
    Private m_Term As String

    <Required> _
    Public Property Venue() As String
        Get
            Return m_Venue
        End Get
        Set(value As String)
            m_Venue = value
        End Set
    End Property
    Private m_Venue As String


    Public Property OrderDetailList() As List(Of OrderDetailsViewModel)
        Get
            Return m_OrderDetailList
        End Get
        Set(value As List(Of OrderDetailsViewModel))
            m_OrderDetailList = value
        End Set
    End Property
    Private m_OrderDetailList As List(Of OrderDetailsViewModel)

End Class
