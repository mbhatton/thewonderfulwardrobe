﻿Imports System.ComponentModel.DataAnnotations

Public Class TermsViewModel
    <Required> _
    Public Property TermId() As Integer
        Get
            Return m_TermId
        End Get
        Set(value As Integer)
            m_TermId = value
        End Set
    End Property
    Private m_TermId As Integer

    <Required> _
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String

    <Required> _
    Public Property TermName() As String
        Get
            Return m_TermName
        End Get
        Set(value As String)
            m_TermName = value
        End Set
    End Property
    Private m_TermName As String

    <Required> _
    Public Property Venue() As String
        Get
            Return m_Venue
        End Get
        Set(value As String)
            m_Venue = value
        End Set
    End Property
    Private m_Venue As String

    <Required> _
    Public Property Day() As String
        Get
            Return m_Day
        End Get
        Set(value As String)
            m_Day = value
        End Set
    End Property
    Private m_Day As String

    <Required> _
    Public Property DiscountAmount() As Decimal
        Get
            Return m_DiscountAmount
        End Get
        Set(value As Decimal)
            m_DiscountAmount = value
        End Set
    End Property
    Private m_DiscountAmount As Decimal

    <Required> _
    Public Property TotalAmount() As String
        Get
            Return m_TotalAmount
        End Get
        Set(value As String)
            m_TotalAmount = value
        End Set
    End Property
    Private m_TotalAmount As String

    '<DisplayFormat(ApplyFormatInEditMode:=True, DataFormatString:="{0:MM/dd/yyyy}")>
    '<Required> _
    'Public Property StartDate() As DateTime
    '    Get
    '        Return m_StartDate
    '    End Get
    '    Set(value As DateTime)
    '        m_StartDate = value
    '    End Set
    'End Property
    'Private m_StartDate As DateTime


    '<DisplayFormat(ApplyFormatInEditMode:=True, DataFormatString:="{0:MM/dd/yyyy}")>
    '<Required> _
    ' Public Property EndDate() As DateTime
    '    Get
    '        Return m_EndDate
    '    End Get
    '    Set(value As DateTime)
    '        m_EndDate = value
    '    End Set
    'End Property
    'Private m_EndDate As DateTime

    <Required> _
    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime

    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

    Public Property TermsList() As List(Of TermsViewModel)
        Get
            Return m_TermsList
        End Get
        Set(value As List(Of TermsViewModel))
            m_TermsList = value
        End Set
    End Property
    Private m_TermsList As List(Of TermsViewModel)

    Public Property ClassList() As List(Of ClassViewModel)
        Get
            Return m_ClassList
        End Get
        Set(value As List(Of ClassViewModel))
            m_ClassList = value
        End Set
    End Property
    Private m_ClassList As List(Of ClassViewModel)
End Class
