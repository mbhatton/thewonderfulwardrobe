﻿Imports System.ComponentModel.DataAnnotations

Public Class PartyBookingViewModel
    <Required> _
    Public Property Id() As Integer
        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer

    <Required> _
    Public Property HostId() As String
        Get
            Return m_HostId
        End Get
        Set(value As String)
            m_HostId = value
        End Set
    End Property
    Private m_HostId As String

    <Required> _
    Public Property FirstName() As String

        Get
            Return m_FirstName

        End Get
        Set(value As String)
            m_FirstName = value
        End Set
    End Property
    Private m_FirstName As String

    <Required> _
    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(value As String)
            m_LastName = value
        End Set
    End Property
    Private m_LastName As String

    <Required> _
    Public Property Address1() As String
        Get
            Return m_Address1
        End Get
        Set(value As String)
            m_Address1 = value
        End Set
    End Property
    Private m_Address1 As String
    <Required> _
    Public Property Address2() As String
        Get
            Return m_Address2
        End Get
        Set(value As String)
            m_Address2 = value
        End Set
    End Property
    Private m_Address2 As String
    <Required> _
    Public Property Address3() As String
        Get
            Return m_Address3
        End Get
        Set(value As String)
            m_Address3 = value
        End Set
    End Property
    Private m_Address3 As String

    <Required> _
    Public Property PostalCode() As String
        Get
            Return m_PostalCode
        End Get
        Set(value As String)
            m_PostalCode = value
        End Set
    End Property
    Private m_PostalCode As String
    <Required> _
    Public Property TelePhone() As String
        Get
            Return m_TelePhone
        End Get
        Set(value As String)
            m_TelePhone = value
        End Set
    End Property
    Private m_TelePhone As String

    Public Property PartyReason() As String
        Get
            Return m_PartyReason
        End Get
        Set(value As String)
            m_PartyReason = value
        End Set
    End Property
    Private m_PartyReason As String

    Public Property PartyLocation() As String
        Get
            Return m_PartyLocation
        End Get
        Set(value As String)
            m_PartyLocation = value
        End Set
    End Property
    Private m_PartyLocation As String

    Public Property ChildrenAttending() As String
        Get
            Return m_ChildrenAttending
        End Get
        Set(value As String)
            m_ChildrenAttending = value
        End Set
    End Property
    Private m_ChildrenAttending As String

    Public Property PaymentMode() As String
        Get
            Return m_PaymentMode
        End Get
        Set(value As String)
            m_PaymentMode = value
        End Set
    End Property
    Private m_PaymentMode As String

    Public Property TotalAmount() As String
        Get
            Return m_TotalAmount
        End Get
        Set(value As String)
            m_TotalAmount = value
        End Set
    End Property
    Private m_TotalAmount As String

    Public Property PaidAmount() As String
        Get
            Return m_PaidAmount
        End Get
        Set(value As String)
            m_PaidAmount = value
        End Set
    End Property
    Private m_PaidAmount As String

    Public Property TransactionId() As String
        Get
            Return m_TransactionId
        End Get
        Set(value As String)
            m_TransactionId = value
        End Set
    End Property
    Private m_TransactionId As String

    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

    Public Property family() As List(Of FamilyViewModel)
        Get
            Return m_family
        End Get
        Set(value As List(Of FamilyViewModel))
            m_family = value
        End Set
    End Property
    Private m_family As List(Of FamilyViewModel)

    Public Property PartyList() As List(Of PartyViewModel)
        Get
            Return m_PartyList
        End Get
        Set(value As List(Of PartyViewModel))
            m_PartyList = value
        End Set
    End Property
    Private m_PartyList As List(Of PartyViewModel)

    Public Property SuggestionList() As List(Of PartyEnquiryViewModel)
        Get
            Return m_SuggestionList
        End Get
        Set(value As List(Of PartyEnquiryViewModel))
            m_SuggestionList = value
        End Set
    End Property
    Private m_SuggestionList As List(Of PartyEnquiryViewModel)



End Class
