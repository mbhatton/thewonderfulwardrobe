﻿Imports System.ComponentModel.DataAnnotations

Public Class PartyEnquiryViewModel
    <Required> _
    Public Property EnquiryId() As Integer
        Get
            Return m_EnquiryId
        End Get
        Set(value As Integer)
            m_EnquiryId = value
        End Set
    End Property
    Private m_EnquiryId As Integer

    <Required> _
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String

    <Required> _
    Public Property Email() As String
        Get
            Return m_Email
        End Get
        Set(value As String)
            m_Email = value
        End Set
    End Property
    Private m_Email As String

    <Required> _
    Public Property PhoneNumber() As String
        Get
            Return m_PhoneNumber
        End Get
        Set(value As String)
            m_PhoneNumber = value
        End Set
    End Property
    Private m_PhoneNumber As String

    <Required> _
    Public Property PreferedDate1() As DateTime
        Get
            Return m_PreferedDate1
        End Get
        Set(value As DateTime)
            m_PreferedDate1 = value
        End Set
    End Property
    Private m_PreferedDate1 As DateTime

    <Required> _
    Public Property PreferedDate2() As DateTime
        Get
            Return m_PreferedDate2
        End Get
        Set(value As DateTime)
            m_PreferedDate2 = value
        End Set
    End Property
    Private m_PreferedDate2 As DateTime

    <Required> _
    Public Property PreferedStartTime() As String
        Get
            Return m_PreferedStartTime
        End Get
        Set(value As String)
            m_PreferedStartTime = value
        End Set
    End Property
    Private m_PreferedStartTime As String

    <Required> _
    Public Property Duration() As String
        Get
            Return m_Duration
        End Get
        Set(value As String)
            m_Duration = value
        End Set
    End Property
    Private m_Duration As String


    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean



    <Required> _
    Public Property SuggestedDate() As DateTime
        Get
            Return m_SuggestedDate
        End Get
        Set(value As DateTime)
            m_SuggestedDate = value
        End Set
    End Property
    Private m_SuggestedDate As DateTime

    <Required> _
    Public Property SuggestedTime() As String
        Get
            Return m_SuggestedTime
        End Get
        Set(value As String)
            m_SuggestedTime = value
        End Set
    End Property
    Private m_SuggestedTime As String

    <Required> _
    Public Property SuggestedDuration() As String
        Get
            Return m_SuggestedDuration
        End Get
        Set(value As String)
            m_SuggestedDuration = value
        End Set
    End Property
    Private m_SuggestedDuration As String

    <Required> _
    Public Property SuggestedPrice() As String
        Get
            Return m_SuggestedPrice
        End Get
        Set(value As String)
            m_SuggestedPrice = value
        End Set
    End Property
    Private m_SuggestedPrice As String

    <Required> _
    Public Property Price() As Decimal
        Get
            Return m_Price
        End Get
        Set(value As Decimal)
            m_Price = value
        End Set
    End Property
    Private m_Price As Decimal

    <Required> _
    Public Property RequiredAmount() As Decimal
        Get
            Return m_RequiredAmount
        End Get
        Set(value As Decimal)
            m_RequiredAmount = value
        End Set
    End Property
    Private m_RequiredAmount As Decimal

    Public Property PartyEnquiryList() As List(Of PartyEnquiryViewModel)
        Get
            Return m_PartyEnquiryList
        End Get
        Set(value As List(Of PartyEnquiryViewModel))
            m_PartyEnquiryList = value
        End Set
    End Property
    Private m_PartyEnquiryList As List(Of PartyEnquiryViewModel)
End Class
