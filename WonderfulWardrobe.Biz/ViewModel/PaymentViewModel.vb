﻿Imports System.ComponentModel.DataAnnotations

Public Class PaymentViewModel
    <Required> _
    Public Property PaymentId() As Integer
        Get
            Return m_PaymentId
        End Get
        Set(value As Integer)
            m_PaymentId = value
        End Set
    End Property
    Private m_PaymentId As Integer

    <Required> _
    Public Property OrderId() As Integer
        Get
            Return m_OrderId
        End Get
        Set(value As Integer)
            m_OrderId = value
        End Set
    End Property
    Private m_OrderId As Integer

    <Required> _
    Public Property UserId() As Guid
        Get
            Return m_UserId
        End Get
        Set(value As Guid)
            m_UserId = value
        End Set
    End Property
    Private m_UserId As Guid

    <Required> _
    Public Property UserName() As String
        Get
            Return m_UserName
        End Get
        Set(value As String)
            m_UserName = value
        End Set
    End Property
    Private m_UserName As String

    <Required> _
    Public Property PaymentType() As String
        Get
            Return m_PaymentType
        End Get
        Set(value As String)
            m_PaymentType = value
        End Set
    End Property
    Private m_PaymentType As String

    <Required> _
    Public Property TotalAmount() As Decimal
        Get
            Return m_TotalAmount
        End Get
        Set(value As Decimal)
            m_TotalAmount = value
        End Set
    End Property
    Private m_TotalAmount As Decimal

    <Required> _
    Public Property PayAmount() As Decimal
        Get
            Return m_PayAmount
        End Get
        Set(value As Decimal)
            m_PayAmount = value
        End Set
    End Property
    Private m_PayAmount As Decimal

    <Required> _
    Public Property PaymentFor() As String
        Get
            Return m_PaymentFor
        End Get
        Set(value As String)
            m_PaymentFor = value
        End Set
    End Property
    Private m_PaymentFor As String

    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime


    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

    Public Property PaymentList() As List(Of PaymentViewModel)
        Get
            Return m_PaymentList
        End Get
        Set(value As List(Of PaymentViewModel))
            m_PaymentList = value
        End Set
    End Property
    Private m_PaymentList As List(Of PaymentViewModel)
End Class
