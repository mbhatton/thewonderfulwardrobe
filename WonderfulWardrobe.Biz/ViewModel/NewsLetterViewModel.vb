﻿Imports System.ComponentModel.DataAnnotations
Imports System.Web.Mvc

Public Class NewsLetterViewModel
    <Required> _
    Public Property Id() As Integer
        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer

    <Required> _
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String

    Public Property content() As String
        Get
            Return m_content
        End Get
        Set(value As String)
            m_content = value
        End Set
    End Property
    Private m_content As String

    Public Property Title() As String
        Get
            Return m_Title
        End Get
        Set(value As String)
            m_Title = value
        End Set
    End Property
    Private m_Title As String

    Public Property IsPublish() As Boolean
        Get
            Return m_IsPublish
        End Get
        Set(value As Boolean)
            m_IsPublish = value
        End Set
    End Property
    Private m_IsPublish As Boolean

    <Required> _
    Public Property EmailId() As String
        Get
            Return m_EmailId
        End Get
        Set(value As String)
            m_EmailId = value
        End Set
    End Property
    Private m_EmailId As String

    Public Property CreatedOn() As DateTime
        Get
            Return m_CreatedOn
        End Get
        Set(value As DateTime)
            m_CreatedOn = value
        End Set
    End Property
    Private m_CreatedOn As DateTime

    Public Property IsActive() As Boolean
        Get
            Return m_IsActive
        End Get
        Set(value As Boolean)
            m_IsActive = value
        End Set
    End Property
    Private m_IsActive As Boolean

    Public Property NewsLetterList() As List(Of NewsLetterViewModel)
        Get
            Return m_NewsLetterList
        End Get
        Set(value As List(Of NewsLetterViewModel))
            m_NewsLetterList = value
        End Set
    End Property
    Private m_NewsLetterList As List(Of NewsLetterViewModel)
End Class
