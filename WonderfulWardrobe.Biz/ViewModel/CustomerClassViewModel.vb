﻿Public Class CustomerClassViewModel
    Public Property VenuesList() As List(Of VenueViewModel)
        Get
            Return m_VenuesList
        End Get
        Set(value As List(Of VenueViewModel))
            m_VenuesList = value
        End Set
    End Property
    Private m_VenuesList As List(Of VenueViewModel)

    Public Property TermsList() As List(Of TermsViewModel)
        Get
            Return m_TermsList
        End Get
        Set(value As List(Of TermsViewModel))
            m_TermsList = value
        End Set
    End Property
    Private m_TermsList As List(Of TermsViewModel)

    Public Property ClassList() As List(Of ClassViewModel)
        Get
            Return m_ClassList
        End Get
        Set(value As List(Of ClassViewModel))
            m_ClassList = value
        End Set
    End Property
    Private m_ClassList As List(Of ClassViewModel)

End Class
