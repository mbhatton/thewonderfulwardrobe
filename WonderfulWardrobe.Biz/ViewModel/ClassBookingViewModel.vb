﻿Imports System.ComponentModel.DataAnnotations

Public Class ClassBookingViewModel

    <Required> _
    Public Property VenueId() As Integer

        Get
            Return m_VenueId

        End Get
        Set(value As Integer)
            m_VenueId = value
        End Set
    End Property
    Private m_VenueId As Integer

    <Required> _
    Public Property Day() As String

        Get
            Return m_Day

        End Get
        Set(value As String)
            m_Day = value
        End Set
    End Property
    Private m_Day As String

    <Required> _
    Public Property VenueName() As String

        Get
            Return m_VenueName

        End Get
        Set(value As String)
            m_VenueName = value
        End Set
    End Property
    Private m_VenueName As String

    <Required> _
    Public Property Logo() As String

        Get
            Return m_Logo

        End Get
        Set(value As String)
            m_Logo = value
        End Set
    End Property
    Private m_Logo As String

    <Required> _
    Public Property BookingType() As String

        Get
            Return m_BookingType

        End Get
        Set(value As String)
            m_BookingType = value
        End Set
    End Property
    Private m_BookingType As String

    <Required> _
    Public Property TotalAmount() As String

        Get
            Return m_TotalAmount

        End Get
        Set(value As String)
            m_TotalAmount = value
        End Set
    End Property
    Private m_TotalAmount As String

    <Required> _
    Public Property PaymentMode() As String

        Get
            Return m_PaymentMode

        End Get
        Set(value As String)
            m_PaymentMode = value
        End Set
    End Property
    Private m_PaymentMode As String

    <Required> _
    Public Property TransactionId() As String

        Get
            Return m_TransactionId

        End Get
        Set(value As String)
            m_TransactionId = value
        End Set
    End Property
    Private m_TransactionId As String

    <Required> _
    Public Property PaidAmount() As String
        Get
            Return m_PaidAmount

        End Get
        Set(value As String)
            m_PaidAmount = value
        End Set
    End Property
    Private m_PaidAmount As String

    <Required> _
    Public Property Address1() As String
        Get
            Return m_Address1
        End Get
        Set(value As String)
            m_Address1 = value
        End Set
    End Property
    Private m_Address1 As String
    <Required> _
    Public Property Address2() As String
        Get
            Return m_Address2
        End Get
        Set(value As String)
            m_Address2 = value
        End Set
    End Property
    Private m_Address2 As String
    <Required> _
    Public Property Address3() As String
        Get
            Return m_Address3
        End Get
        Set(value As String)
            m_Address3 = value
        End Set
    End Property
    Private m_Address3 As String

    <Required> _
    Public Property City() As String
        Get
            Return m_City
        End Get
        Set(value As String)
            m_City = value
        End Set
    End Property
    Private m_City As String

    <Required> _
    Public Property TotalTermAmount() As String

        Get
            Return m_TotalTermAmount

        End Get
        Set(value As String)
            m_TotalTermAmount = value
        End Set
    End Property
    Private m_TotalTermAmount As String
    Public Property CLassList() As List(Of ClassViewModel)
        Get
            Return m_CLassList
        End Get
        Set(value As List(Of ClassViewModel))
            m_CLassList = value
        End Set
    End Property
    Private m_CLassList As List(Of ClassViewModel)

    Public Property TermList() As List(Of TermsViewModel)
        Get
            Return m_TermList
        End Get
        Set(value As List(Of TermsViewModel))
            m_TermList = value
        End Set
    End Property
    Private m_TermList As List(Of TermsViewModel)

    Public Property BookedClassList() As List(Of ClassBookingViewModel)
        Get
            Return m_BookedClassList
        End Get
        Set(value As List(Of ClassBookingViewModel))
            m_BookedClassList = value
        End Set
    End Property
    Private m_BookedClassList As List(Of ClassBookingViewModel)

    Public Property family() As List(Of FamilyViewModel)
        Get
            Return m_family
        End Get
        Set(value As List(Of FamilyViewModel))
            m_family = value
        End Set
    End Property
    Private m_family As List(Of FamilyViewModel)

End Class
