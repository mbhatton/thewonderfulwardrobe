﻿Imports Autofac
Imports WonderfulWardrobe.Data

Public Class AutofacRegisterType
    Public Function RegisterBuilderType(builder As ContainerBuilder) As ContainerBuilder
        builder.RegisterType(Of Account)().[As](Of IAccount)().InstancePerLifetimeScope()
        builder.RegisterType(Of AccountRegister)().[As](Of IAccountRegister)().InstancePerLifetimeScope()

        builder.RegisterType(Of AdminClass)().[As](Of IAdminClass)().InstancePerLifetimeScope()
        builder.RegisterType(Of AdminClassRegistration)().[As](Of IAdminClassRegistration)().InstancePerLifetimeScope()

        builder.RegisterType(Of CustomerClass)().[As](Of ICustomerClasses)().InstancePerLifetimeScope()
        builder.RegisterType(Of ClassBooking)().[As](Of IClassBooking)().InstancePerLifetimeScope()

        builder.RegisterType(Of AdminPayment)().[As](Of IAdminPayment)().InstancePerLifetimeScope()
        builder.RegisterType(Of AdminPaymentDetails)().[As](Of IAdminPaymentDetails)().InstancePerLifetimeScope()

        builder.RegisterType(Of Contact)().[As](Of IContact)().InstancePerLifetimeScope()
        builder.RegisterType(Of ContactUs)().[As](Of IContactUs)().InstancePerLifetimeScope()

        builder.RegisterType(Of Parties)().[As](Of IParties)().InstancePerLifetimeScope()
        builder.RegisterType(Of PartyBooking)().[As](Of IPartyBooking)().InstancePerLifetimeScope()

        builder.RegisterType(Of GalleryClass)().[As](Of IGalleryClass)().InstancePerLifetimeScope()
        builder.RegisterType(Of Galeries)().[As](Of IGaleries)().InstancePerLifetimeScope()

        builder.RegisterType(Of NewsLetter)().[As](Of INewsLetter)().InstancePerLifetimeScope()
        builder.RegisterType(Of NewsLetters)().[As](Of INewsLetters)().InstancePerLifetimeScope()

        Return builder
    End Function
End Class
