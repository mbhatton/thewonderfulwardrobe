﻿Public Interface INewsLetter
    Function BindNewsLettersUser() As List(Of NewsLetterViewModel)
    Sub RegisterNewsLetter(ViewModel As NewsLetterViewModel)
    Sub DeleteUser(id As Integer)
    Sub SaveNewsLetter(ViewModel As NewsLetterViewModel)
    Function BindNewsLettersList() As List(Of NewsLetterViewModel)
    Sub DeleteNewsLetter(id As Integer)
    Function GetNewsLetterDetail(id As Integer) As NewsLetterViewModel
    Sub UpdateNewsLetter(ViewModel As NewsLetterViewModel, Id As Integer)
    Sub SendNewsLetter(id As Integer)

End Interface
