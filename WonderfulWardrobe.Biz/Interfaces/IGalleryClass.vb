﻿Public Interface IGalleryClass
    Function BindGallery() As List(Of GalleryViewModel)
    Sub UploadImage(ViewModel As GalleryViewModel)
    Sub DeleteImage(id As Integer)

End Interface
