﻿Public Interface ICustomerClasses
    Function BindVenues() As List(Of VenueViewModel)
    Function GetTermByVenueId(VenueId As Integer) As List(Of TermsViewModel)
    Function GetClassByClassId(ClassId As Integer) As ClassBookingViewModel
    Function BindTermWiseClasses() As CustomerClassViewModel
    Sub AddChild(family As FamilyViewModel)
    Function GetFamily() As List(Of FamilyViewModel)
    Function BindClassByTermVenue(termId, VenueId) As List(Of ClassViewModel)
    Function BindVenuebyId(VenueId As Integer) As VenueViewModel
    Function BindTermbyId(TermId As Integer) As TermsViewModel
    Function SaveBookedClass(ClassBookingViewModel As ClassBookingViewModel) As String
End Interface
