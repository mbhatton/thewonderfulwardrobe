﻿Public Interface IParties
    Sub CreatePartyEnquiry(ByVal _enquiry As PartyEnquiryViewModel)
    Function BindPartyEnquiryList() As List(Of PartyEnquiryViewModel)
    Function EnquiryDetails(enquiryId As Integer) As PartyEnquiryViewModel
    Sub SaveSuggestion(viewmodel As PartyEnquiryViewModel)
    Function BindprocessedEnquiryList() As List(Of PartyEnquiryViewModel)
    Function ProcessedEnquiryDetails(EnquiryId As Integer) As List(Of PartyEnquiryViewModel)
    Sub SaveParty(ViewModel As PartyBookingViewModel)
    Function BindBookedPartyList() As List(Of PartyViewModel)
    Function PartyDetailsbyId(PartyId As Integer) As PartyBookingViewModel

    Function AddChild(family As FamilyViewModel) As String
    Function GetFamily() As List(Of FamilyViewModel)
    Function GetChildInfo(childid As String) As FamilyViewModel

End Interface
