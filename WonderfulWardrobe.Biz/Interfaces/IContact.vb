﻿Public Interface IContact
    Sub CreateContact(ByVal _contact As ContactViewModel)
    Function BindContactsList() As List(Of ContactViewModel)
    Function ContactInfo(contactId As Integer) As ContactViewModel
End Interface
