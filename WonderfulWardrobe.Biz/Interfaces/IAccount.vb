﻿Imports WonderfulWardrobe.Model
Public Interface IAccount
    Function RegisterFamily(ByVal _AccountViewModel As AccountViewModel) As String
    Function Login(ByVal _AccountViewModel As AccountViewModel) As String
    Function BindAdminList() As List(Of AccountViewModel)

End Interface
