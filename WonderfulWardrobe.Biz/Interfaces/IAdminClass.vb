﻿Imports System.Web.Mvc

Public Interface IAdminClass
    Sub CreateTerms(ByVal _terms As TermsViewModel)
    Sub CreateVenues(ByVal _venues As VenueViewModel)
    Sub CreateClasses(ByVal _classes As ClassViewModel)
    Function GetTermsList() As List(Of SelectListItem)
    Function GetVenuesList() As List(Of SelectListItem)
    Function BindTerms() As List(Of TermsViewModel)
    Function GetTermByTermId(TermId As Integer) As TermsViewModel
    Function GetClassByTermId(TermId As Integer) As List(Of ClassViewModel)
    Function GetClassByVenueId(VenueId As Integer) As List(Of ClassViewModel)
    Sub DeleteTerm(ByVal TermId As Integer)
    Sub UpdateTerm(ByVal term As TermsViewModel)
    Function BindVenues() As List(Of VenueViewModel)
    Function GetVenueByVenueId(VenueId As Integer) As VenueViewModel
    Sub DeleteVenue(ByVal VenueId As Integer)
    Sub UpdateVenue(ByVal venue As VenueViewModel)
    Function BindClasses() As List(Of ClassViewModel)
    Function GetClassByClassId(ClassId As Integer) As ClassViewModel
    Sub DeleteClass(ByVal VenueId As Integer)
    Sub UpdateClass(ByVal venue As ClassViewModel)
    Function GetTermsListByTermId(TermId As Integer) As List(Of SelectListItem)
    Function GetVenuesListByVenueId(VenueId As Integer) As List(Of SelectListItem)
    Function BindBookedClasses(orderId As Integer) As List(Of OrderDetailsViewModel)
    Function BindBookedOrders() As List(Of CreatedOrdersViewModel)
End Interface
