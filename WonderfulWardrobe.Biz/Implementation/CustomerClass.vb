﻿Imports WonderfulWardrobe.Data
Imports WonderfulWardrobe.Model
Imports AutoMapper
Imports System.Globalization
Imports System.Web
Imports System.Web.Security

Public Class CustomerClass
    Implements ICustomerClasses
    Private _ClassReg As IClassBooking
    Public Sub New(classregister As IClassBooking)
        _ClassReg = classregister
    End Sub

    Public Function BindVenues() As List(Of VenueViewModel) Implements ICustomerClasses.BindVenues
        Dim VenuesList As List(Of Venue)
        Mapper.CreateMap(Of Venue, VenueViewModel)()
        VenuesList = _ClassReg.BindVenues()
        Dim _Venues As List(Of VenueViewModel) = Mapper.Map(Of List(Of Venue), List(Of VenueViewModel))(VenuesList)
        Return _Venues
    End Function

    Public Function GetTermByVenueId(VenueId As Integer) As List(Of TermsViewModel) Implements ICustomerClasses.GetTermByVenueId
        Dim term As List(Of Terms)
        Dim _class As List(Of Classes)
        Mapper.CreateMap(Of Terms, TermsViewModel)()
        term = _ClassReg.GetTermByVenueId(VenueId)
        Dim _Term As List(Of TermsViewModel) = Mapper.Map(Of List(Of Terms), List(Of TermsViewModel))(term)
        For Each item As TermsViewModel In _Term
            _class = _ClassReg.GetClassByTermVenueId(item.TermId, VenueId)
            Mapper.CreateMap(Of Classes, ClassViewModel)()
            Dim _ClassList As List(Of ClassViewModel) = Mapper.Map(Of List(Of Classes), List(Of ClassViewModel))(_class)
            item.ClassList = _ClassList
        Next

        Return _Term
    End Function

    Public Function BindTermWiseClasses() As CustomerClassViewModel Implements ICustomerClasses.BindTermWiseClasses
        Dim viewModel As CustomerClassViewModel = New CustomerClassViewModel
        viewModel.TermsList = DirectCast(HttpContext.Current.Session("TermList"), List(Of TermsViewModel))
        Dim list As List(Of VenueViewModel)
        'Get All Venues From Classes
        list = BindVenues()
        viewModel.VenuesList = list
        HttpContext.Current.Session("TermList") = Nothing
        Return viewModel
    End Function

    Public Function GetClassByClassId(ClassId As Integer) As ClassBookingViewModel Implements ICustomerClasses.GetClassByClassId

        Dim _class As ClassesBooking = New ClassesBooking
        Mapper.CreateMap(Of ClassesBooking, ClassBookingViewModel)()
        _class = _ClassReg.GetClassByClassId(ClassId)
        Dim _clasDetails As ClassBookingViewModel = Mapper.Map(Of ClassesBooking, ClassBookingViewModel)(_class)

        Mapper.CreateMap(Of ClassesBooking, TermsViewModel)()
        Dim TermsList As TermsViewModel = Mapper.Map(Of ClassesBooking, TermsViewModel)(_class)

        Mapper.CreateMap(Of ClassesBooking, ClassViewModel)()
        Dim ClassList As ClassViewModel = Mapper.Map(Of ClassesBooking, ClassViewModel)(_class)

        _clasDetails.TermList = New List(Of TermsViewModel)
        _clasDetails.TermList.Add(TermsList)

        _clasDetails.CLassList = New List(Of ClassViewModel)
        _clasDetails.CLassList.Add(ClassList)

        Dim FamilyList As List(Of Family)
        Mapper.CreateMap(Of Family, FamilyViewModel)()
        Dim _User As MembershipUser
        _User = Membership.GetUser()
        FamilyList = _ClassReg.BindFamily(_User.ProviderUserKey)
        Dim _Family As List(Of FamilyViewModel) = Mapper.Map(Of List(Of Family), List(Of FamilyViewModel))(FamilyList)
        _clasDetails.family = _Family

        Return _clasDetails

    End Function

    Public Sub AddChild(family As FamilyViewModel) Implements ICustomerClasses.AddChild
        Mapper.CreateMap(Of FamilyViewModel, Family)()
        Dim _family As Family = Mapper.Map(Of FamilyViewModel, Family)(family)
        _ClassReg.AddChild(_family)
    End Sub

    Public Function GetFamily() As List(Of FamilyViewModel) Implements ICustomerClasses.GetFamily
        Dim FamilyList As List(Of Family)
        Mapper.CreateMap(Of Family, FamilyViewModel)()
        Dim _User As MembershipUser
        _User = Membership.GetUser()
        FamilyList = _ClassReg.BindFamily(_User.ProviderUserKey)
        Dim _Family As List(Of FamilyViewModel) = Mapper.Map(Of List(Of Family), List(Of FamilyViewModel))(FamilyList)
        Return _Family
    End Function

    Public Function BindClassByTermVenue(termId, venueId) As List(Of ClassViewModel) Implements ICustomerClasses.BindClassByTermVenue
        Dim ClassList As List(Of Classes)
        Mapper.CreateMap(Of Classes, ClassViewModel)()
        ClassList = _ClassReg.BindClassByTermVenue(termId, venueId)
        Dim _Classes As List(Of ClassViewModel) = Mapper.Map(Of List(Of Classes), List(Of ClassViewModel))(ClassList)
        Return _Classes
    End Function

    Public Function BindVenuebyId(VenueId As Integer) As VenueViewModel Implements ICustomerClasses.BindVenuebyId
        Dim _Venue As New Venue
        Mapper.CreateMap(Of Venue, VenueViewModel)()
        _Venue = _ClassReg.BindVenueById(VenueId)
        Dim _VenueDetails As VenueViewModel = Mapper.Map(Of Venue, VenueViewModel)(_Venue)
        Return _VenueDetails
    End Function
    Public Function BindTermbyId(TermId As Integer) As TermsViewModel Implements ICustomerClasses.BindTermbyId
        Dim _Terms As New Terms
        Mapper.CreateMap(Of Terms, TermsViewModel)()
        _Terms = _ClassReg.BindTermbyId(TermId)
        Dim _TermsDetails As TermsViewModel = Mapper.Map(Of Terms, TermsViewModel)(_Terms)
        Return _TermsDetails
    End Function

    Public Function SaveBookedClass(_classBooking As ClassBookingViewModel) As String Implements ICustomerClasses.SaveBookedClass
        Dim _Order As New Order
        Dim _User As MembershipUser
        _User = Membership.GetUser()
        _Order.OrderDate = DateTime.Today
        _Order.UserId = _User.ProviderUserKey
        Dim _orderId As String = _ClassReg.SaveOrder(_Order)

        Dim _Payment As New Payment
        _Payment.CreatedOn = DateTime.Today
        _Payment.IsActive = True
        _Payment.OrderId = _orderId
        If _classBooking.PaymentMode = "Cash" Then
            _Payment.PayAmount = 0

        Else
            _Payment.PayAmount = Convert.ToDecimal(_classBooking.PaidAmount)
            _Payment.TransactionId = _classBooking.TransactionId
        End If

        If _classBooking.BookingType = "term" Then
            _Payment.PaymentFor = "Term"
        Else
            _Payment.PaymentFor = "Class"
        End If

        _Payment.PaymentType = _classBooking.PaymentMode
        _Payment.TotalAmount = _classBooking.TotalAmount
        _Payment.UserId = _User.ProviderUserKey
        _ClassReg.SavePayment(_Payment)

        Dim _bookedClass As New BookedClass

        For Each items As ClassBookingViewModel In _classBooking.BookedClassList

            For Each item As ClassViewModel In items.CLassList
                Dim _class As New BookedClass
                Dim selected As String() = _classBooking.Logo.Split(",")
                Dim data As String = ""
                For Each child As String In selected
                    Dim venuechild As String() = child.Split("-")
                    If venuechild(1) = item.VenueId Then
                        data += venuechild(0).ToString() + ","
                    End If
                Next
                Dim length = data.Length
                data = data.Remove(length - 1)
                Dim itemchild As String() = data.Split(",")
                _class.ClassId = item.ClassId
                _class.OrderId = Convert.ToInt32(_orderId)
                For value As Integer = 0 To itemchild.Length - 1
                    _class.ChildId = itemchild(value)
                    _ClassReg.SaveBookedClass(_class)
                Next
            Next
        Next
        Return "Success"
    End Function
End Class
