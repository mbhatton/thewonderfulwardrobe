﻿Imports WonderfulWardrobe.Data
Imports WonderfulWardrobe.Model
Imports AutoMapper
Imports System.Web.Security

Public Class Account
    Implements IAccount
    Private _Account As IAccountRegister
    Public Sub New(account As IAccountRegister)
        _Account = account
    End Sub
    Public Function RegisterFamily(_AccountViewModel As AccountViewModel) As String Implements IAccount.RegisterFamily
       
        Mapper.CreateMap(Of AccountViewModel, Parent)()
        Dim _parent As Parent = Mapper.Map(Of AccountViewModel, Parent)(_AccountViewModel)
        Mapper.CreateMap(Of FamilyViewModel, Family)()
        Dim _family As List(Of Family) = Mapper.Map(Of List(Of FamilyViewModel), List(Of Family))(_AccountViewModel.family)
        Dim status As MembershipCreateStatus = _Account.Register(_parent)
        If status = MembershipCreateStatus.DuplicateEmail OrElse status = MembershipCreateStatus.DuplicateUserName Then
            Return "Fail"
        Else
            _Account.CreateUserRole(_parent.EmailId)
            Dim role() = Roles.GetRolesForUser()
            If role(0) = "SuperAdmin" Then
                Return "Success"
            Else
                _Account.SaveParentDetails(_parent)
                _Account.SaveFamilyDetails(_family)
                Return "Success"
            End If

           
        End If

    End Function

   
    Public Function Login(_AccountViewModel As AccountViewModel) As String Implements IAccount.Login
        Mapper.CreateMap(Of AccountViewModel, Parent)()
        Dim _parent As Parent = Mapper.Map(Of AccountViewModel, Parent)(_AccountViewModel)
        Dim Status As Boolean = _Account.Login(_parent)
        If Status = False Then
            Return "Fail"
        Else
            Dim Role As String = _Account.SetAuthCookieMain(_parent.EmailId)
            Return Role
        End If

    End Function

    Public Function BindAdminList() As List(Of AccountViewModel) Implements IAccount.BindAdminList
        Dim AdminList As List(Of Users)
        Mapper.CreateMap(Of Users, AccountViewModel)()
        AdminList = _Account.BindAdminList()
        Dim _Admin As List(Of AccountViewModel) = Mapper.Map(Of List(Of Users), List(Of AccountViewModel))(AdminList)
        Return _Admin
    End Function

    
End Class
