﻿Imports WonderfulWardrobe.Data
Imports WonderfulWardrobe.Model
Imports AutoMapper
Imports System.Web.Mvc
Imports System.Web
Imports System.Globalization
Imports GoogleMaps.LocationServices

Public Class AdminClass
    Implements IAdminClass
    Private _AdminClassReg As IAdminClassRegistration

    Public Sub New(adminclassregister As IAdminClassRegistration)
        _AdminClassReg = adminclassregister
    End Sub
    Public Sub CreateTerms(_TermsViewModel As TermsViewModel) Implements IAdminClass.CreateTerms
        Mapper.CreateMap(Of TermsViewModel, Terms)()
        Dim _terms As Terms = Mapper.Map(Of TermsViewModel, Terms)(_TermsViewModel)
        _AdminClassReg.CreateTerms(_terms)
    End Sub

    Public Sub CreateVenues(_VenueViewModel As VenueViewModel) Implements IAdminClass.CreateVenues
        Mapper.CreateMap(Of VenueViewModel, Venue)()
        Dim _venues As Venue = Mapper.Map(Of VenueViewModel, Venue)(_VenueViewModel)
        Dim address As String = _VenueViewModel.Name + " " + _VenueViewModel.Address1 + " " + _VenueViewModel.Address2 + " " + _VenueViewModel.Address3 + " " + _VenueViewModel.City
        Dim locationService = New GoogleLocationService()
        Dim point = locationService.GetLatLongFromAddress(address)
        _venues.Longitude = point.Longitude.ToString()
        _venues.Latitude = point.Latitude.ToString()
        _AdminClassReg.CreateVenues(_venues)

    End Sub

    Public Sub CreateClasses(_ClassViewModel As ClassViewModel) Implements IAdminClass.CreateClasses

        Mapper.CreateMap(Of ClassViewModel, Classes)()
        Dim _classes As Classes = Mapper.Map(Of ClassViewModel, Classes)(_ClassViewModel)
        Dim currentdate = DateTime.Parse(_classes.StartDateTime, CultureInfo.InvariantCulture)
        Dim currentdayofweek = DateTime.Parse(_classes.StartDateTime, CultureInfo.InvariantCulture).DayOfWeek.ToString()
        If _classes.Weeks > 0 Then

            ' Exit condition if the value is three.
            Dim list As List(Of VenueViewModel)
            'Get All Venues From Classes
            list = BindVenues()
            'Get the selected venue details
            Dim VenueDetails = list.Find(Function(item) item.VenueId = _classes.VenueId)
            Dim venueday = VenueDetails.Day.ToString()
            If currentdayofweek = venueday Then
                For value As Integer = 1 To _classes.Weeks
                    _classes.StartDateTime = currentdate
                    _AdminClassReg.CreateClasses(_classes)
                    currentdate = currentdate.AddDays(7.0)
                Next

            Else
                Dim day As Integer
                If venueday = "Monday" Then
                    day = 1
                ElseIf venueday = "Tuesday" Then
                    day = 2
                ElseIf venueday = "Wednesday" Then
                    day = 3
                ElseIf venueday = "Thursday" Then
                    day = 4
                ElseIf venueday = "Friday" Then
                    day = 5
                ElseIf venueday = "Saturday" Then
                    day = 6
                Else
                    day = 7
                End If
                Dim startdate = DateTime.Parse(_classes.StartDateTime, CultureInfo.InvariantCulture)
                Dim nextdate As DateTime = GetNextWeekday(startdate, day)
                currentdate = nextdate
                For value As Integer = 1 To _classes.Weeks
                    _classes.StartDateTime = currentdate
                    _AdminClassReg.CreateClasses(_classes)
                    currentdate = currentdate.AddDays(7.0)
                Next
                'Get Next Date for current venue day

            End If

        End If


    End Sub
    Public Shared Function GetNextWeekday(start As DateTime, day As Integer) As DateTime
        ' The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
        Dim daysToAdd As Integer = (CInt(day) - CInt(start.DayOfWeek) + 7) Mod 7
        Return start.AddDays(daysToAdd)
    End Function

    Public Function GetTermsList() As List(Of SelectListItem) Implements IAdminClass.GetTermsList
        Dim TermsList As List(Of Terms)
        Mapper.CreateMap(Of Terms, TermsViewModel)()
        TermsList = _AdminClassReg.GetTermsList()
        Dim _Terms As List(Of TermsViewModel) = Mapper.Map(Of List(Of Terms), List(Of TermsViewModel))(TermsList)
        Dim list As New List(Of SelectListItem)()
        list.Add(New SelectListItem() With { _
     .Text = "-Please select-", _
     .Value = "0", _
     .Selected = True _
})
        For Each term As TermsViewModel In _Terms
            list.Add(New SelectListItem() With { _
                .Text = term.Name, _
                .Value = term.TermId.ToString() _
           })
        Next
        Return list
    End Function

    Public Function GetTermByTermId(TermId As Integer) As TermsViewModel Implements IAdminClass.GetTermByTermId
        Dim term As Terms
        Mapper.CreateMap(Of Terms, TermsViewModel)()
        term = _AdminClassReg.GetTermDetailById(TermId)
        Dim _Term As TermsViewModel = Mapper.Map(Of Terms, TermsViewModel)(term)
        Return _Term
    End Function

    Public Function GetVenuesList() As List(Of SelectListItem) Implements IAdminClass.GetVenuesList
        Dim VenuesList As List(Of Venue)
        Mapper.CreateMap(Of Venue, VenueViewModel)()
        VenuesList = _AdminClassReg.BindVenues()
        Dim _Venues As List(Of VenueViewModel) = Mapper.Map(Of List(Of Venue), List(Of VenueViewModel))(VenuesList)
        Dim list As New List(Of SelectListItem)()
        list.Add(New SelectListItem() With { _
     .Text = "-Please select-", _
     .Value = "0", _
     .Selected = True _
})
        For Each venue As VenueViewModel In _Venues
            list.Add(New SelectListItem() With { _
                .Text = venue.Name, _
                .Value = venue.VenueId.ToString() _
           })
        Next
        Return list
    End Function

    Public Function BindTerms() As List(Of TermsViewModel) Implements IAdminClass.BindTerms
        Dim TermsList As List(Of Terms)
        Mapper.CreateMap(Of Terms, TermsViewModel)()
        TermsList = _AdminClassReg.BindTerms()
        Dim _Terms As List(Of TermsViewModel) = Mapper.Map(Of List(Of Terms), List(Of TermsViewModel))(TermsList)
        Return _Terms
    End Function

    Public Sub DeleteTerm(TermId As Integer) Implements IAdminClass.DeleteTerm
        _AdminClassReg.DeleteTerms(TermId)
    End Sub

    Public Sub UpdateTerm(_TermsViewModel As TermsViewModel) Implements IAdminClass.UpdateTerm
        Mapper.CreateMap(Of TermsViewModel, Terms)()
        Dim _terms As Terms = Mapper.Map(Of TermsViewModel, Terms)(_TermsViewModel)
        _AdminClassReg.UpdateTerms(_terms)
    End Sub

    Public Function BindVenues() As List(Of VenueViewModel) Implements IAdminClass.BindVenues
        Dim VenuesList As List(Of Venue)
        Mapper.CreateMap(Of Venue, VenueViewModel)()
        VenuesList = _AdminClassReg.BindVenues()
        Dim _Venues As List(Of VenueViewModel) = Mapper.Map(Of List(Of Venue), List(Of VenueViewModel))(VenuesList)
        Return _Venues
    End Function

    Public Sub DeleteVenue(VenueId As Integer) Implements IAdminClass.DeleteVenue
        _AdminClassReg.DeleteVenues(VenueId)
    End Sub

    Public Sub UpdateVenue(_VenueViewModel As VenueViewModel) Implements IAdminClass.UpdateVenue
        Mapper.CreateMap(Of VenueViewModel, Venue)()
        Dim _venue As Venue = Mapper.Map(Of VenueViewModel, Venue)(_VenueViewModel)
        _AdminClassReg.UpdateVenues(_venue)
    End Sub

    Public Function GetVenueByVenueId(VenueId As Integer) As VenueViewModel Implements IAdminClass.GetVenueByVenueId
        Dim venue As Venue
        Mapper.CreateMap(Of Venue, VenueViewModel)()
        venue = _AdminClassReg.GetVenueDetailById(VenueId)
        Dim _Venue As VenueViewModel = Mapper.Map(Of Venue, VenueViewModel)(venue)
        Return _Venue
    End Function

    Public Function BindClasses() As List(Of ClassViewModel) Implements IAdminClass.BindClasses
        Dim ClassList As List(Of Classes)
        Mapper.CreateMap(Of Classes, ClassViewModel)()
        ClassList = _AdminClassReg.BindClasses()
        Dim _Classes As List(Of ClassViewModel) = Mapper.Map(Of List(Of Classes), List(Of ClassViewModel))(ClassList)
        Return _Classes
    End Function

    Public Sub DeleteClass(ClassId As Integer) Implements IAdminClass.DeleteClass
        _AdminClassReg.DeleteClasses(ClassId)
    End Sub

    Public Sub UpdateClass(_ClassViewModel As ClassViewModel) Implements IAdminClass.UpdateClass
        Mapper.CreateMap(Of ClassViewModel, Classes)()
        Dim _class As Classes = Mapper.Map(Of ClassViewModel, Classes)(_ClassViewModel)
        _AdminClassReg.UpdateClasses(_class)
    End Sub

    Public Function GetClassByClassId(ClassId As Integer) As ClassViewModel Implements IAdminClass.GetClassByClassId
        Dim cls As Classes
        Mapper.CreateMap(Of Classes, ClassViewModel)()
        cls = _AdminClassReg.GetClassDetailById(ClassId)
        Dim _Class As ClassViewModel = Mapper.Map(Of Classes, ClassViewModel)(cls)
        Return _Class
    End Function

    Public Function GetClassByTermId(TermId As Integer) As List(Of ClassViewModel) Implements IAdminClass.GetClassByTermId
        Dim cls As List(Of Classes)
        Mapper.CreateMap(Of Classes, ClassViewModel)()
        cls = _AdminClassReg.GetClassDetailByTermId(TermId)
        Dim _Class As List(Of ClassViewModel) = Mapper.Map(Of List(Of Classes), List(Of ClassViewModel))(cls)
        Return _Class
    End Function

    Public Function GetClassByVenueId(VenueId As Integer) As List(Of ClassViewModel) Implements IAdminClass.GetClassByVenueId
        Dim cls As List(Of Classes)
        Mapper.CreateMap(Of Classes, ClassViewModel)()
        cls = _AdminClassReg.GetClassDetailByVenueId(VenueId)
        Dim _Class As List(Of ClassViewModel) = Mapper.Map(Of List(Of Classes), List(Of ClassViewModel))(cls)

        Return _Class
    End Function

    Public Function GetTermsListByTermId(TermId As Integer) As List(Of SelectListItem) Implements IAdminClass.GetTermsListByTermId

        Dim TermsList As List(Of Terms)
        Mapper.CreateMap(Of Terms, TermsViewModel)()
        TermsList = _AdminClassReg.GetTermsList()
        Dim _Terms As List(Of TermsViewModel) = Mapper.Map(Of List(Of Terms), List(Of TermsViewModel))(TermsList)
        Dim list As New List(Of SelectListItem)()
        list.Add(New SelectListItem() With { _
     .Text = "-Please select-", _
     .Value = "0" _
})
        For Each term As TermsViewModel In _Terms

            If term.TermId = TermId Then
                list.Add(New SelectListItem() With { _
                .Text = term.Name, _
                .Value = term.TermId.ToString(), _
                 .Selected = True _
           })

            Else
                list.Add(New SelectListItem() With { _
                .Text = term.Name, _
                .Value = term.TermId.ToString() _
           })
            End If

        Next
        Return list
    End Function

    Public Function GetVenuesListByVenueId(VenueId As Integer) As List(Of SelectListItem) Implements IAdminClass.GetVenuesListByVenueId

        Dim VenuesList As List(Of Venue)
        Mapper.CreateMap(Of Venue, VenueViewModel)()
        VenuesList = _AdminClassReg.BindVenues()
        Dim _Venues As List(Of VenueViewModel) = Mapper.Map(Of List(Of Venue), List(Of VenueViewModel))(VenuesList)
        Dim list As New List(Of SelectListItem)()
        list.Add(New SelectListItem() With { _
     .Text = "-Please select-", _
     .Value = "0" _
})
        For Each venue As VenueViewModel In _Venues

            If venue.VenueId = VenueId Then
                list.Add(New SelectListItem() With { _
                 .Text = venue.Name, _
                 .Value = venue.VenueId.ToString(), _
                  .Selected = True _
            })
            Else
                list.Add(New SelectListItem() With { _
                .Text = venue.Name, _
                .Value = venue.VenueId.ToString() _
           })
            End If

        Next
        Return list
    End Function


    Public Function BindBookedClasses(orderId As Integer) As List(Of OrderDetailsViewModel) Implements IAdminClass.BindBookedClasses
        Dim ClassList As List(Of OrderDetails)
        Mapper.CreateMap(Of OrderDetails, OrderDetailsViewModel)()
        ClassList = _AdminClassReg.BindBookedClasses(orderId)
        Dim _Class As List(Of OrderDetailsViewModel) = Mapper.Map(Of List(Of OrderDetails), List(Of OrderDetailsViewModel))(ClassList)
        Return _Class
    End Function

    Public Function BindBookedOrders() As List(Of CreatedOrdersViewModel) Implements IAdminClass.BindBookedOrders
        Dim OrderList As List(Of CreatedOrders)
        Mapper.CreateMap(Of CreatedOrders, CreatedOrdersViewModel)()
        OrderList = _AdminClassReg.BindBookedOrders()
        Dim _Orders As List(Of CreatedOrdersViewModel) = Mapper.Map(Of List(Of CreatedOrders), List(Of CreatedOrdersViewModel))(OrderList)
        Return _Orders
    End Function
End Class
