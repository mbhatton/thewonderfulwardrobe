﻿Imports AutoMapper
Imports WonderfulWardrobe.Model
Imports WonderfulWardrobe.Data
Imports System.Net.Mail
Imports System.Web.Security

Public Class Parties
    Implements IParties
    Private _Parties As IPartyBooking

    Public Sub New(parties As IPartyBooking)
        _Parties = parties
    End Sub

    Public Sub CreatePartyEnquiry(_enquiry As PartyEnquiryViewModel) Implements IParties.CreatePartyEnquiry
        Mapper.CreateMap(Of PartyEnquiryViewModel, PartyEnquiry)()
        Dim _enquiryDetails As PartyEnquiry = Mapper.Map(Of PartyEnquiryViewModel, PartyEnquiry)(_enquiry)
        _enquiryDetails.IsActive = True
        _enquiryDetails.CreatedOn = DateTime.Today
        _Parties.CreatePartyEnquiry(_enquiryDetails)

        Dim message As New System.Net.Mail.MailMessage()
        Dim Email As String = "gurpinderpalsingh88@gmail.com"
        message.[To].Add(Email)
        message.Subject = "Party Booking Enquiry From " & _enquiryDetails.Email
        message.From = New System.Net.Mail.MailAddress("dotnetmailtesting@gmail.com")
        message.IsBodyHtml = True
        Dim strBody As String = "<table><tr><td><h3>Party Enquiry From :" & _enquiryDetails.Email & "</h3>  </td>  </tr><tr><td colspan=""4"" align=""left"">Host Name:- " & _enquiryDetails.Name & "</td></tr><tr><td colspan=""4"" align=""left"">Email:- " & _enquiryDetails.Email & "</td></tr><tr><td colspan=""4"" align=""left"">Phone Number:- " & _enquiryDetails.PhoneNumber & "</td></tr>"
        strBody += "<tr><td colspan=""4"" align=""left"">1st Prefered Date:- " & _enquiryDetails.PreferedDate1.ToString("MMM dd yyy") & "</td></tr><tr><td colspan=""4"" align=""left"">2nd Prefered Date:- " & _enquiryDetails.PreferedDate2.ToString("MMM dd yyy") & "</td></tr><tr><td colspan=""4"" align=""left"">Prefered Start Time:- " & _enquiryDetails.PreferedStartTime & "</td></tr>"
        strBody += "<tr><td align=""left"">Please check this party booking enquiry.</td></tr>"
        strBody += "</table>"
        message.Body = strBody
        Dim smtp As New System.Net.Mail.SmtpClient("smtp.gmail.com")
        smtp.Credentials = New System.Net.NetworkCredential("dotnetmailtesting@gmail.com", "mss10008")
        smtp.EnableSsl = True
        smtp.Port = 587
        smtp.Send(message)

    End Sub

    Public Function BindPartyEnquiryList() As List(Of PartyEnquiryViewModel) Implements IParties.BindPartyEnquiryList
        Dim EnqiryList As List(Of PartyEnquiry)
        Mapper.CreateMap(Of PartyEnquiry, PartyEnquiryViewModel)()
        EnqiryList = _Parties.BindPartyEnquiryList()
        Dim _Enquiry As List(Of PartyEnquiryViewModel) = Mapper.Map(Of List(Of PartyEnquiry), List(Of PartyEnquiryViewModel))(EnqiryList)
        Return _Enquiry
    End Function

    Public Function EnquiryDetails(enquiryId As Integer) As PartyEnquiryViewModel Implements IParties.EnquiryDetails
        Dim enquiryDetail As PartyEnquiry
        Mapper.CreateMap(Of PartyEnquiry, PartyEnquiryViewModel)()
        enquiryDetail = _Parties.EnquiryDetails(enquiryId)
        Dim _enquiry As PartyEnquiryViewModel = Mapper.Map(Of PartyEnquiry, PartyEnquiryViewModel)(enquiryDetail)
        Return _enquiry
    End Function

    Public Sub SaveSuggestion(_enquiry As PartyEnquiryViewModel) Implements IParties.SaveSuggestion
        Mapper.CreateMap(Of PartyEnquiryViewModel, Suggstion)()
        Dim _partyenquiry As List(Of Suggstion) = Mapper.Map(Of List(Of PartyEnquiryViewModel), List(Of Suggstion))(_enquiry.PartyEnquiryList)
        Dim enquiryid = _enquiry.EnquiryId
        _Parties.SaveSuggestion(_partyenquiry, enquiryid)
        _Parties.UpdateEnquiry(enquiryid)
        Dim link As String = "http://localhost:2293/Parties/BookParty?EnquiryId=" + enquiryid.ToString()
        'Email To...
        Dim EmailAddress = _enquiry.Email
     
        'Send Mail
        Dim message As New System.Net.Mail.MailMessage()
        Dim Email As String = "gurpinderpalsingh88@gmail.com"
        message.[To].Add(Email)
        message.Subject = "Party Booking Suggestion From dotnetmailtesting@gmail.com"
        message.From = New System.Net.Mail.MailAddress("dotnetmailtesting@gmail.com")
        message.IsBodyHtml = True
        Dim strBody As String = "<table><tr><td><h3>Your Party Suggestion From : the Wonderful Wardrobe</h3>  </td>  </tr><tr><td colspan=""4"" align=""left""> " & link & "</td></tr><tr><td align=""left"">Please follow this link to book your party.</td></tr>"
        strBody += "</table>"
        message.Body = strBody
        Dim smtp As New System.Net.Mail.SmtpClient("smtp.gmail.com")
        smtp.Credentials = New System.Net.NetworkCredential("dotnetmailtesting@gmail.com", "mss10008")
        smtp.EnableSsl = True
        smtp.Port = 587
        smtp.Send(message)


        'Dim Mail As String = "gurpinderpalsingh88@gmail.com"
        'Const SERVER As String = "relay-hosting.secureserver.net"
        'Dim oMail As New MailMessage()
        'oMail.From = New MailAddress(Mail)
        'oMail.[To].Add(New MailAddress("gurpinderpalsingh88@gmail.com"))
        'oMail.Subject = "Party Booking Suggestion From " & Mail
        'oMail.IsBodyHtml = True
        'oMail.Priority = MailPriority.High

        'Dim strBody As String = "<table><tr><td><h3>Your Party Suggestion From : the Wonderful Wardrobe</h3>  </td>  </tr><tr><td colspan=""4"" align=""left""> " & link & "</td></tr><tr><td align=""left"">Please follow this link to book your party.</td></tr>"
        'strBody += "</table>"
        '' email's body
        'Dim smtpClient As New SmtpClient(SERVER)
        'smtpClient.Send(oMail)
        'oMail = Nothing
    End Sub
     
    Public Function BindprocessedEnquiryList() As List(Of PartyEnquiryViewModel) Implements IParties.BindprocessedEnquiryList
        Dim EnqiryList As List(Of PartyEnquiry)
        Mapper.CreateMap(Of PartyEnquiry, PartyEnquiryViewModel)()
        EnqiryList = _Parties.BindprocessedEnquiryList()
        Dim _Enquiry As List(Of PartyEnquiryViewModel) = Mapper.Map(Of List(Of PartyEnquiry), List(Of PartyEnquiryViewModel))(EnqiryList)
        Return _Enquiry
    End Function

    Public Function ProcessedEnquiryDetails(enquiryId As Integer) As List(Of PartyEnquiryViewModel) Implements IParties.ProcessedEnquiryDetails
        Dim enquiryDetail As List(Of Suggstion)
        Mapper.CreateMap(Of Suggstion, PartyEnquiryViewModel)()
        enquiryDetail = _Parties.ProcessedEnquiryDetails(enquiryId)
        Dim _enquiry As List(Of PartyEnquiryViewModel) = Mapper.Map(Of List(Of Suggstion), List(Of PartyEnquiryViewModel))(enquiryDetail)
        Return _enquiry
    End Function

    Public Sub SaveParty(_ViewModel As PartyBookingViewModel) Implements IParties.SaveParty

        Dim _Order As New Order
        Dim _User As MembershipUser
        _User = Membership.GetUser()
        _Order.OrderDate = DateTime.Today
        _Order.UserId = _User.ProviderUserKey
        Dim _orderId As String = _Parties.SaveOrder(_Order)

        Dim _Payment As New Payment
        _Payment.CreatedOn = DateTime.Today
        _Payment.IsActive = True
        _Payment.OrderId = _orderId
        If _ViewModel.PaymentMode = "Cash" Then
            _Payment.PayAmount = 0

        Else
            _Payment.PayAmount = Convert.ToDecimal(_ViewModel.PaidAmount)
            _Payment.TransactionId = _ViewModel.TransactionId
        End If
        _Payment.PaymentFor = "Party"
        _Payment.PaymentType = _ViewModel.PaymentMode
        _Payment.TotalAmount = _ViewModel.TotalAmount
        _Payment.UserId = _User.ProviderUserKey
        _Parties.SavePayment(_Payment)


        Mapper.CreateMap(Of PartyBookingViewModel, PartyHostDetails)()
        Dim _host As PartyHostDetails = Mapper.Map(Of PartyBookingViewModel, PartyHostDetails)(_ViewModel)
        _host.HostId = _User.ProviderUserKey.ToString()
        _Parties.SaveHostDetails(_host)

        Mapper.CreateMap(Of FamilyViewModel, Family)()
        Dim _family As List(Of Family) = Mapper.Map(Of List(Of FamilyViewModel), List(Of Family))(_ViewModel.family)
        For Each item As Family In _family
            Dim partychilds As New PartyChildrenDetails
            partychilds.ChildId = item.Id
            partychilds.OrderId = _orderId
            _Parties.SavePartyChildren(partychilds)
        Next

        Dim partyList As New List(Of Party)

        For Each item As PartyEnquiryViewModel In _ViewModel.SuggestionList
            Dim _party As New Party
            _party.CreatedOn = DateTime.Today
            _party.Duration = item.SuggestedDuration
            _party.OrderId = _orderId
            _party.HostId = _User.ProviderUserKey
            _party.IsActive = True
            _party.Location = _ViewModel.PartyLocation
            _party.NoOfChildren = _ViewModel.ChildrenAttending
            _party.PartyDate = item.SuggestedDate
            _party.Price = item.Price
            _party.Reason = _ViewModel.PartyReason
            _party.Time = item.SuggestedTime
            partyList.Add(_party)
        Next

        _Parties.SaveParty(partyList)


    End Sub
    Public Function BindBookedPartyList() As List(Of PartyViewModel) Implements IParties.BindBookedPartyList
        Dim PartyList As List(Of Party)
        Mapper.CreateMap(Of Party, PartyViewModel)()
        PartyList = _Parties.BindBookedPartyList()
        Dim _party As List(Of PartyViewModel) = Mapper.Map(Of List(Of Party), List(Of PartyViewModel))(PartyList)
        Return _party
    End Function


    Public Function PartyDetailsbyId(partyId As Integer) As PartyBookingViewModel Implements IParties.PartyDetailsbyId
        Dim partyDetail As Party
        Mapper.CreateMap(Of Party, PartyViewModel)()
        partyDetail = _Parties.PartyDetails(partyId)
        Dim _party As PartyViewModel = Mapper.Map(Of Party, PartyViewModel)(partyDetail)

        Dim partyChildren As List(Of PartyChildrenDetails)
        partyChildren = _Parties.partyChildren(_party.OrderId)

        Dim familyDetails As New List(Of Family)

        For Each child As PartyChildrenDetails In partyChildren
            Dim childDetail As Family
            childDetail = _Parties.ChildDetails(Convert.ToInt32(child.ChildId))
            familyDetails.Add(childDetail)
        Next
        Mapper.CreateMap(Of Family, FamilyViewModel)()
        Dim _family As List(Of FamilyViewModel) = Mapper.Map(Of List(Of Family), List(Of FamilyViewModel))(familyDetails)

        Dim host As PartyHostDetails
        host = _Parties.HostDetails(_party.HostId)

        Dim partybooking As New PartyBookingViewModel
        partybooking.family = _family
        partybooking.PartyList = New List(Of PartyViewModel)
        partybooking.PartyList.Add(_party)
        partybooking.FirstName = host.FirstName
        partybooking.LastName = host.LastName
        partybooking.Address1 = host.Address1
        partybooking.Address2 = host.Address2
        partybooking.Address3 = host.Address3
        partybooking.TelePhone = host.TelePhone

        Return partybooking
    End Function

    Public Function AddChild(family As FamilyViewModel) As String Implements IParties.AddChild
        Mapper.CreateMap(Of FamilyViewModel, Family)()
        Dim _family As Family = Mapper.Map(Of FamilyViewModel, Family)(family)
        Dim childId As String = _Parties.AddChild(_family)
        Return childId
    End Function

    Public Function GetFamily() As List(Of FamilyViewModel) Implements IParties.GetFamily
        Dim FamilyList As List(Of Family)
        Mapper.CreateMap(Of Family, FamilyViewModel)()
        Dim _User As MembershipUser
        _User = Membership.GetUser()
        FamilyList = _Parties.BindFamily(_User.ProviderUserKey)
        Dim _Family As List(Of FamilyViewModel) = Mapper.Map(Of List(Of Family), List(Of FamilyViewModel))(FamilyList)
        Return _Family
    End Function

    Public Function GetChildInfo(childId As String) As FamilyViewModel Implements IParties.GetChildInfo
        Dim childDetail As Family
        Mapper.CreateMap(Of Family, FamilyViewModel)()
        childDetail = _Parties.GetChildInfo(childId)
        Dim _details As FamilyViewModel = Mapper.Map(Of Family, FamilyViewModel)(childDetail)
        Return _details
    End Function
End Class
