﻿Imports WonderfulWardrobe.Model
Imports AutoMapper
Imports WonderfulWardrobe.Data

Public Class AdminPayment
    Implements IAdminPayment
    Private _AdminPaymentDetails As IAdminPaymentDetails

    Public Sub New(adminpaymentdetails As IAdminPaymentDetails)
        _AdminPaymentDetails = adminpaymentdetails
    End Sub

    Public Function BindPayments() As List(Of PaymentViewModel) Implements IAdminPayment.BindPayments
        Dim PaymentsList As List(Of Payment)
        Mapper.CreateMap(Of Payment, PaymentViewModel)()
        PaymentsList = _AdminPaymentDetails.BindPayments()
        Dim _Payment As List(Of PaymentViewModel) = Mapper.Map(Of List(Of Payment), List(Of PaymentViewModel))(PaymentsList)
        Return _Payment
    End Function
End Class
