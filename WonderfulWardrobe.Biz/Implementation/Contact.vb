﻿Imports WonderfulWardrobe.Data
Imports AutoMapper
Imports WonderfulWardrobe.Model

Public Class Contact
    Implements IContact
    Private _ContactUs As IContactUs

    Public Sub New(contactus As IContactUs)
        _ContactUs = contactus
    End Sub

    Public Sub CreateContact(_ContactViewModel As ContactViewModel) Implements IContact.CreateContact
        Mapper.CreateMap(Of ContactViewModel, Contacts)()
        Dim _contactDetails As Contacts = Mapper.Map(Of ContactViewModel, Contacts)(_ContactViewModel)
        _contactDetails.IsActive = True
        _contactDetails.CreatedOn = DateTime.Today
        _ContactUs.CreateContact(_contactDetails)

    End Sub

    Public Function BindContactsList() As List(Of ContactViewModel) Implements IContact.BindContactsList
        Dim ContactList As List(Of Contacts)
        Mapper.CreateMap(Of Contacts, ContactViewModel)()
        ContactList = _ContactUs.BindContactsList()
        Dim _Contacts As List(Of ContactViewModel) = Mapper.Map(Of List(Of Contacts), List(Of ContactViewModel))(ContactList)
        Return _Contacts
    End Function

    Public Function ContactInfo(contactId As Integer) As ContactViewModel Implements IContact.ContactInfo
        Dim Info As Contacts
        Mapper.CreateMap(Of Contacts, ContactViewModel)()
        Info = _ContactUs.ContactInfo(contactId)
        Dim _info As ContactViewModel = Mapper.Map(Of Contacts, ContactViewModel)(Info)
        Return _info
    End Function
End Class
