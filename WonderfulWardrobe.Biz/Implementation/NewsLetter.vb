﻿Imports WonderfulWardrobe.Data
Imports AutoMapper
Imports System.Web.Security
Imports System.Threading
Imports WonderfulWardrobe.Model
Imports System.ComponentModel

Public Class NewsLetter
    Implements INewsLetter

    Private _NewsLetters As INewsLetters

    Public Sub New(_news As INewsLetters)
        _NewsLetters = _news

    End Sub


    Public Function BindNewsLettersUser() As List(Of NewsLetterViewModel) Implements INewsLetter.BindNewsLettersUser
        Dim NewsLetterList As List(Of Model.NewsLetter)
        Mapper.CreateMap(Of Model.NewsLetter, NewsLetterViewModel)()
        NewsLetterList = _NewsLetters.BindNewsLettersUser()
        Dim _letterlist As List(Of NewsLetterViewModel) = Mapper.Map(Of List(Of Model.NewsLetter), List(Of NewsLetterViewModel))(NewsLetterList)
        Return _letterlist
    End Function

    Public Sub RegisterNewsLetter(_ViewModel As NewsLetterViewModel) Implements INewsLetter.RegisterNewsLetter
        Mapper.CreateMap(Of NewsLetterViewModel, Model.NewsLetter)()
        Dim _newsletter As Model.NewsLetter = Mapper.Map(Of NewsLetterViewModel, Model.NewsLetter)(_ViewModel)
        _NewsLetters.RegisterNewsLetter(_newsletter)

    End Sub
    Public Sub DeleteUser(Id As Integer) Implements INewsLetter.DeleteUser
        _NewsLetters.DeleteUser(Id)
    End Sub

    Public Sub SaveNewsLetter(_ViewModel As NewsLetterViewModel) Implements INewsLetter.SaveNewsLetter
        Dim user As MembershipUser
        user = Membership.GetUser()
        Dim userID As Guid = user.ProviderUserKey
        Mapper.CreateMap(Of NewsLetterViewModel, Model.NewsLetterDetail)()
        Dim _newsletter As Model.NewsLetterDetail = Mapper.Map(Of NewsLetterViewModel, Model.NewsLetterDetail)(_ViewModel)
        _newsletter.CreatedBy = userID
        'U is for UnPublished
        _newsletter.IsPublish = False
        _newsletter.CreatedOn = DateTime.Now
        _newsletter.IsActive = True
        _NewsLetters.SaveNewsLetter(_newsletter)
    End Sub

    Public Function BindNewsLettersList() As List(Of NewsLetterViewModel) Implements INewsLetter.BindNewsLettersList
        Dim NewsLetterList As List(Of Model.NewsLetterDetail)
        Mapper.CreateMap(Of Model.NewsLetterDetail, NewsLetterViewModel)()
        NewsLetterList = _NewsLetters.BindNewsLettersList()
        Dim _letterlist As List(Of NewsLetterViewModel) = Mapper.Map(Of List(Of Model.NewsLetterDetail), List(Of NewsLetterViewModel))(NewsLetterList)
        Return _letterlist
    End Function

    Public Sub DeleteNewsLetter(Id As Integer) Implements INewsLetter.DeleteNewsLetter
        _NewsLetters.DeleteNewsLetter(Id)
    End Sub

    Public Function GetNewsLetterDetail(Id As Integer) As NewsLetterViewModel Implements INewsLetter.GetNewsLetterDetail
        Dim _NewsLetter As Model.NewsLetterDetail
        Mapper.CreateMap(Of Model.NewsLetterDetail, NewsLetterViewModel)()
        _NewsLetter = _NewsLetters.GetNewsLetterDetail(Id)
        Dim _letter As NewsLetterViewModel = Mapper.Map(Of Model.NewsLetterDetail, NewsLetterViewModel)(_NewsLetter)
        Return _letter
    End Function
    Public Sub UpdateNewsLetter(_ViewModel As NewsLetterViewModel, Id As Integer) Implements INewsLetter.UpdateNewsLetter
        Dim user As MembershipUser
        user = Membership.GetUser()
        Dim userID As Guid = user.ProviderUserKey
        Mapper.CreateMap(Of NewsLetterViewModel, Model.NewsLetterDetail)()
        Dim _newsletter As Model.NewsLetterDetail = Mapper.Map(Of NewsLetterViewModel, Model.NewsLetterDetail)(_ViewModel)
        _newsletter.CreatedBy = userID
        _newsletter.IsPublish = False
        _newsletter.CreatedOn = DateTime.Now
        _newsletter.IsActive = True
        _NewsLetters.UpdateNewsLetter(_newsletter, Id)
    End Sub

    Public Sub SendNewsLetter(Id As Integer) Implements INewsLetter.SendNewsLetter
        Dim _NewsLetter As Model.NewsLetterDetail
        _NewsLetter = _NewsLetters.GetNewsLetterDetail(Id)
        Dim UserList As List(Of Model.NewsLetter)
        Mapper.CreateMap(Of Model.NewsLetter, NewsLetterViewModel)()
        UserList = _NewsLetters.BindNewsLettersUser()
        Dim Arguments As New Args
        Arguments.Id = Id
        Arguments.NewsLetter = _NewsLetter
        Arguments.UserList = UserList

        Dim worker As New BackgroundWorker()
        AddHandler worker.DoWork, AddressOf worker_DoWork
        worker.RunWorker(Arguments)

    End Sub

    Private Sub worker_DoWork(ByRef progress As Integer, ByRef _result As Object, ByVal arguments As Args)
        ' Do the operation for every subscriber wihout the end.
        Dim UserList As List(Of Model.NewsLetter) = arguments.UserList
        Dim _NewsLetter As NewsLetterDetail = arguments.NewsLetter
        Dim Id As Integer = arguments.Id
        Dim round As Integer = UserList.Count
        For i As Integer = 1 To round
            Try
                Dim message As New System.Net.Mail.MailMessage()
                Dim Email As String = "gurpinderpalsingh88@gmail.com"
                message.[To].Add(Email)
                message.Subject = "News Letter From dotnetmailtesting@gmail.com"
                message.From = New System.Net.Mail.MailAddress("dotnetmailtesting@gmail.com")
                message.IsBodyHtml = True
                Dim msg As String = "<table class=table-bordered table-striped table-condensed t-cus><tr><th> " & _NewsLetter.Title & "</th></tr><tr><td>" & _NewsLetter.content & "</td></tr></table>"
                'Dim strBody As String = "<table><tr><td> " & _NewsLetter.Title & "</td></tr><tr><td>" & _NewsLetter.content & "</td></tr>"
                'strBody += "</table>"
                message.Body = msg
                Dim smtp As New System.Net.Mail.SmtpClient("smtp.gmail.com")
                smtp.Credentials = New System.Net.NetworkCredential("dotnetmailtesting@gmail.com", "mss10008")
                smtp.EnableSsl = True
                smtp.Port = 587
                smtp.Send(message)
            Catch ex As ThreadAbortException
                Continue For
            End Try
        Next
        _NewsLetters.UpdateNewsLetterTable(_NewsLetter.Id)
        Thread.CurrentThread.Abort()
        
    End Sub
    
End Class
