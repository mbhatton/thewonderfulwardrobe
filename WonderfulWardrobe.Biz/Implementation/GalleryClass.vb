﻿Imports WonderfulWardrobe.Data
Imports WonderfulWardrobe.Model
Imports AutoMapper

Public Class GalleryClass
    Implements IGalleryClass

    Private _Galeries As IGaleries

    Public Sub New(galeries As IGaleries)
        _Galeries = galeries
    End Sub
    Public Function BindGallery() As List(Of GalleryViewModel) Implements IGalleryClass.BindGallery
        Dim GalleryList As List(Of Gallery)
        Mapper.CreateMap(Of Gallery, GalleryViewModel)()
        GalleryList = _Galeries.BindGallery()
        Dim _gallerylist As List(Of GalleryViewModel) = Mapper.Map(Of List(Of Gallery), List(Of GalleryViewModel))(GalleryList)
        Return _gallerylist
    End Function

    Public Sub UploadImage(_ViewModel As GalleryViewModel) Implements IGalleryClass.UploadImage
        Mapper.CreateMap(Of GalleryViewModel, Gallery)()
        Dim _gallery As Gallery = Mapper.Map(Of GalleryViewModel, Gallery)(_ViewModel)
        _Galeries.UploadImage(_gallery)

    End Sub

    Public Sub DeleteImage(Id As Integer) Implements IGalleryClass.DeleteImage
        _Galeries.DeleteImage(Id)
    End Sub

End Class
