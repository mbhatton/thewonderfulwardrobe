﻿Imports System.Web.Mvc
Imports WonderfulWardrobe.Biz
Imports System.Globalization
Imports System.Net
Imports System.IO

Public Class ClassesController
    Inherits Controller


    Private _Classes As ICustomerClasses
    Public Sub New(classes As ICustomerClasses)
        _Classes = classes
    End Sub
    ' GET: /Classes

    Function Index() As ActionResult
        If Session("TermList") Is Nothing Then
            Dim list As List(Of VenueViewModel)
            Dim TermList As New List(Of TermsViewModel)()
            list = _Classes.BindVenues()
            Dim viewModel As CustomerClassViewModel = New CustomerClassViewModel
            viewModel.VenuesList = list
            viewModel.TermsList = TermList
            Return View(viewModel)
        Else
            Dim viewModel As CustomerClassViewModel = New CustomerClassViewModel
            viewModel = _Classes.BindTermWiseClasses()
            If viewModel.TermsList.Count = 0 Then
                ViewBag.Message = "No Classes Found."
            End If
            Return View(viewModel)
        End If
    End Function


    Public Function Book(id As Integer) As ActionResult
        Dim term As List(Of TermsViewModel)
        term = _Classes.GetTermByVenueId(id)
        Dim viewModel As CustomerClassViewModel = New CustomerClassViewModel
        viewModel.TermsList = term
        Session.Add("TermList", term)
        Session.Add("Id", id)
        Return RedirectToAction("Index", "Classes")
    End Function

    <Authorize>
    Public Function BookClass(id As Integer, type As String, VenueId As Integer) As ActionResult
        If type = "class" Then
            TempData("ClassId") = id
            TempData("TermId") = Nothing
        Else
            TempData("TermId") = id
            TempData("ClassId") = Nothing
            TempData("VenueId") = VenueId
        End If
        Return RedirectToAction("Booking", "Classes")
    End Function

    <Authorize>
    Function Booking() As ActionResult
        If TempData("TermId") = Nothing Then
            Dim _class1 As New ClassBookingViewModel
            _class1 = TempData("ClassData")
            Dim classId = TempData("ClassId")
            Dim _class As ClassBookingViewModel
            If classId Is Nothing Then
            Else
                _class = _Classes.GetClassByClassId(classId)
            End If
            If Session("ClassDetails") Is Nothing Then
                Dim classdetails As New ClassBookingViewModel
                Dim bookedclasses As New List(Of ClassBookingViewModel)
                Dim family As New List(Of FamilyViewModel)
                If _class Is Nothing Then
                Else
                    _class.BookingType = "class"
                    bookedclasses.Add(_class)
                    family = _class.family
                End If
                classdetails.BookedClassList = bookedclasses
                classdetails.family = family
                Session.Add("ClassDetails", bookedclasses)
                Session("Family") = Nothing
                classdetails.Logo = "0"

                Return View(classdetails)
            Else
                Dim viewModel As ClassBookingViewModel = New ClassBookingViewModel
                Dim _ViewModelData As ClassBookingViewModel = New ClassBookingViewModel
                viewModel.BookedClassList = DirectCast(Session("ClassDetails"), List(Of ClassBookingViewModel))
                Dim _family As List(Of FamilyViewModel)
                _family = _Classes.GetFamily()
                viewModel.family = _family
                _ViewModelData.BookedClassList = viewModel.BookedClassList
                _ViewModelData.family = viewModel.family
                If _class Is Nothing Then
                Else
                    For Each items As ClassBookingViewModel In viewModel.BookedClassList.ToList()
                        If items.VenueId = _class.VenueId Then
                            Dim classResult = items.CLassList.Find(Function(item) item.ClassId = _class.CLassList(0).ClassId)
                            If classResult Is Nothing Then
                                items.CLassList.Add(_class.CLassList(0))
                            End If
                        End If

                    Next
                    Dim Result = viewModel.BookedClassList.Find(Function(item) item.VenueId = _class.VenueId)
                    If Result Is Nothing Then
                        viewModel.BookedClassList.Add(_class)
                    End If
                End If

                Session("ClassDetails") = Nothing
                Session.Add("ClassDetails", _ViewModelData.BookedClassList)
                If _class1 Is Nothing Then
                Else
                    _ViewModelData.TermList = _class1.TermList
                End If
                Dim logo As String = Session("Children")
                If logo Is Nothing Then
                    _ViewModelData.Logo = "0"
                Else
                    _ViewModelData.Logo = Session("Children")
                End If

                Return View(_ViewModelData)
            End If
        Else
            Dim termId = TempData("TermId")
            Dim VenueId = TempData("venueId")
            Dim classdata As New List(Of ClassViewModel)
            classdata = _Classes.BindClassByTermVenue(termId, VenueId)
            Dim termdata As New TermsViewModel
            termdata = _Classes.BindTermbyId(termId)
            Dim venuedata As New VenueViewModel
            venuedata = _Classes.BindVenuebyId(VenueId)

            If Session("ClassDetails") Is Nothing Then
                Dim classdetails As New ClassBookingViewModel
                Dim Classes As New ClassBookingViewModel
                Dim bookedclasses As New List(Of ClassBookingViewModel)
                bookedclasses.Add(Classes)
                Dim classList As New List(Of ClassViewModel)
                bookedclasses(0).CLassList = classList
                bookedclasses(0).VenueName = venuedata.Name
                bookedclasses(0).Address1 = venuedata.Address1
                bookedclasses(0).Address2 = venuedata.Address2
                bookedclasses(0).Address3 = venuedata.Address3
                bookedclasses(0).City = venuedata.City
                bookedclasses(0).Day = venuedata.Day
                bookedclasses(0).VenueId = VenueId
                bookedclasses(0).TotalTermAmount = termdata.TotalAmount
                bookedclasses(0).BookingType = "term"
                classdata = _Classes.BindClassByTermVenue(termId, VenueId)
                For Each item As ClassViewModel In classdata
                    bookedclasses(0).CLassList.Add(item)
                Next
                classdetails.BookedClassList = bookedclasses
                Dim _family As List(Of FamilyViewModel)
                _family = _Classes.GetFamily()
                classdetails.family = _family
                Session("Id") = Nothing
                Session.Add("Id", VenueId)
                Session.Add("ClassDetails", bookedclasses)
                Session("Family") = Nothing
                classdetails.Logo = "0"
                Return View(classdetails)

            Else
                'Dim classdetails As New ClassBookingViewModel
                Dim viewModel As ClassBookingViewModel = New ClassBookingViewModel
                viewModel.BookedClassList = DirectCast(Session("ClassDetails"), List(Of ClassBookingViewModel))
                Dim classList As New List(Of ClassViewModel)
                Dim Classes As New ClassBookingViewModel
                Dim count = viewModel.BookedClassList.Count
                Dim VenueDetails As ClassBookingViewModel = viewModel.BookedClassList.Find(Function(item) item.VenueId = venuedata.VenueId)
                If VenueDetails Is Nothing Then

                Else
                    viewModel.BookedClassList.Remove(VenueDetails)
                End If

                Classes.CLassList = classList
                Classes.VenueName = venuedata.Name
                Classes.Address1 = venuedata.Address1
                Classes.Address2 = venuedata.Address2
                Classes.Address3 = venuedata.Address3
                Classes.City = venuedata.City
                Classes.Day = venuedata.Day
                Classes.VenueId = VenueId
                Classes.TotalTermAmount = termdata.TotalAmount
                Classes.BookingType = "term"
                classdata = _Classes.BindClassByTermVenue(termId, VenueId)
                For Each item As ClassViewModel In classdata
                    Classes.CLassList.Add(item)
                Next
                viewModel.BookedClassList.Add(Classes)
                Dim _family As List(Of FamilyViewModel)
                _family = _Classes.GetFamily()
                viewModel.family = _family
                Session("Id") = Nothing
                Session.Add("Id", VenueId)
                Session.Add("ClassDetails", viewModel.BookedClassList)
                Session("Family") = Nothing
                viewModel.Logo = "0"
                Return View(viewModel)
            End If
        End If
    End Function

    <Authorize>
    Public Function AddAnother(VenueId As Integer) As ActionResult
        Dim _class As List(Of TermsViewModel)
        _class = _Classes.GetTermByVenueId(VenueId)
        Dim _ViewModelData As ClassBookingViewModel = New ClassBookingViewModel
        Dim classdetails As New ClassBookingViewModel
        Dim bookedclasses As New List(Of ClassBookingViewModel)
        classdetails.TermList = _class
        TempData("ClassData") = classdetails
        Return RedirectToAction("Booking", "Classes")

    End Function

    <Authorize>
    Public Function RemoveClass(id As Integer, VenueId As Integer) As ActionResult
        Dim viewModel As ClassBookingViewModel = New ClassBookingViewModel
        Dim _ViewModelData As ClassBookingViewModel = New ClassBookingViewModel
        viewModel.BookedClassList = Session("ClassDetails")
        Dim _family As List(Of FamilyViewModel)
        _family = _Classes.GetFamily()
        viewModel.family = _family
        _ViewModelData.BookedClassList = viewModel.BookedClassList
        _ViewModelData.family = viewModel.family
        Dim count As Integer = 0
        For Each items As ClassBookingViewModel In viewModel.BookedClassList.ToList()
            If items.VenueId = VenueId Then
                Dim classResult = items.CLassList.Find(Function(item) item.ClassId = id)
                If classResult Is Nothing Then
                Else
                    items.BookingType = "class"
                    items.CLassList.Remove(classResult)
                    If items.CLassList.Count = 0 Then
                        viewModel.BookedClassList.Remove(items)
                    End If
                End If
            End If
        Next
        Session("ClassDetails") = Nothing
        Session.Add("ClassDetails", _ViewModelData.BookedClassList)
        Return RedirectToAction("Booking", "Classes")

    End Function

    <Authorize>
    Public Function AddChild(form As FormCollection, _classBooking As ClassBookingViewModel, Command As String) As ActionResult
        If Command = "Pay Now" Then
            Session("Children") = Nothing
            Dim selectedchild As String = form("Logo")
            Dim paymentmode As String = form("paymentoption")
            If selectedchild Is Nothing Then
                selectedchild = 0
            End If
            Session("PaymentMode") = Nothing
            Session.Add("PaymentMode", paymentmode)
            Session.Add("Children", selectedchild)
            Return RedirectToAction("PayNow", "Classes")
        ElseIf Command = "Add" Then
            Dim family As New FamilyViewModel
            Dim boy As String = form("Boy")
            Dim girl As String = form("Girl")
            Dim day As String = form("day").Replace(",", "")
            Dim month As String = form("month").Replace(",", "")
            Dim year As String = form("year").Replace(",", "")
            Dim dob As String = month + "/" + day + "/" + year
            Dim currentdate As DateTime = DateTime.Today
            family.DOB = DateTime.Parse(dob, CultureInfo.InvariantCulture)
            family.CreatedOn = DateTime.Today
            family.FirstName = form("child_first_name").Replace(",", "")
            family.LastName = form("child_last_name").Replace(",", "")
            family.Gender = form("childgender").Replace(",", "")
            family.IsActive = True
            _Classes.AddChild(family)
            Dim _family As List(Of FamilyViewModel)
            _family = _Classes.GetFamily()
            If _classBooking.Logo Is Nothing Then
                _classBooking.Logo = _family(_family.Count - 1).ToString()
            Else
                _classBooking.Logo += "," + _family(_family.Count - 1).Id.ToString()
            End If
            Session("Children") = Nothing
            Session.Add("Children", _classBooking.Logo)
            Return RedirectToAction("Booking", "Classes")
        Else
            Return RedirectToAction("Booking", "Classes")
        End If
    End Function


    <Authorize>
    Public Function PayNow(form As FormCollection, _classBooking As ClassBookingViewModel) As ActionResult
        Dim viewModel As ClassBookingViewModel = New ClassBookingViewModel
        viewModel.BookedClassList = DirectCast(Session("ClassDetails"), List(Of ClassBookingViewModel))
        viewModel.Logo = Session("Children").ToString().Replace(",0", "").Replace("0,", "")
        viewModel.TotalAmount = Session("TotalAmount")
        viewModel.PaymentMode = Session("PaymentMode")
        If viewModel.PaymentMode = "Paypal" Then
            Session.Add("BookingType", "Class")
            Return RedirectToAction("CreatePayment", "Paypal")

        Else

            Dim Status As String = _Classes.SaveBookedClass(viewModel)
            Session("Children") = Nothing
            Session("ClassDetails") = Nothing
            Session("TotalAmount") = Nothing
            Session("PaymentMode") = Nothing
            Session("BookingType") = Nothing
            Return RedirectToAction("ThankYou", "Purchase")
        End If
    End Function
End Class