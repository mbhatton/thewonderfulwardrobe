﻿Imports System.Web.Mvc
Imports WonderfulWardrobe.Biz
Imports System.Globalization
Imports System.IO
Imports System.Reflection

Public Class AdminClassController
    Inherits Controller

    ' GET: /AdminClass

    Private _AdminClass As IAdminClass

    Public Sub New(adminclass As IAdminClass)
        _AdminClass = adminclass
    End Sub


    <Authorize(Roles:="SuperAdmin,Admin")> _
      <HttpPost>
    Public Function CreateTerms(terms As TermsViewModel) As ActionResult
        terms.CreatedOn = DateTime.Today
        If terms.TotalAmount Is Nothing Then
            terms.TotalAmount = "0"
        End If
        _AdminClass.CreateTerms(terms)
        ModelState.Clear()
        ViewBag.Message = "Term Created Successfully."
        Return View()

    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function CreateTerms() As ActionResult
        Return View()
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function CreateVenues() As ActionResult
        Dim list As New List(Of SelectListItem)()
        list.Add(New SelectListItem() With { _
     .Text = "-Please select-", _
     .Value = "0", _
     .Selected = True _
})
        list.Add(New SelectListItem() With { _
    .Text = "Sunday", _
    .Value = "Sunday" _
})
        list.Add(New SelectListItem() With { _
    .Text = "Monday", _
    .Value = "Monday" _
})
        list.Add(New SelectListItem() With { _
    .Text = "Tuesday", _
    .Value = "Tuesday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Wednesday", _
.Value = "Wednesday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Thursday", _
.Value = "Thursday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Friday", _
.Value = "Friday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Saturday", _
.Value = "Saturday" _
})

        ViewBag.listitem = list
        Return View()
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
      <HttpPost>
    Public Function CreateVenues(venues As VenueViewModel, file As HttpPostedFileBase) As ActionResult

        If file IsNot Nothing AndAlso file.ContentLength > 0 Then
            ' extract only the fielname
            Dim fileName = Path.GetFileName(file.FileName)
            ' store the file inside ~/App_Data/uploads folder
            Dim filepath = Path.Combine(Server.MapPath("~/Content/images"), fileName)
            file.SaveAs(filepath)
            venues.Logo = "~/Content/images/" + fileName
        End If

        venues.CreatedOn = DateTime.Today
        _AdminClass.CreateVenues(venues)
        ModelState.Clear()
        ViewBag.Message = "Venue Created Successfully."
        Dim list As New List(Of SelectListItem)()
        list.Add(New SelectListItem() With { _
     .Text = "-Please select-", _
     .Value = "0", _
     .Selected = True _
})
        list.Add(New SelectListItem() With { _
    .Text = "Sunday", _
    .Value = "Sunday" _
})
        list.Add(New SelectListItem() With { _
    .Text = "Monday", _
    .Value = "Monday" _
})
        list.Add(New SelectListItem() With { _
    .Text = "Tuesday", _
    .Value = "Tuesday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Wednesday", _
.Value = "Wednesday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Thursday", _
.Value = "Thursday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Friday", _
.Value = "Friday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Saturday", _
.Value = "Saturday" _
})

        ViewBag.listitem = list
        Return View()
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function CreateClass() As ActionResult
        Dim TermsList As List(Of SelectListItem)
        TermsList = _AdminClass.GetTermsList()
        ViewBag.TermList = TermsList
        Dim VenueList As List(Of SelectListItem)
        VenueList = _AdminClass.GetVenuesList()
        ViewBag.VenueList = VenueList

        Return View()
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
      <HttpPost>
    Public Function CreateClass(classes As ClassViewModel) As ActionResult
        classes.CreatedOn = DateTime.Today
        _AdminClass.CreateClasses(classes)
        ModelState.Clear()
        ViewBag.Message = "Class Created Successfully."
        Dim TermsList As List(Of SelectListItem)
        TermsList = _AdminClass.GetTermsList()
        ViewBag.TermList = TermsList
        Dim VenueList As List(Of SelectListItem)
        VenueList = _AdminClass.GetVenuesList()
        ViewBag.VenueList = VenueList
        Return View()
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function TermsList() As ActionResult
        Dim list As List(Of TermsViewModel)
        list = _AdminClass.BindTerms()
        Dim viewModel As TermsViewModel = New TermsViewModel
        viewModel.TermsList = list
        ViewBag.Message = TempData("Message")
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function EditTerm(id As Integer) As ActionResult
        Dim term As TermsViewModel
        term = _AdminClass.GetTermByTermId(id)
        Return View(term)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function DeleteTermConfirmed(id As Integer) As ActionResult
        Dim _class As List(Of ClassViewModel)
        _class = _AdminClass.GetClassByTermId(id)
        If _class.Count = 0 Then
            _AdminClass.DeleteTerm(id)
            TempData("Message") = ""
            Return RedirectToAction("TermsList", "AdminClass")
        Else
            TempData("Message") = "You can't Delete because it is associated with another class."
            Return RedirectToAction("TermsList", "AdminClass")
        End If

    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function DeleteTerm(id As Integer) As ActionResult
        Dim term As TermsViewModel
        term = _AdminClass.GetTermByTermId(id)
        Return View(term)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function UpdateTerm(_term As TermsViewModel) As ActionResult
        If _term.TotalAmount Is Nothing Then
            _term.TotalAmount = "0"
        End If
        _AdminClass.UpdateTerm(_term)
        Return RedirectToAction("TermsList", "AdminClass")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function DetailTerm(id As Integer) As ActionResult
        Dim term As TermsViewModel
        term = _AdminClass.GetTermByTermId(id)
        Return View(term)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function VenuesList() As ActionResult
        Dim list As List(Of VenueViewModel)
        ViewBag.Message = TempData("Message")
        list = _AdminClass.BindVenues()
        Dim viewModel As VenueViewModel = New VenueViewModel
        viewModel.VenuesList = list
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function EditVenue(id As Integer) As ActionResult
        Dim list As New List(Of SelectListItem)()
        list.Add(New SelectListItem() With { _
     .Text = "-Please select-", _
     .Value = "0", _
     .Selected = True _
})
        list.Add(New SelectListItem() With { _
    .Text = "Sunday", _
    .Value = "Sunday" _
})
        list.Add(New SelectListItem() With { _
    .Text = "Monday", _
    .Value = "Monday" _
})
        list.Add(New SelectListItem() With { _
    .Text = "Tuesday", _
    .Value = "Tuesday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Wednesday", _
.Value = "Wednesday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Thursday", _
.Value = "Thursday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Friday", _
.Value = "Friday" _
})
        list.Add(New SelectListItem() With { _
.Text = "Saturday", _
.Value = "Saturday" _
})

        ViewBag.listitem = list
        Dim venue As VenueViewModel
        venue = _AdminClass.GetVenueByVenueId(id)
        Dim image As String = venue.Logo
        Dim ImageUrl As String = ConfigurationManager.AppSettings("ImageUrl")
        'Dim url As String = Request.Url + "/Content/images/" + image
        Dim url As String() = image.Split("/")
        Dim path As String = ImageUrl + "Content/images/" + url(url.Length - 1)
        venue.Logo = path
        Return View(venue)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function DeleteVenueConfirmed(id As Integer) As ActionResult
        Dim _class As List(Of ClassViewModel)
        _class = _AdminClass.GetClassByVenueId(id)
        If _class.Count = 0 Then
            _AdminClass.DeleteVenue(id)
            TempData("Message") = ""
            Return RedirectToAction("VenuesList", "AdminClass")
        Else
            TempData("Message") = "You can't Delete because it is associated with another class."
            Return RedirectToAction("VenuesList", "AdminClass")
        End If

    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function DeleteVenue(id As Integer) As ActionResult
        Dim venue As VenueViewModel
        venue = _AdminClass.GetVenueByVenueId(id)
        Return View(venue)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function UpdateVenue(_venue As VenueViewModel, file As HttpPostedFileBase) As ActionResult
        If file IsNot Nothing AndAlso file.ContentLength > 0 Then
            ' extract only the fielname
            Dim fileName = Path.GetFileName(file.FileName)
            ' store the file inside ~/App_Data/uploads folder
            Dim filepath = Path.Combine(Server.MapPath("~/Content/images"), fileName)
            file.SaveAs(filepath)
            _venue.Logo = "~/Content/images/" + fileName
        Else
            Dim url As String() = _venue.Logo.Split("/")
            _venue.Logo = "~/Content/images/" + url(url.Length - 1)
            '_venue.Logo = url(url.Length - 1)
        End If
        _AdminClass.UpdateVenue(_venue)
        Return RedirectToAction("VenuesList", "AdminClass")
    End Function

    <Authorize(Roles:="SuperAdmin")> _
    Function DetailVenue(id As Integer) As ActionResult
        Dim venue As VenueViewModel
        venue = _AdminClass.GetVenueByVenueId(id)
        Dim image As String = venue.Logo
        Dim ImageUrl As String = ConfigurationManager.AppSettings("ImageUrl")
        'Dim path As String = ImageUrl + "Content/images/" + image
        Dim url As String() = image.Split("/")
        Dim path As String = ImageUrl + "Content/images/" + url(url.Length - 1)
        venue.Logo = path
        Return View(venue)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function ClassesList() As ActionResult
        Dim list As List(Of ClassViewModel)
        list = _AdminClass.BindClasses()
        Dim viewModel As ClassViewModel = New ClassViewModel
        viewModel.ClassesList = list
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function EditClass(id As Integer) As ActionResult
        'Get Class Details by Class ID
        Dim cls As ClassViewModel
        cls = _AdminClass.GetClassByClassId(id)
        'Get Term's List
        Dim TermsList As List(Of SelectListItem)
        TermsList = _AdminClass.GetTermsListByTermId(cls.TermId)
        ViewBag.TermList = TermsList

        'Get Venue's List
        Dim VenueList As List(Of SelectListItem)
        VenueList = _AdminClass.GetVenuesListByVenueId(cls.VenueId)
        ViewBag.VenueList = VenueList

        Return View(cls)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function DeleteClassConfirmed(id As Integer) As ActionResult
        _AdminClass.DeleteClass(id)
        Return RedirectToAction("ClassesList", "AdminClass")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function DeleteClass(id As Integer) As ActionResult
        Dim cls As ClassViewModel
        cls = _AdminClass.GetClassByClassId(id)
        Return View(cls)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function UpdateClass(_class As ClassViewModel, form As FormCollection) As ActionResult
        _class.StartDateTime = form("datetime")
        _AdminClass.UpdateClass(_class)
        Return RedirectToAction("ClassesList", "AdminClass")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function DetailClass(id As Integer) As ActionResult
        Dim cls As ClassViewModel
        cls = _AdminClass.GetClassByClassId(id)
        Return View(cls)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function Index() As ActionResult
        Return View()
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function BookedOrders() As ActionResult
        Dim list As List(Of CreatedOrdersViewModel)
        list = _AdminClass.BindBookedOrders()
        Dim viewModel As CreatedOrdersViewModel = New CreatedOrdersViewModel
        viewModel.OrderList = list
        ViewBag.Message = TempData("Message")
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function OrderDetails(OrderId As Integer) As ActionResult
        Dim list As List(Of OrderDetailsViewModel)
        list = _AdminClass.BindBookedClasses(OrderId)
        Dim viewModel As OrderDetailsViewModel = New OrderDetailsViewModel
        viewModel.OrderDetailList = list
        ViewBag.Message = TempData("Message")
        Return View(viewModel)
    End Function
End Class