﻿Imports System.Web.Mvc
Imports WonderfulWardrobe.Biz

Public Class NewsLetterController
    Inherits Controller

    Private _NewsLetter As INewsLetter

    Public Sub New(_News As INewsLetter)
        _NewsLetter = _News
    End Sub
    ' GET: /NewsLetter

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function Index() As ActionResult
        Dim list As List(Of NewsLetterViewModel)
        list = _NewsLetter.BindNewsLettersUser()
        Dim viewModel As NewsLetterViewModel = New NewsLetterViewModel
        viewModel.NewsLetterList = list
        Return View(viewModel)
    End Function

    <AllowAnonymous>
    Public Function NewsLetter(form As FormCollection) As ActionResult
        Dim ViewModel As New NewsLetterViewModel
        ViewModel.CreatedOn = DateTime.Now
        ViewModel.EmailId = form("email")
        ViewModel.IsActive = True
        ViewModel.Name = form("name")
        _NewsLetter.RegisterNewsLetter(ViewModel)
        Return RedirectToAction("Index", "Group")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Public Function Delete(id As Integer) As ActionResult
        _NewsLetter.DeleteUser(id)
        Return RedirectToAction("Index", "NewsLetter")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Public Function CreateNewsLetter() As ActionResult
        ViewBag.Message = TempData("Message")
        Return View()
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    <ValidateInput(False)>
    Public Function SaveNewsLetter(_ViewModel As NewsLetterViewModel) As ActionResult
        _NewsLetter.SaveNewsLetter(_ViewModel)
        TempData("Message") = "News Letter Saved Successfully."
        Return RedirectToAction("CreateNewsLetter")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Public Function NewsLettersList() As ActionResult
        Dim list As List(Of NewsLetterViewModel)
        list = _NewsLetter.BindNewsLettersList()
        Dim viewModel As NewsLetterViewModel = New NewsLetterViewModel
        viewModel.NewsLetterList = list
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Public Function DeleteNewsLetter(id As Integer) As ActionResult
        _NewsLetter.DeleteNewsLetter(id)
        Return RedirectToAction("NewsLettersList", "NewsLetter")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    <ValidateInput(False)>
    Public Function EditNewsLetter(id As Integer) As ActionResult
        Dim ViewModel As New NewsLetterViewModel
        ViewModel = _NewsLetter.GetNewsLetterDetail(id)
        Return View(ViewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
   <ValidateInput(False)>
    Public Function UpdateNewsLetter(_ViewModel As NewsLetterViewModel) As ActionResult
        Dim Id As Integer = _ViewModel.Id
        _NewsLetter.UpdateNewsLetter(_ViewModel, Id)
        Return RedirectToAction("NewsLettersList")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
   <ValidateInput(False)>
    Public Function ViewNewsLetter(id As Integer) As ActionResult
        Dim ViewModel As New NewsLetterViewModel
        ViewModel = _NewsLetter.GetNewsLetterDetail(id)
        Return View(ViewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
   <ValidateInput(False)>
    Public Function SendNewsLetter(id As Integer) As ActionResult
        _NewsLetter.SendNewsLetter(id)
        Return RedirectToAction("NewsLettersList")
    End Function
End Class