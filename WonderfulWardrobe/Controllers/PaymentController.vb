﻿Imports System.Web.Mvc
Imports WonderfulWardrobe.Biz

Public Class PaymentController
    Inherits Controller

    ' GET: /Payment

    Private _Payment As IAdminPayment

    Public Sub New(payment As IAdminPayment)
        _Payment = payment
    End Sub

    '<Authorize(Roles:="Admin")> _

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function Index() As ActionResult
        Dim list As List(Of PaymentViewModel)
        list = _Payment.BindPayments()
        Dim viewModel As PaymentViewModel = New PaymentViewModel
        viewModel.PaymentList = list
        Return View(viewModel)
    End Function

End Class