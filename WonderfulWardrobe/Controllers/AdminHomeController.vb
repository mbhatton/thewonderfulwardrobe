﻿Imports System.Web.Mvc

Public Class AdminHomeController
    Inherits Controller

    ' GET: /AdminHome

    <Authorize>
    Function Index() As ActionResult

        Return View()
    End Function
End Class