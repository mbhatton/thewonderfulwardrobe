﻿Imports System.Security.Claims
Imports System.Threading.Tasks
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin.Security
Imports WonderfulWardrobe.Biz
Imports System.Globalization
Imports System.Net
Imports Newtonsoft.Json
Imports System.IO
Imports System.Security.Cryptography

<Authorize>
Public Class AccountController
    Inherits Controller

    Private _Account As IAccount
    Public Sub New(account As IAccount)
        _Account = account
    End Sub

    <AllowAnonymous>
    <HttpPost>
    Public Function Login(account As AccountViewModel) As ActionResult
        Dim Role As String = _Account.Login(account)
        If Role = "User" Then
            Return RedirectToAction("Index", "Group")
        ElseIf Role = "Admin" Then
            Return RedirectToAction("Index", "AdminHome")
        ElseIf Role = "SuperAdmin" Then
            Return RedirectToAction("Index", "AdminHome")
        Else
            ViewBag.Message = "You have Enterd wrong username or password"
        End If


        Return View()
    End Function
    <AllowAnonymous>
    Public Function Login(returnUrl As String) As ActionResult
        ViewBag.ReturnUrl = returnUrl
        Return View()
    End Function
    


    <AllowAnonymous>
    Public Function Logout() As ActionResult
        Session("Children") = Nothing
        Session("ClassDetails") = Nothing
        Session("TotalAmount") = Nothing
        Session("PartyDetails") = Nothing
        Session("PaymentMode") = Nothing
        Session("BookingType") = Nothing
        FormsAuthentication.SignOut()
        Return RedirectToAction("Login", "Account")
    End Function

    <AllowAnonymous>
    <HttpPost>
    Public Function Register(form As FormCollection, account As AccountViewModel) As ActionResult
        Dim familyList As New List(Of FamilyViewModel)
        Dim childCount As String = form("count")

        Dim childRemoveCount As String = form("removecount")
        Dim removeChildArray As String()
        If Not childRemoveCount Is Nothing Then
            removeChildArray = childRemoveCount.Split(",")
        Else
            removeChildArray = New String(-1) {}
        End If

        If Not childCount Is Nothing Then
            Dim childArray As String() = childCount.Split(",")
            Dim counter As Integer = Convert.ToInt32(childArray.Last())
            For i As Integer = 2 To counter
                Dim pos As Integer = removeChildArray.Contains(i)
                If pos = -1 Then

                Else
                    Dim family As New FamilyViewModel
                    Dim dateofbirth As String() = form("DOB" + i.ToString()).Split("/")
                    Dim day As String = dateofbirth(0)
                    Dim month As String = dateofbirth(1)
                    Dim year As String = dateofbirth(2)
                    Dim dob As String = month + "/" + day + "/" + year
                    Dim currentdate As DateTime = DateTime.Today
                    family.DOB = DateTime.Parse(dob, CultureInfo.InvariantCulture)
                    family.CreatedOn = DateTime.Today
                    Dim childname As String() = form("childname" + i.ToString()).Split(" ")
                    family.FirstName = childname(0)
                    Dim lastname = ""
                    For value As Integer = 1 To childname.Length - 1
                        lastname += childname(value) + " "
                    Next
                    family.LastName = lastname
                    family.Gender = form("gender" + i.ToString())
                    family.IsActive = True
                    familyList.Add(family)
                End If

            Next
        End If
        account.family = familyList
        account.CreatedOn = DateTime.Today
        account.IsActive = True
        Dim status As String = _Account.RegisterFamily(account)
        If status = "Success" Then

            ViewBag.Message = "You have Successfully registered"
            Return RedirectToAction("Index", "Group")
        Else
            ViewBag.Message = "You have Enterd wrong"
        End If


        Return View()
    End Function

    <AllowAnonymous>
    Public Function Register() As ActionResult
        Return View()
    End Function
    Public Function Index() As ActionResult
        Return View()
    End Function
End Class

