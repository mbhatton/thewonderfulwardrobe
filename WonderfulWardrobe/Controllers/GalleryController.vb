﻿Imports System.Web.Mvc
Imports System.IO
Imports WonderfulWardrobe.Biz

Public Class GalleryController
    Inherits Controller

    ' GET: /Gallery

    Private _GalleryClass As IGalleryClass

    Public Sub New(galleryclass As IGalleryClass)
        _GalleryClass = galleryclass
    End Sub

    Function Index() As ActionResult
        Dim list As List(Of GalleryViewModel)
        list = _GalleryClass.BindGallery()
        Dim viewModel As GalleryViewModel = New GalleryViewModel
        viewModel.GalleryList = list
        Return View(viewModel)
    End Function


    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function Gallery() As ActionResult
        Return View()
    End Function


    <Authorize(Roles:="SuperAdmin,Admin")> _
      <HttpPost>
    Function Gallery(file As HttpPostedFileBase) As ActionResult
        If file IsNot Nothing AndAlso file.ContentLength > 0 Then
            ' extract only the fielname
            Dim fileName = Path.GetFileName(file.FileName)
            ' store the file inside ~/App_Data/uploads folder
            Dim filepath = Path.Combine(Server.MapPath("~/Content/gallery"), fileName)
            file.SaveAs(filepath)
            Dim ViewModel As New GalleryViewModel
            ViewModel.CreatedOn = DateTime.Now
            ViewModel.ImageName = fileName
            ViewModel.IsActive = True
            ViewModel.Location = "~/Content/gallery/" + fileName
            _GalleryClass.UploadImage(ViewModel)
            ViewBag.Message = "Uploaded Successfully"
        End If
        Return View()
    End Function


    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function GalleryList() As ActionResult
        Dim list As List(Of GalleryViewModel)
        ViewBag.Message = TempData("Message")
        list = _GalleryClass.BindGallery()
        Dim viewModel As GalleryViewModel = New GalleryViewModel
        viewModel.GalleryList = list
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function Delete(id As Integer) As ActionResult
        _GalleryClass.DeleteImage(id)
        Return RedirectToAction("GalleryList", "Gallery")
    End Function

End Class