﻿Imports System.Web.Mvc
Imports WonderfulWardrobe.Biz
Imports PayPal
Imports PayPal.Api.Payments
Imports System.Net
Imports System.IO
Imports Newtonsoft.Json.Linq
Imports System.Xml
Imports PayPal.Manager
Imports PayPal.Exception

Public Class PayPalController
    Inherits Controller

    Private _Classes As ICustomerClasses
    Private _Parties As IParties
    Public Sub New(classes As ICustomerClasses, parties As IParties)
        _Classes = classes
        _Parties = parties
    End Sub
    ' GET: /PayPal
    <Authorize>
    Public Function CreatePayment(description As String, Optional tax As Decimal = 0, Optional shipping As Decimal = 0) As ActionResult
        Dim viewData = New PayPalViewData()
        Dim guid__1 = Guid.NewGuid().ToString()

        Dim paymentInit = New Payment() With { _
             .intent = "authorize", _
             .payer = New Payer() With { _
                 .payment_method = "paypal" _
            }, _
             .transactions = New List(Of Transaction)() From { _
                New Transaction() With { _
                     .amount = New Amount() With { _
                         .currency = "GBP", _
                         .total = (Convert.ToDecimal(Session("TotalAmount")) + tax + shipping).ToString(), _
                         .details = New Details() With { _
                             .subtotal = Session("TotalAmount").ToString(), _
                             .tax = tax.ToString(), _
                             .shipping = shipping.ToString() _
                        } _
                    }, _
                     .description = "Class" _
                } _
            }, _
             .redirect_urls = New RedirectUrls() With { _
                 .return_url = Utilities.ToAbsoluteUrl(HttpContext, [String].Format("~/paypal/confirmed?id={0}", guid__1)), _
                 .cancel_url = Utilities.ToAbsoluteUrl(HttpContext, [String].Format("~/paypal/canceled?id={0}", guid__1)) _
            } _
        }



        viewData.JsonRequest = JObject.Parse(paymentInit.ConvertToJson()).ToString(Formatting.Indented)

        Try
            Dim clientId As String = ConfigManager.Instance.GetProperties()("ClientID")
            Dim clientSecret As String = ConfigManager.Instance.GetProperties()("ClientSecret")
            Dim accessToken = New OAuthTokenCredential(ConfigManager.Instance.GetProperties()("ClientID"), ConfigManager.Instance.GetProperties()("ClientSecret")).GetAccessToken()
            Dim apiContext = New APIContext(accessToken)
            Dim createdPayment = paymentInit.Create(apiContext)

            Dim approvalUrl = createdPayment.links.ToArray().FirstOrDefault(Function(f) f.rel.Contains("approval_url"))

            If approvalUrl IsNot Nothing Then
                Session.Add(guid__1, createdPayment.id)

                Return Redirect(approvalUrl.href)
            End If

            viewData.JsonResponse = JObject.Parse(createdPayment.ConvertToJson()).ToString(Formatting.Indented)

            Return View("ErrorMessage", viewData)
        Catch ex As PayPalException
            viewData.ErrorMessage = ex.Message

            Return View("ErrorMessage", viewData)
        End Try
    End Function

    <Authorize>
    Public Function Confirmed(id As Guid, token As String, payerId As String) As ActionResult
        Dim viewData = New ConfirmedViewData() With { _
             .Id = id, _
             .Token = token, _
             .PayerId = payerId _
        }

        Dim accessToken = New OAuthTokenCredential(ConfigManager.Instance.GetProperties()("ClientID"), ConfigManager.Instance.GetProperties()("ClientSecret")).GetAccessToken()
        Dim apiContext = New APIContext(accessToken)
        Dim payment = New Payment() With { _
             .id = DirectCast(Session(id.ToString()), String) _
        }

        Dim executedPayment = payment.Execute(apiContext, New PaymentExecution() With { _
             .payer_id = payerId _
        })

        viewData.AuthorizationId = executedPayment.transactions(0).related_resources(0).authorization.id
        viewData.JsonRequest = JObject.Parse(payment.ConvertToJson()).ToString(Formatting.Indented)
        viewData.JsonResponse = JObject.Parse(executedPayment.ConvertToJson()).ToString(Formatting.Indented)




        Dim viewData1 = New PayPalViewData()
        Try
            Dim accessToken1 = New OAuthTokenCredential(ConfigManager.Instance.GetProperties()("ClientID"), ConfigManager.Instance.GetProperties()("ClientSecret")).GetAccessToken()
            Dim apiContext1 = New APIContext(accessToken1)
            Dim authorization__1 = PayPal.Api.Payments.Authorization.[Get](apiContext1, viewData.AuthorizationId)

            If authorization__1 IsNot Nothing Then
                Dim total = Convert.ToDecimal(authorization__1.amount.total)

                Dim capture__2 = authorization__1.Capture(apiContext, New Capture() With { _
                     .is_final_capture = True, _
                     .amount = New Amount() With { _
                         .currency = "GBP", _
                         .total = total.ToString("f2") _
                    } _
                })
                viewData1.JsonResponse = JObject.Parse(capture__2.ConvertToJson()).ToString(Formatting.Indented)
                Dim result = Newtonsoft.Json.JsonConvert.DeserializeObject(Of TransectionDetails)(viewData1.JsonResponse)


                Dim bokingtype = Session("BookingType").ToString()
                If (bokingtype = "Class") Then
                    Dim viewModel As ClassBookingViewModel = New ClassBookingViewModel
                    viewModel.BookedClassList = DirectCast(Session("ClassDetails"), List(Of ClassBookingViewModel))
                    viewModel.Logo = Session("Children").ToString().Replace(",0", "").Replace("0,", "")
                    viewModel.TotalAmount = Session("TotalAmount")
                    viewModel.PaymentMode = Session("PaymentMode")
                    viewModel.TransactionId = result.TransactionId
                    viewModel.PaidAmount = result.PaidAmount.Total
                    Dim Status As String = _Classes.SaveBookedClass(viewModel)
                    Session("BookingType") = Nothing
                    Session("Children") = Nothing
                    Session("ClassDetails") = Nothing
                    Session("TotalAmount") = Nothing
                    Session("PaymentMode") = Nothing
                    Return RedirectToAction("ThankYou", "Purchase")
                Else
                    Dim viewModel As New PartyBookingViewModel
                    viewModel = DirectCast(Session("PartyDetails"), PartyBookingViewModel)
                    viewModel.TransactionId = result.TransactionId
                    viewModel.PaidAmount = result.PaidAmount.Total
                    _Parties.SaveParty(viewModel)
                    Session("BookingType") = Nothing
                    Session("PartyDetails") = Nothing
                    Session("TotalAmount") = Nothing
                    Session("PaymentMode") = Nothing
                    Return RedirectToAction("ThankYou", "Purchase")
                End If
               
            End If

            viewData1.ErrorMessage = "Could not find previous authorization."

            Return View("ErrorMessage", viewData1)
        Catch ex As PayPalException
            viewData1.ErrorMessage = ex.Message

            Return View("ErrorMessage", viewData1)
        End Try
    End Function

   
  
    'Function Index() As ActionResult
    '    Return View()
    'End Function


    <Authorize>
    Public Function CreatePartyPayment(description As String, Optional tax As Decimal = 0, Optional shipping As Decimal = 0) As ActionResult
        Dim viewData = New PayPalViewData()
        Dim guid__1 = Guid.NewGuid().ToString()
        Dim totalamount As Decimal = Convert.ToDecimal(Session("RequiredAmount"))
        Dim paymentInit = New Payment() With { _
             .intent = "authorize", _
             .payer = New Payer() With { _
                 .payment_method = "paypal" _
            }, _
             .transactions = New List(Of Transaction)() From { _
                New Transaction() With { _
                     .amount = New Amount() With { _
                         .currency = "GBP", _
                         .total = (Convert.ToDecimal(totalamount) + tax + shipping).ToString(), _
                         .details = New Details() With { _
                             .subtotal = totalamount.ToString(), _
                             .tax = tax.ToString(), _
                             .shipping = shipping.ToString() _
                        } _
                    }, _
                     .description = "Party" _
                } _
            }, _
             .redirect_urls = New RedirectUrls() With { _
                 .return_url = Utilities.ToAbsoluteUrl(HttpContext, [String].Format("~/paypal/confirmed?id={0}", guid__1)), _
                 .cancel_url = Utilities.ToAbsoluteUrl(HttpContext, [String].Format("~/paypal/canceled?id={0}", guid__1)) _
            } _
        }



        viewData.JsonRequest = JObject.Parse(paymentInit.ConvertToJson()).ToString(Formatting.Indented)

        Try
            Dim clientId As String = ConfigManager.Instance.GetProperties()("ClientID")
            Dim clientSecret As String = ConfigManager.Instance.GetProperties()("ClientSecret")
            Dim accessToken = New OAuthTokenCredential(ConfigManager.Instance.GetProperties()("ClientID"), ConfigManager.Instance.GetProperties()("ClientSecret")).GetAccessToken()
            Dim apiContext = New APIContext(accessToken)
            Dim createdPayment = paymentInit.Create(apiContext)

            Dim approvalUrl = createdPayment.links.ToArray().FirstOrDefault(Function(f) f.rel.Contains("approval_url"))

            If approvalUrl IsNot Nothing Then
                Session.Add(guid__1, createdPayment.id)

                Return Redirect(approvalUrl.href)
            End If

            viewData.JsonResponse = JObject.Parse(createdPayment.ConvertToJson()).ToString(Formatting.Indented)

            Return View("ErrorMessage", viewData)
        Catch ex As PayPalException
            viewData.ErrorMessage = ex.Message

            Return View("ErrorMessage", viewData)
        End Try
    End Function
End Class



Public NotInheritable Class Utilities
    Private Sub New()
    End Sub
    Public Shared Function ToAbsoluteUrl(httpContext As HttpContextBase, relativeUrl As String) As String
        If String.IsNullOrEmpty(relativeUrl) Then
            Return relativeUrl
        End If

        If httpContext Is Nothing Then
            Return relativeUrl
        End If

        If relativeUrl.StartsWith("/") Then
            relativeUrl = relativeUrl.Insert(0, "~")
        End If
        If Not relativeUrl.StartsWith("~/") Then
            relativeUrl = relativeUrl.Insert(0, "~/")
        End If

        Dim url = httpContext.Request.Url
        Dim port = If(url.Port <> 80, (":" & Convert.ToString(url.Port)), [String].Empty)

        Return [String].Format("{0}://{1}{2}{3}", url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl))
    End Function
End Class



