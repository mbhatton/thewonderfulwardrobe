﻿Imports System.Web.Mvc
Imports WonderfulWardrobe.Biz

Public Class SuperAdminController
    Inherits Controller

    Private _Account As IAccount
    Public Sub New(account As IAccount)
        _Account = account
    End Sub
    ' GET: /SuperAdmin


    <Authorize(Roles:="SuperAdmin")> _
    Function Index() As ActionResult
        Return View()
    End Function


    <Authorize(Roles:="SuperAdmin")> _
   <HttpPost>
    Function Index(account As AccountViewModel) As ActionResult
        account.CreatedOn = DateTime.Today
        account.IsActive = True
        Dim status As String = _Account.RegisterFamily(account)
        If status = "Success" Then
            ModelState.Clear()
            ViewBag.Message = "You have Successfully registered"

            Return View()
        Else

            ViewBag.Message = "You have Enterd wrong"
        End If
        Return View()
    End Function

    <Authorize(Roles:="SuperAdmin")> _
    Function AdminList() As ActionResult
        Dim list As List(Of AccountViewModel)
        list = _Account.BindAdminList()
        Dim viewModel As AccountViewModel = New AccountViewModel
        viewModel.AdminList = list
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin")> _
    Function DeleteAdmin(UserName As String) As ActionResult
        Membership.DeleteUser(UserName)
        Return RedirectToAction("AdminList", "SuperAdmin")
    End Function
End Class