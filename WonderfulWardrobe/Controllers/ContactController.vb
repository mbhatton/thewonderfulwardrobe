﻿Imports System.Web.Mvc
Imports WonderfulWardrobe.Biz

Public Class ContactController
    Inherits Controller

    ' GET: /Contact

    Private _Contact As IContact

    Public Sub New(contact As IContact)
        _Contact = contact
    End Sub

    Function Index() As ActionResult
        Return View()
    End Function


    Function Contact(_ContactUs As ContactViewModel) As ActionResult
        _Contact.CreateContact(_ContactUs)
        ViewBag.Message = "Thanks Our Support will contact you soon."
        ModelState.Clear()
        Return View("Index")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function ContactDetails() As ActionResult
        Dim list As List(Of ContactViewModel)
        list = _Contact.BindContactsList()
        Dim viewModel As ContactViewModel = New ContactViewModel
        viewModel.ContactsList = list
        ViewBag.Message = TempData("Message")
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function Details(ContactId As Integer) As ActionResult
        Dim Info As ContactViewModel
        Info = _Contact.ContactInfo(ContactId)
        Return View(Info)
    End Function
End Class