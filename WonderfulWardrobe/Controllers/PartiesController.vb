﻿Imports System.Web.Mvc
Imports WonderfulWardrobe.Biz
Imports System.Globalization
Imports System.IO
Imports System.Net
Imports System.Xml.Serialization
Imports System.Xml
Imports Json
Imports System.Net.Http
Imports Newtonsoft.Json

Public Class PartiesController
    Inherits Controller

    ' GET: /Parties
    Private _Parties As IParties
    Public Sub New(parties As IParties)
        _Parties = parties
    End Sub
    Function Index() As ActionResult
        Return View()
    End Function

    <HttpPost>
    Function Index(enquiry As PartyEnquiryViewModel, form As FormCollection) As ActionResult
        enquiry.Duration = form("timeduration")
        _Parties.CreatePartyEnquiry(enquiry)
        ModelState.Clear()
        ViewBag.Message = "Thanks Our Support will contact you soon."
        Return View()
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function PartyEnquiryList() As ActionResult
        Dim list As List(Of PartyEnquiryViewModel)
        list = _Parties.BindPartyEnquiryList()
        Dim viewModel As PartyEnquiryViewModel = New PartyEnquiryViewModel
        viewModel.PartyEnquiryList = list
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function ProcessEnquiry(EnquiryId As Integer) As ActionResult
        Dim enquiry As PartyEnquiryViewModel
        enquiry = _Parties.EnquiryDetails(EnquiryId)
        Return View(enquiry)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function SubmitSuggestion(form As FormCollection) As ActionResult
        Dim ViewModel As New PartyEnquiryViewModel
        Dim enquiry As New PartyEnquiryViewModel
        Dim SuggestedDateList As New List(Of PartyEnquiryViewModel)
        Dim enquiryId = form("hiddenid")
        Dim emailId = form("hiddenemail")
        ViewModel.EnquiryId = enquiryId
        ViewModel.Email = emailId
        Dim dateCount As String = form("count")
        Dim dateRemoveCount As String = form("removecount")
        Dim removedateArray As String()
        If Not dateRemoveCount Is Nothing Then
            removedateArray = dateRemoveCount.Split(",")
        Else
            removedateArray = New String(-1) {}
        End If

        If Not dateCount Is Nothing Then
            Dim dateArray As String() = dateCount.Split(",")
            Dim counter As Integer = Convert.ToInt32(dateArray.Last())
            For i As Integer = 2 To counter
                Dim pos As Integer = removedateArray.Contains(i)
                If pos = -1 Then

                Else
                    Dim _enquiry As New PartyEnquiryViewModel
                    _enquiry.SuggestedDate = form("date" + i.ToString())
                    _enquiry.CreatedOn = DateTime.Today
                    _enquiry.SuggestedTime = form("time" + i.ToString())
                    _enquiry.SuggestedDuration = form("timeduration" + i.ToString())
                    _enquiry.SuggestedPrice = form("partyprice" + i.ToString())
                    _enquiry.RequiredAmount = form("requiredpayment" + i.ToString())
                    _enquiry.IsActive = True
                    SuggestedDateList.Add(_enquiry)
                End If

            Next
        End If
        ViewModel.PartyEnquiryList = SuggestedDateList
        _Parties.SaveSuggestion(ViewModel)
        Return RedirectToAction("PartyEnquiryList")
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function ProcessedEnquiries() As ActionResult
        Dim list As List(Of PartyEnquiryViewModel)
        list = _Parties.BindprocessedEnquiryList()
        Dim viewModel As PartyEnquiryViewModel = New PartyEnquiryViewModel
        viewModel.PartyEnquiryList = list
        Return View(viewModel)

    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function EnquiryDetails(EnquiryId As Integer) As ActionResult
        Dim list As List(Of PartyEnquiryViewModel)
        list = _Parties.ProcessedEnquiryDetails(EnquiryId)
        Dim viewModel As PartyEnquiryViewModel = New PartyEnquiryViewModel
        viewModel.PartyEnquiryList = list
        Return View(viewModel)

    End Function

    <Authorize>
    Public Function BookParty(EnquiryId As Integer) As ActionResult
        TempData("EnquiryId") = EnquiryId
        Session("PartyDetails") = Nothing
        Session.Add("EnquiryId", EnquiryId)
        Return RedirectToAction("Booking", "Parties")
    End Function

    <Authorize>
    Function Booking() As ActionResult
        Dim EnquiryId As Integer = TempData("EnquiryId")
        Dim list As New List(Of PartyEnquiryViewModel)
        If EnquiryId = 0 Then
        Else
            list = _Parties.ProcessedEnquiryDetails(EnquiryId)
        End If
        If Session("PartyDetails") Is Nothing Then
            Dim viewModel As PartyBookingViewModel = New PartyBookingViewModel
            viewModel.SuggestionList = list
            Dim _family As List(Of FamilyViewModel)
            _family = _Parties.GetFamily()
            viewModel.family = _family
            Session.Add("PartyDetails", viewModel)
            Return View(viewModel)
        Else
            Dim viewModel As New PartyBookingViewModel
            viewModel = DirectCast(Session("PartyDetails"), PartyBookingViewModel)

            Return View(viewModel)
        End If
    End Function


    <Authorize>
    Public Function RemoveChild(id As Integer) As ActionResult
        Dim viewModel As New PartyBookingViewModel
        viewModel = DirectCast(Session("PartyDetails"), PartyBookingViewModel)
        Dim childResult = viewModel.family.Find(Function(item) item.Id = id)
        viewModel.family.Remove(childResult)
        Session.Add("PartyDetails", viewModel)
        Return RedirectToAction("Booking", "Parties")
    End Function

    <Authorize>
    Public Function RemoveParty(id As Integer) As ActionResult
        Dim viewModel As New PartyBookingViewModel
        viewModel = DirectCast(Session("PartyDetails"), PartyBookingViewModel)
        Dim PartyResult = viewModel.SuggestionList.Find(Function(item) item.EnquiryId = id)
        viewModel.SuggestionList.Remove(PartyResult)
        Session.Add("PartyDetails", viewModel)
        Return RedirectToAction("Booking", "Parties")
    End Function

    <Authorize>
    Public Function UndoParty() As ActionResult
        TempData("EnquiryId") = Session("EnquiryId")
        Session("PartyDetails") = Nothing
        Return RedirectToAction("Booking", "Parties")
    End Function

    <Authorize>
    Public Function PayNow(_ViewModel As PartyBookingViewModel, form As FormCollection, Command As String) As ActionResult
        If Command = "Add" Then
            Dim family As New FamilyViewModel
            Dim boy As String = form("Boy")
            Dim girl As String = form("Girl")
            Dim day As String = form("day").Replace(",", "")
            Dim month As String = form("month").Replace(",", "")
            Dim year As String = form("year").Replace(",", "")
            Dim dob As String = month + "/" + day + "/" + year
            Dim currentdate As DateTime = DateTime.Today
            family.DOB = DateTime.Parse(dob, CultureInfo.InvariantCulture)
            family.CreatedOn = DateTime.Today
            family.FirstName = form("child_first_name").Replace(",", "")
            family.LastName = form("child_last_name").Replace(",", "")
            family.Gender = form("childgender").Replace(",", "")
            family.IsActive = True
            Dim childid As String = _Parties.AddChild(family)
            Dim _childinfo As FamilyViewModel
            _childinfo = _Parties.GetChildInfo(childid)

            Dim viewModel As New PartyBookingViewModel
            viewModel = DirectCast(Session("PartyDetails"), PartyBookingViewModel)
            'Dim _family As List(Of FamilyViewModel)
            '_family = _Parties.GetFamily()
            viewModel.family.Add(_childinfo)
            Session("PartyDetails") = viewModel

            Return RedirectToAction("Booking", "Parties")
        Else
            Dim paymentmode As String = form("paymentoption")
            _ViewModel.PaymentMode = paymentmode
            _ViewModel.TotalAmount = Session("TotalAmount")
            Dim viewModel As New PartyBookingViewModel
            viewModel = DirectCast(Session("PartyDetails"), PartyBookingViewModel)
            _ViewModel.SuggestionList = viewModel.SuggestionList
            _ViewModel.family = viewModel.family
            Session("PartyDetails") = _ViewModel

            If _ViewModel.PaymentMode = "Paypal" Then
                Session.Add("PaymentMode", paymentmode)
                Session.Add("BookingType", "Party")
                Return RedirectToAction("CreatePartyPayment", "Paypal")

            Else
                _Parties.SaveParty(_ViewModel)
                Session("PartyDetails") = Nothing
                Session("TotalAmount") = Nothing
                Session("PaymentMode") = Nothing
                Session("BookingType") = Nothing
                Return RedirectToAction("ThankYou", "Purchase")
            End If
        End If
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function PartyBookingList() As ActionResult
        Dim list As List(Of PartyViewModel)
        list = _Parties.BindBookedPartyList()
        Dim viewModel As PartyBookingViewModel = New PartyBookingViewModel
        viewModel.PartyList = list
        Return View(viewModel)
    End Function

    <Authorize(Roles:="SuperAdmin,Admin")> _
    Function PartyDetails(PartyId As Integer) As ActionResult
        Dim partyInfo As PartyBookingViewModel
        partyInfo = _Parties.PartyDetailsbyId(PartyId)
        Return View(partyInfo)

    End Function



    
End Class