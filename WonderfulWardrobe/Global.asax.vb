﻿Imports System.Web.Optimization
Imports WonderfulWardrobe.Biz

Public Class MvcApplication
    Inherits System.Web.HttpApplication
    Protected Sub Application_Start()
        AreaRegistration.RegisterAllAreas()
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
        Dim bootstrap = New BootStrap()
        bootstrap.Configure()
    End Sub
End Class

