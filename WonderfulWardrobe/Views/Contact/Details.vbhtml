﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.ContactViewModel
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

<div class="row-fluid">
    <div class="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i>Contact Details</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body no-more-tables">
                <div class="row-fluid" style="float: none;">
                    <div class="span5" style="float: none;">

                        <div class="display-label" style="margin-bottom: 2px;">Name:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Name</div>
                        <br />
                        <div class="display-label" style="margin-bottom: 2px;">Email:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Email</div>
                        <br />
                        <div class="display-label" style="margin-bottom: 2px;">TelePhone:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.TelePhone</div>
                        <br />
                        <div class="display-label" style="margin-bottom: 2px;">Message:</div>
                            <div class="display-field" style="margin-bottom: 5px;">@Model.Message</div>
                        <br />

                        @Html.ActionLink("Back to List", "ContactDetails")

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







