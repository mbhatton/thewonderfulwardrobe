﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.ContactViewModel
@Code
    ViewData("Title") = "ContactDetails"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("ContactDetails")
    @<text>
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Contacts Details</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    TelePhone
                                </th>

                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As ContactViewModel In Model.ContactsList
                                @:<tr>
                                    @:<td>
                                        @items.Name
                                        @:</td>
                                    @:<td>
                                        @items.Email
                                        @:</td>
                                    @:<td>
                                        @items.TelePhone
                                        @:</td>
                                    @:<td>
                                      @Html.ActionLink("Details", "Details", "Contact", New With { _
                                            Key .ContactId = items.ContactId _
                                            }, Nothing) 
                                        @:</td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using

