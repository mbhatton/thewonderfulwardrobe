﻿@*@Code
    ViewData("Title") = "Index"
End Code

<h2>Index</h2>*@
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.ContactViewModel

<div class="body contact-page clearfix">

   
    <a href="#" class="facebook-like">Facebook Like</a>
    <aside class="left">
        <div class="urgant-notice">
            <h2>Urgent Notice Title</h2>
            <p>We are pleased to announce the return of our summer sessions! These MUST be pre-booked on the classes page and remember the ticket covers all of your children up to the age of 6.</p>
            <p>
        </div><!-- .urgant-notice ends -->
        <div class="image">
            <a href="#">
                <img src="~/Content/images/1.jpg" width="193" height="205" alt="We do Great parties" />
                <span class="caption">we do great parties >></span>
            </a>
        </div><!-- .image ends -->
        <div class="testimonal">
            <p>"We are pleased to announce the return of our summer sessions! These MUST be pre-booked on the classes page and remember the ticket covers all of your children up to the age of 6."</p>
            <h3>Mr Joe Bloggs, Cardiff</h3>
            <div class="tip"></div>
        </div><!-- .testimonal ends -->
    </aside><!-- .left ends -->
    <div class="page">
        <h1><span>Contact </span> the Wonderful Wardrobe</h1>
        <p>Feel free to contact us by the enquiry form below.</p>
        <p>Prefer to speak to someone? No problem!! Call Joanne on <a href="#">07972 115 096</a></p>
        <p>Hate the restriction of filling in a form? We understand you!! Send a mail to <a href="mailto:info@thewonderfulwardrobe.co.uk" title="Email us">info@thewonderfulwardrobe.co.uk</a></p>
        <div class="form-wrapper">
            @Using Html.BeginForm("Contact", "Contact", FormMethod.Post, New With {.class = "contact-form", .id = "contact-form", .role = "form"})
                @<text>
            @*<form action="#" method="post" id="contact-form">*@
                <h4>Contact Form</h4>
                <span class="required">*Required Fields</span>
                <div class="form-row">
                    <label for="name">Name*</label>
                    @Html.TextBoxFor(Function(m) m.Name, New With { _
                                Key .[placeholder] = "Enter your Name" _
                                })
                    @*<input type="text" name="name" id="name" placeholder="Enter your Name" />*@
                </div>
                <div class="form-row">
                    <label for="email">Email:</label>
                    @Html.TextBoxFor(Function(m) m.Email, New With { _
                                Key .[placeholder] = "Enter your Email id" _
                                })
                    @*<input type="email" name="email" id="email" placeholder="Enter your Email id" />*@
                </div>
                <div class="form-row">
                    <label for="telephone">Telephone</label>
                    @Html.TextBoxFor(Function(m) m.TelePhone, New With { _
                                Key .[placeholder] = "Your Telephone Number" _
                                })
                    @*<input type="tel" name="telephone" id="telephone" placeholder="Your Telephone Number" />*@
                </div>
                <div class="form-row">
                    <label for="message">Message</label>
                    @Html.TextAreaFor(Function(m) m.Message)
                    @*<textarea name="message" id="message"></textarea>*@
                </div>

            <div class="messages">
                <div class="messages error" style="text-align: center;">
                    <p style="color:red;">@ViewBag.Message</p>
                </div>
            </div>
                <div class="form-row">
                    <input type="submit" name="submit" id="submit" value="Send" class="btn pink-btn next" />
                </div>
            @*</form>*@
        </text>
            End Using
        </div><!-- .form-wrapper ends -->
    </div><!-- .page ends -->
</div>
<script>
    // FORM VALIDATION
    jQuery("#contact-form").validate({
        rules: {
            Name: "required",
            Email: {
                required: true,
                email: true
            },
            Message: "required",
        },
        messages: {
            Name: "",
            Email: "",
            Message: "",
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>