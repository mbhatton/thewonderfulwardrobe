﻿@ModelType WonderfulWardrobe.Biz.AccountViewModel
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Scripts.Render("~/bundles/jqueryval")
@Using Html.BeginForm("Index", "SuperAdmin", FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Create Admin</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">

                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <div class="row-fluid" style="float: none;">
                            <div class="span5" style="float: none;">

                                <label for="first_name">First Name:</label>
                                 @Html.TextBoxFor(Function(m) m.FirstName, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })
                               
                                <label for="last_name">Last Name:</label>
                                @Html.TextBoxFor(Function(m) m.LastName, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="email_id">Email Id:</label>
                                @Html.TextBoxFor(Function(m) m.EmailId, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="password">Password:</label>
                               @Html.PasswordFor(Function(m) m.Password, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="retype_password">Retype Password:</label>
                                <input type="password" name="retype_password" class="span12 m-wrap" id="retype_password" />

                                <label for="password_hint">Password Hint:</label>
                                @Html.TextBoxFor(Function(m) m.PasswordHint, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })
                                <br />
                                <br />
                                <input type="submit" name="submit" class="btn" value="Save" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </text>
End Using

<script>
    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            FirstName: "required",
            LastName: "required",
            EmailId: {
                required: true,
                email: true
            },
            Password: {
                required: true,
                minlength: 8
            },

            retype_password: {
                minlength: 8,
                equalTo: "#Password"
            },
            PasswordHint: "required",
        },
        messages: {
            FirstName: "*Required Field",
            LastName: "*Required Field",
            EmailId: "*Invalid Email Address",
            Password: "*Minimum of 8 characters with atleast 1 special charecter required",
            retype_password: "*Passwords do not match",
            PasswordHint: "*Required Field",
        },
        submitHandler: function (form) {
            var name = $("#Password").val();
            var errors;
            if (!isLetters(name)) {
            }
            function isLetters(str) {
                if (/^[a-zA-Z0-9- ]*$/.test(str) == false) {
                    form.submit();
                }
                else {
                    errors = {
                        Password: "*Incorrect Format"
                    }
                    validator.showErrors(errors);
                }
            }

        }
    });

</script>


<div style="padding-bottom:10px;">
    @Html.ActionLink("Back to List", "AdminList")
</div>


