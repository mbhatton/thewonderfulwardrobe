﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.AccountViewModel
@Code
    ViewData("Title") = "AdminList"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("Index")
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Admin List</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        @Html.ActionLink("Create New", "Index")
                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                   User Name
                                </th>
                                <th>
                                   Email Id
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As AccountViewModel In Model.AdminList
                                @:<tr>
                                    @:<td>
                                        @items.FirstName
                                        @:</td>
                                    @:<td>
                                        @items.EmailId
                                        @:</td>
                                   
                                    @:<td>
                                        @Html.ActionLink("Delete", "DeleteAdmin", "SuperAdmin", New With { _
                                            Key .UserName = items.EmailId _
                                            }, Nothing)
                                        @: </td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using




