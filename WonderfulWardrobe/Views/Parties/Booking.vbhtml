﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.PartyBookingViewModel
@*@Scripts.Render("~/bundles/jqueryval")*@

<style>
    .delete-btn {
        width: 18px;
        height: 18px;
        display: block;
        text-indent: -9999px;
        background: url('/Content/images/sprite.png') no-repeat -127px -583px;
    }

    .paymentmode {
        color: white !important;
    }
    .Addchildbtn {
        background: linear-gradient(#E8AAD2, #BD5999) repeat scroll 0 0 rgba(0, 0, 0, 0) !important;
        color: white !important;
    }
</style>
<div class="body parties-booking-page clearfix">
  
     
    
      <a href="#" class="facebook-like">Facebook Like</a>
    <div class="page">
        <h1>Your Booking</h1>
        <p class="note">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo fugiat nulla pariatur.</p>
        <div class="form-wrapper">
            @Using Html.BeginForm("PayNow", "Parties", FormMethod.Post, New With {.class = "form-horizontal", .id = "parties-booking-detail", .role = "form"})
                @<text>
            @*<form action="#" method="post" id="parties-booking-detail">*@
                <h2 class="following-heading">The Following dates are available <em>Please remove unwanted dates </em>
             @*<a href="#" title="Undo your list">Undo</a>*@
                    @Html.ActionLink("Undo", "UndoParty", "Parties")
            </h2>
                <div class="detail">
                    <div class="form-row">
@If 1 < 2 Then
                    Dim amount As Decimal = 0
                    Dim requiredamount As Decimal = 0
                        @<table>
                            <thead>
                                <tr>
                                    <th class="date">Date</th>
                                    <th class="time">Time</th>
                                    <th class="duration">Durations</th>
                                    <th class="price">Price</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>

    
                                @For Each item As PartyEnquiryViewModel In Model.SuggestionList
                                     @<tr>
                                    <td>@item.SuggestedDate.ToString("MMM dd yyy")</td>
                                    <td>@item.SuggestedTime</td>
                                    <td>@item.SuggestedDuration Hrs</td>
                                    <td>&#163; @item.Price.ToString("N2")</td>
                                    @*<td class="close"><span><a href="#" class="close-btn">Close</a></span></td>*@
                                          <td class="">
                                              <span style="width:55px;">
                                                  @Html.ActionLink("Close", "RemoveParty", "Parties", New With { _
                                                  Key .id = item.EnquiryId _
                                                  }, New With { _
    Key .[class] = "delete-btn" _
})
                                              </span>
                                          </td>
                                </tr>
                                amount += item.Price
                                requiredamount += item.RequiredAmount
                                Session("TotalAmount") = Nothing
                                Session.Add("TotalAmount", amount.ToString("N2"))
                                Session("RequiredAmount") = Nothing
                                Session.Add("RequiredAmount", requiredamount.ToString("N2"))
                                Next
                            </tbody>
                        </table>
                        @<div class="total-amount clearfix">

                            <span class="amount">Total: &#163;@amount.ToString("N2")</span>
                             <input type="hidden" name="totalamount" id="totalamount" value="@amount.ToString("N2")" />
                             <input type="hidden" name="totalsuggestionCount" id="totalsuggestionCount" value="@Model.SuggestionList.Count" />
                            <span class="pay-now"><input type="button" name="add" value="Next" class="btn nextinfo pink-btn" /></span>
                        </div>
End If<!-- .total-amount ends -->
                    </div><!-- div.form-row -->
                   
                   
                </div><!-- .detail ends -->
                <label id="lblMessage" style="color: red; width: 135px; float: right;"></label>
            
                <div id="showinfo" class="">
                    <h4>Yout Details</h4>
                    <div>
                        <div class="form-row">
                            <label for="first_detail_name">First Name:</label>
                            @Html.TextBoxFor(Function(m) m.FirstName)
                            @*<input type="text" name="first_detail_name" id="first_detail_name" />*@
                        </div><!-- .form-row -->
                        <div class="form-row">
                            <label for="last_detail_name">Last Name:</label>
                            @Html.TextBoxFor(Function(m) m.LastName)
                            @*<input type="text" name="last_detail_name" id="last_detail_name" />*@
                        </div><!-- .form-row -->
                        <div class="form-row">
                            <label for="address1">Address:</label>
                            @Html.TextBoxFor(Function(m) m.Address1)
                            @*<input type="text" name="address1" id="address1" />*@
                        </div><!-- .form-row -->
                        <div class="form-row">
                            <label>&nbsp;</label>
                            @*<input type="text" name="address2" id="address2" />*@
                            @Html.TextBoxFor(Function(m) m.Address2)
                        </div><!-- .form-row -->
                        <div class="form-row">
                            <label>&nbsp;</label>
                            @Html.TextBoxFor(Function(m) m.Address3)
                            @*<input type="text" name="address3" id="address3" />*@
                        </div><!-- .form-row -->
                        <div class="form-row">
                            <label for="postcode">Postcode:</label>
                            @Html.TextBoxFor(Function(m) m.PostalCode)
                            @*<input type="text" name="postcode" id="postcode" />*@
                        </div><!-- .form-row -->
                        <div class="form-row">
                            <label for="telenumber">Tel Number:</label>
                            @Html.TextBoxFor(Function(m) m.TelePhone)
                            @*<input type="tel" name="telenumber" id="telenumber" />*@
                        </div><!-- .form-row -->
                    </div><!-- DIV ends -->
                    <h4>Name(s) of the children the party is for</h4>

                    <div class="child-detail" id="classchild-1">
                        <div class="form-row">
                            <label for="child_first_name">First Name:</label>
                            <input type="text" name="child_first_name" id="child_first_name" />
                        </div>
                        <div class="form-row">
                            <label for="child_last_name">Last Name:</label>
                            <input type="text" name="child_last_name" id="child_last_name" />

                        </div>
                        <div class="form-row dob">
                            <label for="day">Date of Birth:</label>
                            <input type="text" name="day" id="day" class="mini" />
                            <span class="seprator">/</span>
                            <input type="text" name="month" id="month" class="mini" />
                            <span class="seprator">/</span>
                            <input type="text" name="year" id="year" class="small" /><label for="format">dd/MM/yyyy</label>

                        </div>
                        <div class="form-row radio childgender">
                            <input type="radio" id="boy" value="Boy" name="gender" checked />
                            <label for="boy">Boy</label>
                            <input type="radio" id="girl" value="Girl" name="gender" />
                            <label for="girl">Girl</label>
                            <input type="hidden" id="childgender" value="Boy" name="childgender" />
                        </div>
                        <div class="form-row add-btn">
                            <input type="submit" name="Command" style="font: bold 15px/1.75" class="btn pink-btn addchildren Addchildbtn cancel" value="Add" />
                            @*<input type="button" id="add" name="add" class="btn pink-btn button" value="Add" />*@
                        </div>
                        <!-- .child-list -->
                    </div><!-- .child-detail ends -->
                    <div class="child-list" id="child-list">
                        <table>
                            <tbody>
                                @If Model.family.Count > 0 Then

                                    @For Each item As FamilyViewModel In Model.family
                                        @<tr>
                                            <td class="child-name">
                                                <label for="childname" style="text-align:left">@item.FirstName @item.LastName</label>
                                                <input class="ss" type="hidden" id="@item.Id" name="@item.Id" value="@item.FirstName @item.LastName" />
                                            </td>
                                            <td class="dob">
                                                -D.O.B
                                                <label for="DOB" style="text-align:left">@item.DOB.ToShortDateString()</label>
                                                <input type="hidden" id="DOB" name="DOB" value="@item.DOB" />
                                            </td>
                                            <td class="gender">
                                                <label for="gender" style="text-align:left">@item.Gender</label>
                                                <input type="hidden" id="gender" name="gender" value="@item.Gender" />
                                            </td>
                                            <td class="">
                                                <span>
                                                    @Html.ActionLink("Close", "RemoveChild", "Parties", New With { _
                                                    Key .id = item.Id _
                                                    }, New With { _
    Key .[class] = "delete-btn" _
})
                                                </span>
                                            </td>

                                        </tr>
                                    Next
                                    End If
                            </tbody>
                        </table>
                    </div>


                    <h4>Party Details</h4>
                    <div>
                        <div class="form-row">
                            <label for="party_reason">Reason for Party?</label>
                            @*<textarea name="party_reason" id="party_reason"></textarea>*@
                            @Html.TextAreaFor(Function(m) m.PartyReason)
                        </div><!-- .form-row -->
                        <div class="form-row">
                            <label for="party_location">Party Location:</label>
                            @Html.TextAreaFor(Function(m) m.PartyLocation)
                            @*<textarea name="party_location" id="party_location"></textarea>*@
                        </div><!-- .form-row -->
                        <div class="form-row">
                            <label for="children_attending">Number of children attending:</label>
                            @Html.TextBoxFor(Function(m) m.ChildrenAttending, New With { _
                                Key .[class] = "mini" _
                                })
                            @*<input type="text" name="children_attending" id="children_attending" class="mini" />*@
                        </div><!-- .form-row ends -->
                    </div><!-- DIV ends -->
                    <p class="note">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    
                    @If Model.SuggestionList.Count > 0 Then
                        @<div class="total-amount clearfix">

                             @If 1 < 2 Then
                             Dim NetAmount As Decimal = Session("TotalAmount")
                             Dim NetRequiredAmount As Decimal = Session("RequiredAmount")
                                 @<span class="amount">Total: &#163;@NetAmount.ToString("N2") Deposit payment required of &#163;@NetRequiredAmount.ToString("N2")</span>
                             End If
                            <div class="form-row amount option radio" style="padding-left: 0px; width: 255px;">
                                <input type="radio" id="cash" value="Cash" name="payment" checked />
                                <label class="paymentmode" for="cash">Cash</label>
                                <input type="radio" id="paypal" value="Paypal" name="payment" />
                                <label class="paymentmode" for="paypal">Paypal</label>
                                <input type="hidden" id="paymentoption" value="Cash" name="paymentoption" />
                            </div>
                            <span class="pay-now">

                                <span class="pay-now"><input type="submit" name="Command" value="Pay Deposit" class="btn pink-btn paid formsubmit" />
    @*<input type="submit" name="pay_deposit" value="Pay Deposit" class="btn pink-btn formsubmit" />*@</span>
                            </span>
                        </div>
                    End If
                    <p class="validation-warning"><strong>*Please review errors above</strong></p>
                </div>
            @*</form>*@
        </text>
            End Using
        </div><!-- .form-wrapper ends -->
    </div><!-- .page ends -->
    <aside class="right">
        <div class="logged-in">
            <h2>Currently logged in as</h2>
            @If 1 < 2 Then
                Dim _user As String() = Membership.GetUser().UserName.Split("@")
                @<div class="user">@_user(0)</div>
                @<div class="logout">@Html.ActionLink("Logout", "Logout", "Account")</div>
            End If
           
           
        </div><!-- .logged-in -->
    </aside><!-- .right ends -->
   
</div>
<script>

    var amount = $("#totalamount").val();
    var totalcount = $("#totalsuggestionCount").val();
    if (totalcount > 1) {
        $("#showinfo").hide("fast");
    }else if (amount == 0) {
        $("#showinfo").hide("fast");
    }
    else {
        $("#showinfo").show("fast");
    }
    
    $('.nextinfo').click(function () {
        var amount = $("#totalamount").val();
        var totalcount = $("#totalsuggestionCount").val();
        if (amount == 0) {
            return false;
        }
        else if (totalcount > 1) {
            document.getElementById('lblMessage').innerHTML = 'Please Choose any one';
            return false;
        }
        else {
            $("#showinfo").show("fast");

            document.getElementById('lblMessage').innerHTML = '';
            return false;
        }
    });

    $('.formsubmit').click(function () {
        var payoption = $('.option input:radio:checked').val();

       $("#paymentoption").val(payoption);
       var paymentmode = $("#paymentoption").val();
    });
</script>

<script>


   
    var inputs = document.getElementsByTagName("input");
    for (var i = inputs.length - 1; i >= 0; i--) {
        if (inputs[i].getAttribute("type") === "radio") {
            inputs[i].checked = true;
        }
    }
    // ICHECK PLUGIN
    $(".radio input").iCheck({
        radioClass: 'iradio'
    });
    var i = 1;

    // SHOWS CHILD DETAIL ENDS
        $('.child-detail').show();
       
        $('.cancel').click(function () {
            var id = $(this).attr('id');
            validator.resetForm();
            $(".error").removeClass("error");
        });

    // FORM VALIDATION
    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            FirstName: "required",
            LastName: "required",
            Address1: "required",
            TelePhone: {
                required: true,
                number: true
            },
            PostalCode: {
                required: true,
                number: true
            },
            PartyReason: "required",
            PartyLocation: "required",
            ChildrenAttending: "required",
        },
        messages: {
            FirstName: "*Required Field",
            LastName: "*Required Field",
            Address1: "*Required Field",
            TelePhone: "*Number Only",
            PostalCode: "*Number Only",
            PartyReason: "*Required Field",
            PartyLocation: "*Required Field",
            ChildrenAttending: "*Required Field",
        },
        submitHandler: function (form) {
                    form.submit();
              

        }
    });

    // attach Validate plugin to form
    $("#form").validate();

    $('table tbody .close-btn').live('click', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var name = tr.find('.ss').attr('name');
        var array = name.split('e');
        var count = $('<input type="hidden" id="removecount" name="removecount" value="' + array[array.length - 1] + '" />');
        $('.child-list table').append(count);
        tr.fadeOut(400, function () {
            tr.remove();
        });
    });

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]);
    }
    // ASIGNING VALUES TO DYNAMICALY GANERATED TABLE ROW
    $('.addchildren').click(function () {
        child_fname = $('#child_first_name').val();
        child_lname = $('#child_last_name').val();
        child_day = $('#day').val();
        child_month = $('#month').val();
        child_year = $('#year').val();
        child_gender = $('.radio input:checked').val();

        var errors;
        if ($.trim($('#child_first_name').val()) == '') {
            errors = {
                child_first_name: "*Required Field"
            };
            validator.showErrors(errors);
            return false;

        } else if ($.trim($('#child_last_name').val()) == '') {
            errors = {
                child_last_name: "*Required Field"
            }
            validator.showErrors(errors);
            return false;

        } else if ($.trim($('#year').val()) == '') {
            errors = {
                year: "*Incorrect Format"
            }
            validator.showErrors(errors);
            return false;

        } else if (!$('input[name=gender]:checked').val()) {
            errors = {
                gender: "*Please Select"
            }
            validator.showErrors(errors);
            return false;

        } else if (child_year.length != 4) {
            errors = {
                year: "*Invalid Format"
            }
            validator.showErrors(errors);

        } else if (!isValidDate(child_day + '/' + child_month + '/' + child_year)) {
            errors = {
                year: "*Invalid Format"
            }
            validator.showErrors(errors);
            return false;

        } else {
            $("#childgender").val(child_gender);
        }
       
    });
</script>

@section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section