﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.PartyBookingViewModel
@Code
    ViewData("Title") = "PartyBookingList"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("PartyBookingList")
    @<text>
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Party Booking List</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">

                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    OrderNo
                                </th>
                                <th>
                                    UserName
                                </th>
                                <th>
                                    Party Date
                                </th>

                                <th>
                                    Time
                                </th>
                                <th>
                                    Duration(Hrs)
                                </th>
                                <th>
                                    Location
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Paid Amount
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As PartyViewModel In Model.PartyList
                                @:<tr>


                                    @:<td>
                                        @items.OrderId
                                        @:</td>
@:<td>


                            @If 1 < 2 Then
                                    Dim _user As String() =  items.UserName.Split("@")
                                @_user(0)

                            End If
                            @:</td>
                                    @:<td>
                                        @items.PartyDate.ToString("MMM dd yyy")
                                        @:</td>
                                    @:<td>
                                        @items.Time
                                        @:</td>
                                    @:<td>
                                        @items.Duration
                                        @:</td>
                                    @:<td>
                                        @items.Location
                                        @:</td>
                                    @:<td>
                                       @<span> &#163;</span>@items.Price.ToString("N2")
                                        @:</td>
 @:<td>
                                        @<span> &#163;</span>@items.PaidAmount.ToString("N2")
                                        @:</td>
                                    @:<td>
                                        @Html.ActionLink("Details", "PartyDetails", "Parties", New With { _
                                            Key .PartyId = items.PartyId _
                                            }, Nothing)
                                        @:</td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using




