﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.PartyEnquiryViewModel
@Code
    ViewData("Title") = "PartyEnquiryList"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("EnquiryDetails")
    @<text>
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Party Enquiry Details</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                      
                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    PhoneNumber
                                </th>

                                <th>
                                   1st PreferedDate
                                </th>
                                <th>
                                   2nd PreferedDate
                                </th>
                                <th>
                                    Prefered Start Time
                                </th>
                                <th>
                                    Duration(Hrs)
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As PartyEnquiryViewModel In Model.PartyEnquiryList
                                @:<tr>
                               
                                    @:<td>
                                        @items.Name
                                        @:</td>
                                    @:<td>
                                        @items.Email
                                        @:</td>
                                    @:<td>
                                        @items.PhoneNumber
                                        @:</td>
 @:<td>
                                        @items.PreferedDate1.ToString("MMM dd yyy")
                                        @:</td>
 @:<td>
                                         @items.PreferedDate2.ToString("MMM dd yyy")
                                        @:</td>
 @:<td>
                                        @items.PreferedStartTime
                                        @:</td>
                                    @:<td>
                                        @items.Duration
                                        @:</td>
 @:<td>
                                         @Html.ActionLink("Process", "ProcessEnquiry", "Parties", New With { _
                                            Key .EnquiryId = items.EnquiryId _
                                            }, Nothing) 
                                        @:</td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using
