﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.PartyBookingViewModel
@Code
    ViewData("Title") = "PartyDetails"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code


@Using Html.BeginForm("PartyDetails")
    @<text>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i>Party Details</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body no-more-tables">
                <div class="row-fluid" style="float: none;">
                    <div class="span5" style="float: none;">
                        @For Each item As PartyViewModel In Model.PartyList
                                @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">UserName:</div>
                             @If 1 < 2 Then
                                     Dim _user As String() = item.UserName.Split("@")
                                
 @<div class="display-field" style="margin-bottom: 5px;">@_user(0)</div>
                            End If
                       @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Name:</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@Model.FirstName @Model.LastName</div>
                            
                            @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Address:</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@Model.Address1 @Model.Address2 @Model.Address3</div>
                            
                            @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Telephone:</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@Model.TelePhone</div>
                            
  @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">OrderNo:</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@item.OrderId</div>
                            
                        @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Party Date:</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@item.PartyDate.ToString("MMM dd yyy")</div>

                        @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Time:</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@item.Time</div>

                        @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Duration(Hrs):</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@item.Duration</div>

                        @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Price:</div>
                       @<div class="display-field" style="margin-bottom: 10px;"> &#163;@item.Price.ToString("N2")</div>
                            
                             @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">PaidAmount:</div>
                       @<div class="display-field" style="margin-bottom: 10px;"> &#163;@item.PaidAmount.ToString("N2")</div>

                        @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Location:</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@item.Location</div>
                            
                            @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Reason:</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@item.Reason</div>
                            
                            @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">No of Children:</div>
                        @<div class="display-field" style="margin-bottom: 10px;">@item.NoOfChildren</div>
                            
                             @<div class="display-label" style="margin-bottom: 2px; font-weight: bold;">Party For:</div>
                             @For Each items As FamilyViewModel In Model.family
                                     
                        @<div class="display-field" style="margin-bottom: 10px;">@items.FirstName @items.LastName</div>
                                 Next


Next
                   
                            @Html.ActionLink("Back to List", "PartyBookingList")
                        
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</text>
End Using