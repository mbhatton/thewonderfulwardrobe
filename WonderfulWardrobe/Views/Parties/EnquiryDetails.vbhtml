﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.PartyEnquiryViewModel
@Code
    ViewData("Title") = "EnquiryDetails"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("ProcessedEnquiryDetails")
    @<text>
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Processed Enquiry Details</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">

                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                   Suggested Date
                                </th>
                                <th>
                                    Suggested Time
                                </th>
                                <th>
                                    Suggested  Duration(Hrs)
                                </th>

                                <th>
                                    Price
                                </th>
                                <th>
                                    Deposit Required
                                </th>
                               
                            </tr>

                            @For Each items As PartyEnquiryViewModel In Model.PartyEnquiryList
                                @:<tr>

                                    @:<td>
                                        @items.SuggestedDate.ToString("MMM dd yyy")
                                        @:</td>
                                    @:<td>
                                        @items.SuggestedTime
                                        @:</td>
                                    @:<td>
                                        @items.SuggestedDuration 
                                        @:</td>
                                    @:<td>

                                       @<span> &#163;</span>@items.Price.ToString("N2")
                                        @:</td>
 @:<td>
                                        @<span> &#163;</span>@items.RequiredAmount.ToString("N2")
                                        @:</td>
                                  
                                @:</tr>
                            Next
                        </table>
                    </div>
                </div>
                @Html.ActionLink("Back To List", "ProcessedEnquiries", "Parties")
            </div>
        </div>
    </text>
End Using

