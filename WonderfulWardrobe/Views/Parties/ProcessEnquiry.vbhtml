﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.PartyEnquiryViewModel
@Code
    ViewData("Title") = "ProcessEnquiry"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code
<link href="~/Content/datepicker.css" rel="stylesheet" />
@*<link href="~/Content/bootstrap-datetimepicker.min.css" rel="stylesheet" />*@
<script src="~/Content/bootstrap-datepicker.js"></script>
@Scripts.Render("~/bundles/jqueryval")
@Using Html.BeginForm("SubmitSuggestion", "Parties", FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})
    @<text>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i>Party Enquiry Details</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body no-more-tables">

                <table class="table-bordered table-striped table-condensed t-cus">
                    <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            PhoneNumber
                        </th>

                        <th>
                            1st PreferedDate
                        </th>
                        <th>
                            2nd PreferedDate
                        </th>
                        <th>
                            Prefered Start Time
                        </th>
                        <th>
                            Duration(Hrs)
                        </th>
                        
                    </tr>

                        <tr>
                            <td>
                                @Model.Name
                                <input type="hidden" name="hiddenid" id="hiddenid" value="@Model.EnquiryId" />
                                </td>
                           <td>
                                @Model.Email
                               <input type="hidden" name="hiddenemail" id="hiddenemail" value="@Model.Email" />
                                </td>
                            <td>
                                @Model.PhoneNumber
                                </td>
                            <td>
                                @Model.PreferedDate1.ToString("MMM dd yyy")
                                </td>
                            <td>
                                @Model.PreferedDate2.ToString("MMM dd yyy")
                               </td>
                           <td>
                                @Model.PreferedStartTime
                               </td>
                           <td>
                                @Model.Duration
                               </td>
                            </tr>
                          
                </table>
            </div>
        </div>
    </div>
</div>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Add Suggested Dates</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">

                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>
                        
                        <div class="row-fluid"  style="float: none;">
                            <div class="span5 adddates" id="adddates" style="float: none;">


                                <label for="SuggestedDate">Date</label>
                                <input type="text" name="datetimepicker" id="datetimepicker" class="span11 m-wrap" />


                                <label for="time">Time(AM/PM):</label>
                                <input type="text" name="time" id="time" class="span11 m-wrap" />

                                <label for="duration">Duration(Hrs):</label>
                                <input type="text" name="timeduration" id="timeduration" class="span11 m-wrap" />

                                <label for="price">Price(£):</label>
                                <input type="text" name="partyprice" id="partyprice" class="span11 m-wrap" />

                                <label for="price">Deposit Payment Required(£):</label>
                                <input type="text" name="paymentrequired" id="paymentrequired" class="span11 m-wrap" />
                                <br />
                                <br />
                                <input type="button" name="AddDate" id="AddDate" class="btn" value="Add" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


<div class="row-fluid">
    <div class="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i>Party Suggested Dates</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body no-more-tables">

                <table class="table-bordered table-striped table-condensed t-cus suggestiontable" id="suggestedtable">
                    <tr>
                        <th>
                            Date
                        </th>
                        <th>
                            Time
                        </th>
                        <th>
                            Duration(Hrs)
                        </th>

                        <th>
                           Price
                        </th>
                        <th>
                            Deposit Payment Required
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>

                    

                </table>
            </div>
        </div>
    </div>
</div>
<div style="text-align: right;">
    <input type="submit" name="Submit" id="Submit" class="btn submitdata" value="Submit" />
</div>
</text>
End Using
   <script type="text/javascript">
    $(function () {
        $('#datetimepicker').datepicker({
            autoclose: true
        });
    });
</script>
<script type="text/javascript">
    var i = 1;
    $(function () {
        $(document).on('click', '.removedate', function () {
            var tr = $(this).closest('tr');
            var name = tr.find('.ss').attr('name');
            var array = name.split('e');
            var count = $('<input type="hidden" id="removecount" name="removecount" value="' + array[array.length - 1] + '" />');
            $('.suggestiontable').append(count);
            tr.fadeOut(400, function () {
                tr.remove();
            });
           
        })
    })
      
    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            
        },
        messages: {
           
        },
        submitHandler: function (form) {
            var errors;
            var name = $(".removedate").attr("id");
                if (name!=null) {
                    form.submit();
                    
                }
                else {
                    errors = {
                        partyprice: "*No data found"
                    }
                    validator.showErrors(errors);
                }
        }
    });
    $('#AddDate').click(function () {
        selecteddate = $('#datetimepicker').val();
        selectedtime = $('#time').val();
        timeduration = $('#timeduration').val();
        partyprice = $('#partyprice').val();
        paymentrequired = $('#paymentrequired').val();

        var errors;
        if ($.trim($('#datetimepicker').val()) == '') {
            errors = {
               
                datetimepicker: "*Required Field"
            };
            validator.showErrors(errors);

        } else if ($.trim($('#time').val()) == '') {
            errors = {
                time: "*Required Field"
            }
            validator.showErrors(errors);

        } else if ($.trim($('#timeduration').val()) == '') {
            errors = {
                timeduration: "*Required Field"
            }
            validator.showErrors(errors);

        } else if ($.trim($('#timeduration').val()).match(/^\d+(?:\.\d+)?$/) == null) {
            errors = {
                timeduration: "*Only Decimal Number"
            }
            validator.showErrors(errors);

        } else if ($.trim($('#partyprice').val()) == '') {
            errors = {
                partyprice: "*Required Field"
            }
            validator.showErrors(errors);

        } else if ($.trim($('#partyprice').val()).match(/^\d+(?:\.\d+)?$/) == null) {
            errors = {
                partyprice: "*Only Decimal Number"
            }
            validator.showErrors(errors);

        } else if ($.trim($('#paymentrequired').val()) == '') {
            errors = {
                paymentrequired: "*Required Field"
            }
            validator.showErrors(errors);

        } else if ($.trim($('#paymentrequired').val()).match(/^\d+(?:\.\d+)?$/) == null) {
            errors = {
                paymentrequired: "*Only Decimal Number"
            }
            validator.showErrors(errors);

        } else {
            i++;
            var newrow = $('<tr><td><label for="date' + i + '" style="">' + selecteddate + '</label><input class="ss" type="hidden" id="date' + i + '" name="date' + i + '" value="' + selecteddate + '" /> </td><td ><label for="time' + i + '" style="">' + selectedtime + '</label><input type="hidden" id="time' + i + '" name="time' + i + '" value="' + selectedtime + '" /></td><td class=""> <label for="timeduration' + i + '" style="">' + timeduration + 'Hrs</label><input type="hidden" id="timeduration' + i + '" name="timeduration' + i + '" value="' + timeduration + '" /></td><td><label for="partyprice' + i + '" style="">£' + partyprice + '</label><input class="ss" type="hidden" id="partyprice' + i + '" name="partyprice' + i + '" value="' + partyprice + '" /> </td><td><label for="requiredpayment' + i + '" style="">£' + paymentrequired + '</label><input class="ss" type="hidden" id="requiredpayment' + i + '" name="requiredpayment' + i + '" value="' + paymentrequired + '" /> </td><td class="removedate" id="del"> <input type="button" name="close" id="close" class="btn" value="Delete" /></td></tr> ');
            $('.suggestiontable').append(newrow);
            $('#datetimepicker').val('');
            $('#time').val('');
            $('#timeduration').val('');
            $('#partyprice').val('');
            $('#paymentrequired').val('');
            validator.resetForm();
            var count = $('<input type="hidden" id="count" name="count" value="' + i + '" />');
            $('.suggestiontable').append(count);
        }
       
        
    });
    </script>