﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.PartyEnquiryViewModel

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<div class="body parties-homepage clearfix">

    
    <a href="#" class="facebook-like">Facebook Like</a>
    <div class="page">
        <h1>The Wonderful wardrobe <span>Parties</span></h1>
        <p class="note">A Wonderful Wardrobe party is a unique and enchanting experience for boys and girls...</p>
        <div class="intro">
            <img class="image-left" src="~/Content/images/3.jpg" width="187" height="187" alt="Image3" />
            <p>We'll write a special story all about the birthday child and their favourite things, but don't worry it's not a sit down and listen type of story... it's lots of up and about, music, rhymes, action songs and fun, fun, fun.</p>
            <p>The children will have plenty of costumes to choose from The Wonderful Wardrobe tree, whether they want to be pirates or princess, lions or horses, police officers or fairies. Then they'll be transported to a world of make believe... where anything can happen.</p>
            <a href="#" title="Find out more" class="find-more">Find out more >> </a></p>
        </div><!-- intro ends -->
        <div class="party-timming">
            <h2>Our Parties Time Slots</h2>
            <div class="clearfix">
                <div class="ante-meridian"><div><span class="clock">Clock</span><strong>AM <span>-</span></strong>Start at or before 11am</div></div>
                <div class="post-meridian"><div><span class="clock">Clock</span><strong>PM <span>-</span></strong>Start at or after 2pm</div></div>
                <div class="anytime"><div><span class="clock">Clock</span><strong>Anytime <span>-</span></strong>Booked outside above time slots or location above 25 minute radius from us </div></div>
            </div>
        </div><!-- .party-timming ends -->
        <div class="parties-prices">
            <h2>Our Parties Prices</h2>
            <ul class="clearfix">
                <li>1Hour - <span>&#163;100</span></li>
                <li>2Hour - <span>&#163;150</span></li>
            </ul>
            <p>Anytime (2 hour) or location above 25 minute radius from us <span>-</span> &#163;185</p>
        </div> <!-- .partie-prices ends -->
        <div class="form-wrapper">
            @Using Html.BeginForm("Index", "Parties", FormMethod.Post, New With {.class = "parties-homepage", .id = "parties-homepage", .role = "form"})
                @<text>
            @*<form action="#" method="post" id="parties-homepage">*@
                <h2>Enquiry about our availability for a Party</h2>
                <div>
                    <div class="form-row">
                        <label for="name">Name:</label>
                        @Html.TextBoxFor(Function(m) m.Name)
                        @*<input type="text" name="name" id="name" />*@
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="email">Email:</label>
                        @Html.TextBoxFor(Function(m) m.Email)
                        @*<input type="email" name="email" id="email" />*@
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="phonenumber">Phone Number:</label>
                        @Html.TextBoxFor(Function(m) m.PhoneNumber)
                        @*<input type="tel" name="phonenumber" id="phonenumber" />*@
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="preferred_date1">1st Preferred date:</label>
                       
                            @Html.TextBoxFor(Function(m) m.PreferedDate1, New With { _
                                Key .[class] = "datepicker" _
                                })
                            
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="preferred_date2">2nd Preferred date:</label>
                       
                        @Html.TextBoxFor(Function(m) m.PreferedDate2, New With { _
                                Key .[class] = "datepicker" _
                                })
                            
                        @*<input type="text" name="preferred_date2" id="preferred_date2" />*@
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="preferred_start_time">Preferred Start Time(AM/PM):</label>
                        @Html.TextBoxFor(Function(m) m.PreferedStartTime)
                        @*<input type="text" name="preferred_start_time" id="preferred_start_time" />*@
                    </div><!-- .form-row -->
                    <div class="form-row radio">
                        <input type="radio" id="1hour" value="1hour" name="hour" checked />
                        <label for="1hour">1 Hour</label>
                        <input type="radio" id="2hour" value="2hour" name="hour" />
                        <label for="2hour">2 Hour</label>
                        <input type="hidden" id="timeduration" value="1hour" name="timeduration" />
                    </div><!-- .form-row -->
                    <p style="color: red; text-align: center;">@ViewBag.Message</p>
                    <div class="form-row submit">
                        <input type="submit" id="sumbit" value="Send" class="btn pink-btn" />
                        <p class="validation-warning"><strong>*Please review errors above</strong></p>
                       
                    </div>
                </div>
        </text>
            End Using
            @*</form>*@
        </div><!-- .form-wrapper ends -->
    </div><!-- .page ends -->
    <aside class="right">
        <div class="twitter">
            <h2>Follow us >></h2>
            <div>
                <h3>Twitter Feed</h3>
                <ul>
                    <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                    <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                </ul>
            </div>
        </div><!-- .twitter ends -->
    </aside><!-- .right ends -->
</div>

<script>
    $(function () {
        $(".datepicker").datepicker();
    });
</script>

<script>
    // ICHECK PLUGIN
    $(".radio input").iCheck({
        radioClass: 'iradio'
    });
  

   $('#sumbit').click(function () {
       var timeduration = $('input:radio:checked').val();
       $("#timeduration").val(timeduration);
   });



    // FORM VALIDATION
    jQuery("#parties-homepage").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            Name: "required",
            Email: {
                required: true,
                email: true
            },
            PhoneNumber: {
                required: true,
                number: true
            },
            PreferedDate1: "required",
            PreferedDate2: "required",
            PreferedStartTime: {
                required: true
            },
            hour: "required"
        },
        messages: {
            Name: "*Required Field",
            Email: "*Enter valid Email",
            PhoneNumber: "*Required Field",
            PreferedDate1: "*Required Field",
            PreferedDate2: "*Required Field",
            PreferedStartTime: "*Required Field",
            hour: "*Required Field",
        }

    });
</script>