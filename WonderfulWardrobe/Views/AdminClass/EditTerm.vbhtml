﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.TermsViewModel
@Code
    ViewData("Title") = "EditTerm"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

<h2>EditTerm</h2>
<link href="~/Content/datepicker.css" rel="stylesheet" />
<script src="~/Scripts/bootstrap-datepicker.js"></script>
@Scripts.Render("~/bundles/jqueryval")

@Using Html.BeginForm("UpdateTerm", "AdminClass", FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Edit Term</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">

                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <div class="row-fluid" style="float: none;">
                            <div class="span5" style="float: none;">
                                <label for="term_id">Term ID:</label>
                                @Html.TextBox("TermId", Model.TermId, New With {.readonly = "readonly"})

                                <label for="term_name">Name:</label>
                                @Html.TextBox("Name", Model.Name, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="discount_amount">Discount Amount(£):</label>
                                @Html.TextBox("DiscountAmount", Model.DiscountAmount.ToString("N2"), New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="total_amount">Whole Term Price(£):(Optional)</label>
                                @Html.TextBoxFor(Function(m) m.TotalAmount, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })
                                @*<label for="start_date">Start Date:</label>
                                @Html.TextBox("StartDate", Model.StartDate.ToString("MM/dd/yyyy"), New With { _
                                Key .[class] = "span12 m-wrap datepicker" _
                                })

                                <label for="end_date">End Date:</label>
                                @Html.TextBox("EndDate", Model.EndDate.ToString("MM/dd/yyyy"), New With { _
                                Key .[class] = "span12 m-wrap datepicker" _
                                })*@
                                <br />
                                <br />
                                <input type="submit" name="submit" class="btn" value="Save" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </text>
End Using
<script type="text/javascript">
    $(document).ready(function () {

        $('.datepicker').datepicker({
            autoclose: true
        });
       
       
        @*var date = dateFormat(new Date("@Model.StartDate"));
        $('#StartDate').datepicker("setDate", date);
        alert("@Model.EndDate");
        $('#EndDate').datepicker("setDate", new Date("@Model.EndDate"));*@
    });

</script>

<script>
    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            Name: "required",
            DiscountAmount: {
                required: true,
                number: true
            },
            TotalAmount: {
                number: true
            },

        },
        messages: {
            Name: "*Required Field",
            DiscountAmount: "*Required Field and Decimal Value Only",
            TotalAmount: "*Decimal Value Only"

        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>


<div>
    @Html.ActionLink("Back to List", "TermsList")
</div>
