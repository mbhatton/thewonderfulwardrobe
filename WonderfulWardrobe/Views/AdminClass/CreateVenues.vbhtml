﻿@ModelType WonderfulWardrobe.Biz.VenueViewModel
@Code
    ViewData("Title") = "CreateVenues"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@*<style>
    .error {
        color: red !important;
    }
</style>*@

@Scripts.Render("~/bundles/jqueryval")
@*@Using Html.BeginForm("CreateVenues", "AdminClass", FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})*@
  @Using (Html.BeginForm("CreateVenues", "AdminClass", FormMethod.Post, New With { _
    Key .enctype = "multipart/form-data" _
, .class = "form-horizontal", .role = "form"}))
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Create Venues</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">

                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <div class="row-fluid" style="float: none;">
                            <div class="span5" style="float: none;">

                                <label for="Venue_day">Day:</label>
                                @Html.DropDownListFor(Function(m) m.Day, DirectCast(ViewBag.listitem, List(Of SelectListItem)), New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="Venue_name">Name:</label>
                                @Html.TextBoxFor(Function(m) m.Name, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })
                               
                                <label for="Venue_logo">Logo:</label>
                                <input type="file" name="file" class="span8 m-wrap" />
                             <br />
                             
                                <label for="address1">Address Line 1:</label>
                                @Html.TextBoxFor(Function(m) m.Address1, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="address2">Address Line 2:</label>
                                @Html.TextBoxFor(Function(m) m.Address2, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="address3">Address Line 3:</label>
                                @Html.TextBoxFor(Function(m) m.Address3, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })
                                
                                <label for="city">City:</label>
                                @Html.TextBoxFor(Function(m) m.City, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="postalcode">PostalCode:</label>
                                @Html.TextBoxFor(Function(m) m.PostalCode, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <br />
                                <br />
                                <input type="submit" name="submit" class="btn" value="Save" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using
<script>
    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            Name: "required",
            Address1: "required",
            Address2: "required",
            Address3: "required",
            City: "required",
            PostalCode: "required",
           
        },
        messages: {
            Name: "*Required Field",
            Address1: "*Required Field",
            Address2: "*Required Field",
            Address3: "*Required Field",
            City: "*Required Field",
            PostalCode: "*Required Field",
           
        },
        submitHandler: function (form) {
            var Day = $("#Day").val();
            if (Day == "0") {
                errors = {
                    Day: "*Required Field",

                }
                validator.showErrors(errors);
            }
            else
            {
                form.submit();
            }
                    
        }
    });
    </script>
   
