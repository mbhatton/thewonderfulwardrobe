﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.VenueViewModel
@Code
    ViewData("Title") = "DetailVenue"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code
<div class="row-fluid">
    <div class="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i>Term Details</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body no-more-tables">
                <div class="row-fluid" style="float: none;">
                    <div class="span5" style="float: none;">

                        <div class="display-label" style="margin-bottom: 2px;">Day:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Day</div>

                        <div class="display-label" style="margin-bottom: 2px;">Name:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Name</div>

                        <div class="display-label" style="margin-bottom: 2px;">Logo:</div>
                        <img src="@Url.Content(Model.Logo)" id="logo" height="100" width="100" style="padding-top:5px" alt="" /> 

                        <div class="display-label" style="margin-bottom: 2px;">Address1:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Address1</div>

                        <div class="display-label" style="margin-bottom: 2px;">Address2:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Address2</div>

                        <div class="display-label" style="margin-bottom: 2px;">Address3:</div>
                        <div class="display-field" style="margin-bottom: 10px;">@Model.Address3</div>

                        <div class="display-label" style="margin-bottom: 2px;">City:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.City</div>

                        <div class="display-label" style="margin-bottom: 2px;">Postal Code:</div>
                        <div class="display-field" style="margin-bottom: 10px;">@Model.PostalCode</div>

                          @Html.ActionLink("Edit", "EditVenue", "AdminClass", New With { _
                                            Key .id = Model.VenueId _
                                            }, Nothing)
                        &nbsp;&nbsp;
                        @Html.ActionLink("Back to List", "VenuesList")

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>