﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.OrderDetailsViewModel

@Code
    ViewData("Title") = "OrderDetails"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("BookedOrders")
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Order Details</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    Term
                                </th>
                                <th>
                                    Venue
                                </th>
                                <th>
                                    StartDateTime
                                </th>

                                <th>
                                    Duration
                                </th>

                                <th>
                                    Attending Child
                                </th>
                            </tr>

                            @For Each items As OrderDetailsViewModel In Model.OrderDetailList
                                @:<tr>
                                    @:<td>
                                        @items.Term
                                        @:</td>
 @:<td>
                                        @items.Venue
                                        @:</td>
                                    @:<td>
                                        @items.StartDateTime
                                        @:</td>
                                    @:<td>
                                        @items.Duration
                                        @:</td>
                                    @:<td>
                                         @items.FirstName  @items.LastName 
                                        @:</td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                   
                    
                </div>
                @Html.ActionLink("Back To List", "BookedOrders", "AdminClass")
            </div>
        </div>
    </text>
End Using


