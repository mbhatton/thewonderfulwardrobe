﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.VenueViewModel
@Code
    ViewData("Title") = "VenuesList"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("CreateVenues")
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Venues</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        @Html.ActionLink("Create New", "CreateVenues")
                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    Day
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Address1
                                </th>
                                <th>
                                    Address2
                                </th>
                                <th>
                                    Address3
                                </th>
                                <th>
                                    City
                                </th>
                                <th>
                                    Postal Code
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As VenueViewModel In Model.VenuesList
                                @:<tr>
 @:<td>
                                        @items.Day
                                        @:</td>
                                    @:<td>
                                        @items.Name
                                        @:</td>
                                    @:<td>
                                        @items.Address1
                                        @:</td>
                                    @:<td>
                                        @items.Address2
                                        @:</td>
                                    @:<td>
                                        @items.Address3
                                        @:</td>
                                    @:<td>
                                        @items.City
                                        @:</td>
                                 @:<td>
                                        @items.PostalCode
                                        @:</td>
                                    @:<td>
                                        @Html.ActionLink("Edit", "EditVenue", "AdminClass", New With { _
                                            Key .id = items.VenueId _
                                            }, Nothing) @:&nbsp;&nbsp;
                                        @Html.ActionLink("Details", "DetailVenue", "AdminClass", New With { _
                                            Key .id = items.VenueId _
                                            }, Nothing) @:&nbsp;&nbsp;
                                        @Html.ActionLink("Delete", "DeleteVenue", "AdminClass", New With { _
                                            Key .id = items.VenueId _
                                            }, Nothing)
                                        @:</td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using

