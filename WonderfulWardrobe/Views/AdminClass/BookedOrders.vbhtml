﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.CreatedOrdersViewModel
@Code
    ViewData("Title") = "BookedOrders"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("BookedOrders")
    @<text>
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Orders</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    Order No
                                </th>
                                <th>
                                    Order Date
                                </th>
                                <th>
                                    Family
                                </th>
                                 
                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As CreatedOrdersViewModel In Model.OrderList
                                @:<tr>
                                    @:<td>
                                        @items.OrderId
                                        @:</td>
                                    @:<td>
                                        @items.OrderDate.ToString("MMM dd yyy")
                                        @:</td>
                                    @:<td>
                                         @If 1 < 2 Then
                                                 Dim _user As String() = items.UserName.Split("@")
                 @_user(0)
               
            End If
                                        @:</td>
                                    @:<td>
                                        @Html.ActionLink("Details", "OrderDetails", "AdminClass", New With { _
                                            Key .OrderId = items.OrderId _
                                            }, Nothing) 
                                        @:</td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using