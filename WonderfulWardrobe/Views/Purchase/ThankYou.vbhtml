﻿@*@Code
    ViewData("Title") = "ThankYou"
End Code

<h2>ThankYou</h2>*@

<div class="body thankyou-page clearfix">
    <div class="page clearfix">
        <h1>Purchase Confirmation</h1>
        <div class="thankyou clearfix">
            <div class="girl-toy"><img src="~/Content/images/thankyou-character1.png" alt="girl" /></div>
            <div class="boy-toy"><img src="~/Content/images/thankyou-character2.png" alt="girl" /></div>
            <div class="content clearfix">
                <h1>Thank you</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
            </div><!-- .thankyou ends -->

        </div><!-- .thankyou ends -->
    </div><!-- .page ends -->
    <aside class="right">
        <div class="twitter">
            <h2>Follow us >></h2>
            <div>
                <h3>Twitter Feed</h3>
                <ul>
                    <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                    <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                </ul>
            </div><!-- DIV ends -->
        </div><!-- .twitter ends -->
    </aside><!-- .right ends -->
</div>