﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.ClassBookingViewModel
<table>
    <tbody>
        @If Model.TermList Is Nothing Then

        Else
            @For Each itemclass As TermsViewModel In Model.TermList
                @For Each classitem As ClassViewModel In itemclass.ClassList
                    Dim time As DateTime = DateTime.Today.Add(New TimeSpan(Convert.ToInt32(classitem.StartDateTime.TimeOfDay.Hours), Convert.ToInt32(classitem.StartDateTime.TimeOfDay.Minutes), 0))
                    Dim myNewTimeString As String = time.ToString("h:mm tt")
                    @<tr>
                        <td class="date"> @classitem.StartDateTime.ToString("MMM dd yyy")</td>
                        <td class="time">@myNewTimeString</td>
                        <td class="duration">@classitem.Duration Minutes</td>
                        <td class="price"> &#163;@classitem.Price.ToString("N2")</td>
                        <td class="add-btn">
                            @Html.ActionLink("Add", "BookClass", "Classes", New With { _
                            Key .id = classitem.ClassId, _
                            Key .type = "class", _
                            Key .VenueId = classitem.VenueId
                            }, New With { _
    Key .[class] = "btn pink-btn" _
})
                        </td>
                    </tr>
                Next
Next
        End If

    </tbody>
</table>