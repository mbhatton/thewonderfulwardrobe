﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.CustomerClassViewModel

<style>
    .span {
        color: #fff;
        width: auto;
        height: auto;
        border: 0 none;
        display: block;
        margin: 0 -2px;
        padding: 12px 0 15px;
        border-radius: 0 0 4px 4px;
        font: bold 15px/1.13 Arial, Helvetica, sans-serif;
        background: url('/Content/images/sprite.png') no-repeat -5px -662px;
        position: relative;
    }
</style>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5GjqnfoSwqzGDvABAwIsUfd6WDuvpO1s&sensor=true"></script>

   @Using (Html.BeginForm("Index", "Classes", FormMethod.Post, New With { _
    Key .enctype = "multipart/form-data" _
, .class = "form-horizontal", .role = "form"}))
    @<text>
<div class="classes-page">
    <div class="body clearfix">

       

        <a href="#" class="facebook-like">Facebook Like</a>
        <div class="page">
            <h1>The Wonderful Wardrobe <span>classes</span></h1>
            <p class="note">At our classes, our specially trained staff will transport your young children to the land of the Wonderful Wardrobe, through dressing up, music, action songs and rhymes. We also use footballs and a parachute to encourage co-ordination.</p>
            <div class="intro">
                <img class="image-left" src="~/Content/images/3.jpg" width="187" height="187" alt="Image3" />
                <p>In each 60 minute session we have 45 minutes of structured activity where the Magical Fairy and Pirate Peter will guide your child through a world of adventure. It will include songs, rhymes, poems and action songs, plus props such as puppets balls, hoops, ribbons and parachutes. We then have 15 minutes of relaxed free play – for example, playing with the puppets, making a treasure chest and exploring the instruments.</p>
                <p>We know that every child is different, so It doesn't matter if your child is shy.... or outgoing. 6 months old or 4 years.. each class is specially written to include everyone whatever their needs.</p>
                <p>Space is limited and our recent summer sessions sold out so please book early - we have to give priority to those booking for the term.  You can book online and pay by paypal, debit or credit card, or book online and pay on the day by cash or cheque.</p>
                <p>Please <a href="#" title="Get in touch">get in touch</a> for more information. We have classes running right now on at.....</p>
            </div><!-- intro ends -->
          
         
         <div class="venues">
                <h2>The  Venues our classes are held</h2>
                <ul class="clearfix">
                  
                    @If Model.VenuesList.Count > 0 Then
                            Dim totalcount = Model.VenuesList.Count
                            Dim count As Integer = 1
                        
                    @For Each items As VenueViewModel In Model.VenuesList
                            'Dim count As Integer = 1
                          
                    If count = 1 Then
                         @<li id="box" class="venues-1">
        <h2>@items.Day</h2>
        <span>
            <img src=@Url.Content(items.Logo) width="156" height="63" alt="Bolder" />
            @items.Address1,<br />@items.Address2,<br />@items.Address3
                @Html.ActionLink("Book Now", "Book", "Classes", New With { _
                Key .id = items.VenueId _
                }, New With { _
    Key .[class] = "active span" _
})
            
            </span>
    </li> 
                        count = count + 1
                    ElseIf count = 2 Then
                         @<li id="box" class="venues-2">
        <h2>@items.Day</h2>
        <span>
            <img src=@Url.Content(items.Logo) width="156" height="63" alt="Bolder" />
            @items.Address1,<br />@items.Address2,<br />@items.Address3
                @Html.ActionLink("Book Now", "Book", "Classes", New With { _
                Key .id = items.VenueId _
                }, New With { _
    Key .[class] = "active span" _
})
            
            </span>
    </li> 
                        count = count + 1
                    ElseIf count = 3 Then
                         @<li id="box" class="venues-3">
        <h2>@items.Day</h2>
        <span>
            <img src=@Url.Content(items.Logo) width="156" height="63" alt="Bolder" />
            @items.Address1,<br />@items.Address2,<br />@items.Address3
                @Html.ActionLink("Book Now", "Book", "Classes", New With { _
                Key .id = items.VenueId _
                }, New With { _
    Key .[class] = "active span" _
})
            
            </span>
    </li> 
                        count = count + 1
                    ElseIf count = 4 Then
                         @<li id="box" class="venues-4">
        <h2>@items.Day</h2>
        <span>
            <img src=@Url.Content(items.Logo) width="156" height="63" alt="Bolder" />
            @items.Address1,<br />@items.Address2,<br />@items.Address3
                @Html.ActionLink("Book Now", "Book", "Classes", New With { _
                Key .id = items.VenueId _
                }, New With { _
    Key .[class] = "active span" _
})
            
            </span>
    </li> 
                        count = 1
                   
                    Else
                        count = 1
                    End If
                    
Next
   End If
   
                </ul>
            </div><!-- venue ends -->
            <p style="color:red;">@ViewBag.Message</p>

        @If Model.TermsList.Count > 0 Then
                Dim VenueDetails = Model.VenuesList.Find(Function(item) item.VenueId = Session("Id"))
               @<div class="ticket-detail">
                <h2>@VenueDetails.Name Classes</h2>
            </div>
               End If

@For Each items As TermsViewModel In Model.TermsList
        @<div class="ticket-detail">
        <div>
    <span class="seasion-ticket">
        Seasion Tickets
    </span>
    <h3>@items.Name Classes</h3>
    <table>
        <thead>
            <tr>
                <th class="date">Date</th>
                <th class="time">Time</th>
                <th class="duration">Duration</th>
                <th class="price">Price</th>
                <th class="book-now">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @For Each classitem As ClassViewModel In items.ClassList
                    Dim VenueDetails = Model.VenuesList.Find(Function(item) item.VenueId = Session("Id"))
                    Dim time As DateTime = DateTime.Today.Add(New TimeSpan(Convert.ToInt32(classitem.StartDateTime.TimeOfDay.Hours), Convert.ToInt32(classitem.StartDateTime.TimeOfDay.Minutes), 0))
                    Dim myNewTimeString As String = time.ToString("h:mm tt")
        @<tr>
            <td>@VenueDetails.Day.Substring(0, 3) @classitem.StartDateTime.ToString("MMM dd yyy")</td>
            <td>@myNewTimeString</td>
            <td>@classitem.Duration Minutes</td>
            <td> &#163;@classitem.Price.ToString("N2")</td>
             <td>
                 @Html.ActionLink("Book Now", "BookClass", "Classes", New With { _
                Key .id = classitem.ClassId, _
                     Key .type = "class", _
                    Key .VenueId = VenueDetails.VenueId
                }, New With { _
    Key .[class] = "btn pink-btn" _
})
             </td>
        </tr>

    Next

        </tbody>
    </table>

</div>
     </div>
    If items.TotalAmount > 0 Then
         @<div class="term-ticket clearfix">
                <h2>Save £@items.DiscountAmount.ToString("N2") - <br />Book the whole term for £@items.TotalAmount</h2>
              @Html.ActionLink("Book Now", "BookClass", "Classes", New With { _
                Key .id = items.TermId, _
                     Key .type = "term", _
                    Key .VenueId = Session("Id")
                }, New With { _
    Key .[class] = "btn orange-btn" _
})
                @*<a href="#" class="btn orange-btn">Book Now</a>*@
            </div>
    End If
    

Next
@If Model.TermsList.Count > 0 Then
        Dim VenueDetails = Model.VenuesList.Find(Function(item) item.VenueId = Session("Id"))
     @<div id="map" class="boulder">
          <div id="mapheader" class="boulder-center clearfix">
              <h2>@VenueDetails.Name<span>@VenueDetails.Address1,@VenueDetails.Address2,@VenueDetails.Address3,@VenueDetails.City </span></h2>
              <img class="logo-img" src=@Url.Content(VenueDetails.Logo) width="123" height="49" alt="@VenueDetails.Name" />
          </div><!-- .boulder-center ends -->
            <div id="google-map"></div><!-- #google-map ends -->
        </div>
    End If
            <!-- #map ends -->
        </div><!-- .page ends -->
        <aside class="right">
            <div class="twitter">
                <h2>Follow us >></h2>
                <div>
                    <h3>Twitter Feed</h3>
                    <ul>
                        <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                        <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                    </ul>
                </div><!-- DIV ends -->
            </div><!-- .twitter ends -->
            <div class="ticket-cloumn sticky">
                <h2>Our Ticket Types</h2>
                <div class="row one">
                    <span class="ticket"></span>
                    <h3>Session<br />Family Ticket</h3>
                    <p>Covers all of your children for one session.</p>
                </div><!-- .row ends -->
                <div class="row two">
                    <span class="ticket"></span>
                    <h3>Term <br />Family Ticket</h3>
                    <p>Covers all of your children for all of the classes in the term and offers a discount over the individual session tickets.</p>
                </div><!-- .row ends -->
            </div><!-- .ticket-column ends -->
        </aside><!-- .right ends -->
    </div>
    </div>

</text>
End Using
                        @Scripts.Render("~/bundles/sticky")
<script>

    $('.venues li.venues-1').click(function () {
        $('.venues li.venues-2 a, .venues li.venues-3 a, .venues li.venues-4 a').removeClass('active');
        $(this).find('a').addClass('active');
        $('#map').removeClass('bijou sea-cadets funcky-chickens');
        $('#map').addClass('boulder');
        $('#mapheader').removeClass('boulder-center bijou-center sea-cadets-center funky-little-center clearfix');
        $('#mapheader').addClass('boulder-center clearfix');
        $('.boulder-center').show();
       
    });
    $('.venues li.venues-2').click(function () {
        $('.venues li.venues-1 a, .venues li.venues-3 a, .venues li.venues-4 a').removeClass('active');
        $(this).find('a').addClass('active');
        $('#map').removeClass('boulder sea-cadets funcky-chickens');
        $('#map').addClass('bijou');
        $('#mapheader').removeClass('boulder-center bijou-center sea-cadets-center funky-little-center clearfix');
        $('#mapheader').addClass('bijou-center clearfix');
        $('.bijou-center').show();
       
    });
    $('.venues li.venues-3').click(function () {
        $('.venues li.venues-1 a, .venues li.venues-2 a, .venues li.venues-4 a').removeClass('active');
        $(this).find('a').addClass('active');
        $('#map').removeClass('boulder bijou funcky-chickens');
        $('#map').addClass('sea-cadets');
        $('#mapheader').removeClass('boulder-center bijou-center sea-cadets-center funky-little-center clearfix');
        $('#mapheader').addClass('sea-cadets-center clearfix');
        $('.sea-cadets-center').show();
        
    });
    $('.venues li.venues-4').click(function () {
        $('.venues li.venues-1 a, .venues li.venues-2 a, .venues li.venues-3 a').removeClass('active');
        $(this).find('a').addClass('active');
        $('#map').removeClass('bijou boulder sea-cadets');
        $('#map').addClass('funcky-chickens');
        $('#mapheader').removeClass('boulder-center bijou-center sea-cadets-center funky-little-center clearfix');
        $('#mapheader').addClass('funky-little-center clearfix');
        $('.funky-little-center').show();
       
    });
   
</script>
    <script>

    if (($(window).width() > 966)) {
        //STICKY PLUGIN
        $(".ticket-cloumn").sticky({ topSpacing: 0, ClassName: 'wrap' });
    }
    var longitude=0;
    var latitude=0;
    //GOOGLE MAP
    if (@Model.TermsList.Count > 0)
    {
        longitude= @(Model.VenuesList.Find(Function(item) item.VenueId = 1).Longitude);
        latitude= @(Model.VenuesList.Find(Function(item) item.VenueId = 1).Latitude);
    }
   
    function initialize() {

        var myLatlng = new google.maps.LatLng(latitude, longitude);
        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("google-map"), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
        });

    }
    google.maps.event.addDomListener(window, 'load', initialize);

</script>
