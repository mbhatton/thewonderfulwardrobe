﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.ClassBookingViewModel
@*@Code
    ViewData("Title") = "Booking"
End Code
<h2>Booking</h2>*@
@Scripts.Render("~/bundles/icheck")
<style>
    .delete-btn {
        width: 18px;
        height: 18px;
        display: block;
        text-indent: -9999px;
        background: url('/Content/images/sprite.png') no-repeat -127px -583px;
    }
    .paymentmode {
        color: white !important;
    }
</style>
<div class="classes-page">
  
    <div class="body clearfix">

        
        <a href="#" class="facebook-like">Facebook Like</a>
        <div class="page" id="pagebook">
            <h1>Your <span>Classes booking</span></h1>
            <p class="note">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore<br /> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br /> consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            <div class="form-wrapper booking">

                    
                <form action="AddChild" method="post" class="form-horizontal" id="children">
                    @For Each itemclass As ClassBookingViewModel In Model.BookedClassList
                        @<div class="clearfix">
                            <span class="seasion-ticket">
                                Seasion Tickets
                            </span>
                            <h5><strong>@itemclass.VenueName</strong> - @itemclass.Address1 @itemclass.Address2 @itemclass.Address3 @itemclass.City</h5>
                            <div id="add-show">
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="date">Date</th>
                                            <th class="time">Time</th>
                                            <th class="duration">Duration</th>
                                            <th class="price">Price</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @For Each classitem As ClassViewModel In itemclass.CLassList
                                        Dim time As DateTime = DateTime.Today.Add(New TimeSpan(Convert.ToInt32(classitem.StartDateTime.TimeOfDay.Hours), Convert.ToInt32(classitem.StartDateTime.TimeOfDay.Minutes), 0))
                                        Dim myNewTimeString As String = time.ToString("h:mm tt")
                                            @<tr>
                                                <td>@itemclass.Day.Substring(0, 3) @classitem.StartDateTime.ToString("MMM dd yyy")</td>
                                                <td>@myNewTimeString</td>
                                                <td>@classitem.Duration Minutes</td>
                                                <td> &#163;@classitem.Price.ToString("N2")</td>
                                                <td class="">
                                                    <span>
                                                        @Html.ActionLink("Close", "RemoveClass", "Classes", New With { _
                Key .id = classitem.ClassId, _
                 Key .VenueId = itemclass.VenueId
                }, New With { _
    Key .[class] = "delete-btn" _
})
                                                    </span>
                                                </td>
                                            </tr>
                                        Next

                                    </tbody>
                                </table>
                            </div><!-- #add-show ends -->


                            <div id="show-detail">

                               
                                @Html.ActionLink("Add another+", "AddAnother", "Classes", New With { _
                Key .VenueId = itemclass.VenueId
                }, New With { _
    Key .[class] = "add-another" _
})

                               
                                @Html.Partial("~/Views/Classes/AddAnother.vbhtml", Model)
                            </div><!-- #show-detail ends -->
                            <div class="children-attending" id="classbooking">
                                <div class="clearfix">
                                    <h4>Children attending<span>Please select</span></h4>
                                  
                                    <a href="" id="@itemclass.VenueId" name="child-@itemclass.VenueId" class="add-another addchild">Add another child+</a>
                                </div>
                                <div class="form-row radio no-of-child">
                                    @Html.Hidden("Logo", Model.Logo)
                                    @For Each child As FamilyViewModel In Model.family
                                    Dim selectedid As String = child.Id.ToString() + "-" + itemclass.VenueId.ToString()
                                        @<div class="clearfix childdetails">
                                    
                                            @If Model.Logo.Contains(selectedid) Then
                                                @<input type="checkbox" class="testclass" id="child_name1" value="@child.Id-@itemclass.VenueId" checked name="@child.Id-@itemclass.VenueId" />
                                                @<label for="child_name">@child.FirstName  @child.LastName</label>
                                            
                                            Else
                                                @<input type="checkbox" class="testclass" id="child_name1" value="@child.Id-@itemclass.VenueId" name="@child.Id-@itemclass.VenueId" />
                                                @<label for="child_name">@child.FirstName  @child.LastName</label>
                                            End If
                                            @*@Html.CheckBox(child.Id)*@

                                        </div>

                                    Next
                                </div>
                            </div>



                        </div>
                        Dim amount As Decimal = 0
                        @If itemclass.BookingType = "term" Then
                            amount = itemclass.TotalTermAmount
                        Else
                           @For Each classitem As ClassViewModel In itemclass.CLassList
                            amount += classitem.Price
                        Next  
                        End If
                        itemclass.TotalAmount = amount
                       
                       @<div class="children-attending childattend" id="classchild-@itemclass.VenueId" style="display:none">
    <div class="child one">
        <div class="form-row">
            <label for="child_first_name">First Name:</label>

            <input type="text" name="child_first_name" id="child_first_name" />
        </div>
        <div class="form-row">
            <label for="child_last_name">Last Name:</label>
            <input type="text" name="child_last_name" id="child_last_name" />
        </div>
        <div class="form-row dob">
            <label for="day">Date of Birth:</label>
            <input type="text" name="day" id="day" class="mini" />
            <span class="seprator">/</span>
            <input type="text" name="month" id="month" class="mini" />
            <span class="seprator">/</span>
            <input type="text" name="year" id="year" class="small" /><label for="format">dd/MM/yyyy</label>
        </div>
        <div class="form-row radio">
            <input type="radio" id="boy" value="Boy" name="gender" checked />
            <label for="boy">Boy</label>
            <input type="radio" id="girl" value="Girl" name="gender" />
            <label for="girl">Girl</label>
            <input type="hidden" id="childgender" value="Boy" name="childgender" />
        </div>
        <div class="form-row add-btn">

            <input type="submit" name="Command" class="btn pink-btn button addchildren" value="Add" />
            @*<input type="button" name="add" class="btn pink-btn button" value="Add" />*@
            <input type="button" id="@itemclass.VenueId" name="reset" value="Cancel" class="cancel" />
        </div>
        <p class="validation-warning"><strong>*Validation message</strong></p>
    </div><!-- .child.one ends -->
    <!-- .child.two ends -->
</div>

                    Next
                        @If 1 < 2 Then
                            Dim netamount As Decimal = 0
                             @For Each item In Model.BookedClassList
                                netamount += item.TotalAmount
                             Next
                             Session("TotalAmount") = Nothing
                            Session("TotalAmount") = netamount
                        End If
                       

@If Model.BookedClassList.Count > 0 Then
                        @<div class="total-amount clearfix">

                            <span class="amount">Total: &#163;@Session("TotalAmount")</span>
                            <div class="form-row amount option radio" style="padding-left: 205px;">
                                <input type="radio" id="cash" value="Cash" name="payment" checked />
                                <label class="paymentmode" for="cash">Cash</label>
                                <input type="radio" id="paypal" value="Paypal" name="payment" />
                                <label class="paymentmode" for="paypal">Paypal</label>
                                <input type="hidden" id="paymentoption" value="Cash" name="paymentoption" />
                            </div>
                            <span class="pay-now">

                                <input type="submit" name="Command" value="Pay Now" class="btn pink-btn paid" />
                            </span>
                        </div>
End If

</form>
                    @*</text>
                    End Using*@

</div><!-- .form-wrapper ends -->
        </div><!-- .page ends -->
        <aside class="right">
            <div class="logged-in">
                <h2>Currently logged in as</h2>
            @If 1 < 2 Then
                Dim _user As String() = Membership.GetUser().UserName.Split("@")
                  @<div class="user">@_user(0)</div>
                @<div class="logout">@Html.ActionLink("Logout", "Logout", "Account")</div>
            End If
              
            </div><!-- .logged-in -->
            <div class="ticket-cloumn more-tickets">
                <h2>Add more tickets</h2>
                <div class="row one">
                
                    <span class="ticket-option">Ticket Option</span>
                    @Html.ActionLink("View Ticket Options", "Book", "Classes", New With { _
                Key .id = Session("Id") _
                }, New With { _
    Key .[class] = "" _
})
                    @*<a href="#">View Ticket Options</a>*@
                </div><!-- .row ends -->
            </div><!-- .ticket-cloumn ends -->
        </aside><!-- .right ends -->
    </div>

    </div>

    <script>
    $('.no-of-child').show('fast');
    $("#show-detail table").removeAttr("style");
    // UNCHECK ALL RADIO BUTTONS
    var inputs = document.getElementsByTagName("input");
    // ICHECK PLUGIN
    $(".radio input").iCheck({
        radioClass: 'iradio'
    });
    
    $('.paid').click(function () {
        if(@Model.BookedClassList.Count==0)
        {
            return false;
        }
        child_gender = $('.option input:radio:checked').val();
        $("#paymentoption").val(child_gender);
        var paymentmode = $("#paymentoption").val();
        var checkedValues = $('input:checkbox:checked').map(function () {
            return this.value;
        }).get();
        $("#Logo").val(checkedValues);
        var href = "";
        $('#pagebook').find('div').each(function () {
            $('#classbooking').find('div').each(function () {
                $('.clearfix').find('a').each(function () {
                    href += $(this).attr('href');
                });
            });
        });
        var lastlink = href.split("VenueId=");
        var lastid = lastlink[lastlink.length - 1].split("/");
        var count = lastid[lastid.length - 1];

        for (var s = 1; s <= count; s++) {
            var venue="VenueId="+s;
            if(href.toString().indexOf(venue)!= -1)
            {
                var find = "-" + s;
                if (checkedValues.toString().indexOf(find) == -1) {
                    alert("Please Select Children");
                    return false;
                }
            }
            else
            {
              
            }
        }
       
    });
        $('.addchildren').click(function () {
           var _gender = $('.radio input:checked').val();
            $("#childgender").val(_gender);
        });
        
    // SHOW NUMBER OF CHILD DIV
    $('.addchild').click(function () {
        var id = $(this).attr('id'); 
        $('#classchild-'+id).removeAttr("style");
        var checkedValues = $('input:checkbox:checked').map(function () {
            return this.value;
        }).get();
        $("#Logo").val(checkedValues);
    });
    $('.children-attending .add-another').click(function () {
        $(".child.one").show("fast");
        return false;
    });

    $('.cancel').click(function () {
        var id = $(this).attr('id');
        document.getElementById('classchild-' + id).style.display = "none";
        validator.resetForm();
        $(".error").removeClass("error");
    });

        // FORM VALIDATION
    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]);
    }

    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            child_first_name: "required",
            child_last_name: "required",
            day: {
                required: true,
                maxlength: 2,
                number: true
            },
            month: {
                required: true,
                maxlength: 2,
                number: true
            },
            year: {
                required: true,
                minlength: 4,
                maxlength: 4,
                number: true
            },
            gender: "required",
        },
        messages: {
            child_first_name: "*Required Field",
            child_last_name: "*Required Field",
            year: "*Incorrect Format",
            day: "*",
            month:"*",
            gender: "*Please select",
        },
        submitHandler: function (form) {
            //$("#Logo").val(checkedValues);
            
            var errors;
            if (!isValidDate(child_day + '/' + child_month + '/' + child_year)) {
                errors = {
                    year: "*Invalid Format"
                }
                validator.showErrors(errors);
            }
            else {
                form.submit();
            }
           
        }
    });
   
    </script>

