﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.ClassBookingViewModel

@Scripts.Render("~/bundles/jqueryval")
<form action="AddChild" method="post" class="form-horizontal" id="children">
    @*@Using (Html.BeginForm("AddChild", "Classes", FormMethod.Post, New With { _
    Key .enctype = "multipart/form-data" _
, .class = "form-horizontal", .role = "form"}))
        @<text>*@
            <div class="children-attending">
                <div class="child one">
                    <div class="form-row">
                        <label for="child_first_name">First Name:</label>

                        <input type="text" name="child_first_name" id="child_first_name" />
                    </div>
                    <div class="form-row">
                        <label for="child_last_name">Last Name:</label>
                        <input type="text" name="child_last_name" id="child_last_name" />
                    </div>
                    <div class="form-row dob">
                        <label for="day">Date of Birth:</label>
                        <input type="text" name="day" id="day" class="mini" />
                        <span class="seprator">/</span>
                        <input type="text" name="month" id="month" class="mini" />
                        <span class="seprator">/</span>
                        <input type="text" name="year" id="year" class="small" /><label for="format">dd/MM/yyyy</label>
                    </div>
                    <div class="form-row radio">
                        <input type="radio" id="boy" value="Boy" name="gender" checked />
                        <label for="boy">Boy</label>
                        <input type="radio" id="girl" value="Girl" name="gender" />
                        <label for="girl">Girl</label>
                        <input type="hidden" id="childgender" value="Boy" name="childgender" />
                    </div>
                    <div class="form-row add-btn">
                        @Html.Hidden("Logo", Model.Logo)
                        <input type="submit" name="Add" class="btn pink-btn button" value="Add" />
                        @*<input type="button" name="add" class="btn pink-btn button" value="Add" />*@
                        <input type="reset" name="reset" value="Cancel" class="cancel" />
                    </div>
                    <p class="validation-warning"><strong>*Validation message</strong></p>
                </div><!-- .child.one ends -->
                <!-- .child.two ends -->
            </div><!-- .children-attending ends -->
        @*</text>
    End Using*@
</form><!-- #register-from ends -->

<script>
    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]);
    }
    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            child_first_name: "required",
            child_last_name: "required",
            year: {
                required: true,
                minlength: 4,
               maxlength:4
            },
            gender: "required",
        },
        messages: {
            child_first_name: "*Required Field",
            child_last_name: "*Required Field",
            year: "*Incorrect Format",
            gender: "*Please select",
        },
        submitHandler: function (form) {
            var errors;
            if (!isValidDate(child_day + '/' + child_month + '/' + child_year)) {
                errors = {
                    year: "*Invalid Format"
                }
            }
            else {
                form.submit();
            }
            validator.showErrors(errors);
        }
    });
    $('.button').click(function () {
        child_gender = $('.radio input:radio:checked').val();
        $("#childgender").val(child_gender);
        var gender = $("#childgender").val();
        var checkedValues = $('input:checkbox:checked').map(function () {
            return this.value;
        }).get();
        $("#Logo").val(checkedValues);

        child_fname = $('#child_first_name').val();
        child_lname = $('#child_last_name').val();
        child_day = $('#day').val();
        child_month = $('#month').val();
        child_year = $('#year').val();
        child_gender = $('.radio input:checked').val();

        var errors;
        if ($.trim($('#child_first_name').val()) == '') {
            errors = {
                child_first_name: "*Required Field"
            };
            validator.showErrors(errors);

        } else if ($.trim($('#child_last_name').val()) == '') {
            errors = {
                child_last_name: "*Required Field"
            }
            validator.showErrors(errors);

        } else if ($.trim($('#year').val()) == '') {
            errors = {
                year: "*Incorrect Format"
            }
            validator.showErrors(errors);

        } else if (!$('input[name=gender]:checked').val()) {
            errors = {
                gender: "*Please Select"
            }
            validator.showErrors(errors);

        } else if (child_year.length != 4) {
            errors = {
                year: "*Invalid Format"
            }
            validator.showErrors(errors);

        } else if (!isValidDate(child_day + '/' + child_month + '/' + child_year)) {
            errors = {
                year: "*Invalid Format"
            }
            validator.showErrors(errors);

        } else {
           

        }
       
    });
   
    </script>
