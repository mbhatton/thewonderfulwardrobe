﻿
@Code
    ViewData("Title") = "Gallery"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code



@Using (Html.BeginForm("Gallery", "Gallery", FormMethod.Post, New With { _
    Key .enctype = "multipart/form-data" _
, .class = "form-horizontal", .role = "form"}))
    @<text>
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Upload Images</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">

                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <div class="row-fluid" style="float: none;">
                            <div class="span5" style="float: none;">

                                <label for="image">Image:</label>
                                <input type="file" name="file" id="file" class="span8 m-wrap" />
                               
                                <br />
                                <br />
                                <input type="submit" name="submit" class="btn" value="Save" />
                            </div>
                        </div>                        
                    </div>
                   
                </div>
                @Html.ActionLink("Back to List", "GalleryList")
            </div>
        </div>
    </text>
End Using
<script>
    
    $('.btn').click(function () {
        var selectedFile = $("#file").val();
        if (selectedFile == "") {
            alert("Please Select Image File.");
            return false;
        }
        else {
        }
    });
</script>
