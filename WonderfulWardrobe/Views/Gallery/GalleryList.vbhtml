﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.GalleryViewModel
@Code
    ViewData("Title") = "GalleryList"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code
<script type="text/javascript">

    function LoadDiv(url) {
        $("#PopImage").attr('src', url);
    }
 
</script>
@Using Html.BeginForm("GalleryList")
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Gallery</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        @Html.ActionLink("Upload New", "Gallery")
                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    Image
                                </th>
                                <th>
                                    Upload Date
                                </th>
                               
                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As GalleryViewModel In Model.GalleryList
                                    Dim day = DateTime.Parse(items.CreatedOn).DayOfWeek.ToString().Substring(0, 3)
                                @:<tr>
 @:<td>
 @<a data-toggle="modal" href="#responsive">
    <img src="@Url.Content(items.Location)" id="img" height="100" width="100" onclick="return LoadDiv(this.src);" style="padding-top:5px" alt="" />
</a>
                                        @:</td>
                                    @:<td>
                                        @day@:&nbsp;&nbsp; @items.CreatedOn.ToString("MMM dd yyy")
                                        @:</td>
                                   
                                   
                                   
                                    @:<td>
                                        @Html.ActionLink("Delete", "Delete", "Gallery", New With { _
                                            Key .id = items.Id _
                                            }, Nothing)
                                        @:</td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
    End Using

<div class="portlet-body form">
    <div id="responsive" class="modal hide fade" tabindex="-1" data-width="560" style="text-align:center; top:50%;">
        <img id="PopImage" alt="" src="" />
        <div class="modal-footer" style="vertical-align:bottom;">
            <button type="button" data-dismiss="modal" class="btn">
                Close
            </button>
        </div>
    </div>
</div>