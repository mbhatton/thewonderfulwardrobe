﻿@*@Code
    ViewData("Title") = "Index"
End Code

<h2>Index</h2>*@

@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.GalleryViewModel

<div class="gallery-page">
    <div class="body clearfix">
      

        <a href="#" class="facebook-like">Facebook Like</a>
        <aside class="left">
            <div class="urgant-notice">
                <h2>Urgent Notice Title</h2>
                <p>We are pleased to announce the return of our summer sessions! These MUST be pre-booked on the classes page and remember the ticket covers all of your children up to the age of 6.</p>
                <p>
            </div><!-- .urgant-notice ends -->
            <div class="image">
                <a href="#">
                    <img src="~/Content/images/1.jpg" width="193" height="205" alt="We do Great parties" />
                    <span class="caption">we do great parties >></span>
                </a>
            </div><!-- .image ends -->
            <div class="image rotate">
                <a href="#">
                    <img src="~/Content/images/2.jpg" width="193" height="205" alt="We do Great parties" />
                </a>
            </div><!-- .image ends -->
        </aside><!-- .left ends -->
        <div class="page">
            <div class="gallery">
                <h1>Our Gallery</h1>
                <p class="note">Click any thumb image below to view larger</p>
                <div class="gallery-slider">
                    <div id="gallery-2" class="royalSlider rsDefault">
@For Each items As GalleryViewModel In Model.GalleryList
    Dim url = items.Location.Replace("~/", "")
                        @<a class="rsImg" href="@url" data-rsbigimg="@url"><img class="rsTmb" src="@url" width="80" height="90" alt="Gallery-1" /></a>
    '<a class="rsImg" href="~/Content/gallery/2.jpg" data-rsbigimg="~/Content/gallery/2.jpg"><img class="rsTmb" src="~/Content/gallery/thumb/thumb-1.jpg" width="90" height="90" alt="Gallery-1" /></a>
    '<a class="rsImg" href="~/Content/gallery/1.jpg" data-rsbigimg="~/Content/gallery/1.jpg"><img class="rsTmb" src="~/Content/gallery/thumb/thumb-1.jpg" width="90" height="90" alt="Gallery-1" /></a>
    '<a class="rsImg" href="~/Content/gallery/1.jpg" data-rsbigimg="~/Content/gallery/1.jpg"><img class="rsTmb" src="~/Content/gallery/thumb/thumb-1.jpg" width="90" height="90" alt="Gallery-1" /></a>
    '<a class="rsImg" href="~/Content/gallery/2.jpg" data-rsbigimg="~/Content/gallery/2.jpg"><img class="rsTmb" src="~/Content/gallery/thumb/thumb-1.jpg" width="90" height="90" alt="Gallery-1" /></a>
    '<a class="rsImg" href="~/Content/gallery/1.jpg" data-rsbigimg="~/Content/gallery/1.jpg"><img class="rsTmb" src="~/Content/gallery/thumb/thumb-1.jpg" width="90" height="90" alt="Gallery-1" /></a>
    '<a class="rsImg" href="~/Content/gallery/1.jpg" data-rsbigimg="~/Content/gallery/1.jpg"><img class="rsTmb" src="~/Content/gallery/thumb/thumb-1.jpg" width="90" height="90" alt="Gallery-1" /></a>
    '<a class="rsImg" href="~/Content/gallery/2.jpg" data-rsbigimg="~/Content/gallery/2.jpg"><img class="rsTmb" src="~/Content/gallery/thumb/thumb-1.jpg" width="90" height="90" alt="Gallery-1" /></a>
    '<a class="rsImg" href="~/Content/gallery/1.jpg" data-rsbigimg="~/Content/gallery/1.jpg"><img class="rsTmb" src="~/Content/gallery/thumb/thumb-1.jpg" width="90" height="90" alt="Gallery-1" /></a>
    '<a class="rsImg" href="~/Content/gallery/2.jpg" data-rsbigimg="~/Content/gallery/2.jpg"><img class="rsTmb" src="~/Content/gallery/thumb/thumb-1.jpg" width="90" height="90" alt="Gallery-1" /></a>
Next
                     </div><!-- #gallery2 ends -->
                </div><!-- .gallery-slider ends -->
            </div><!-- .gallery ends -->
        </div><!-- .page ends -->
    </div>
</div>
    <script>
        // ROYAL SLIDER
        $('#gallery-2').royalSlider({
controlNavigation:  'thumbnails',
            autoScaleSlider: true,
            autoScaleSliderWidth: 960,
            autoScaleSliderHeight: 850,
            loop: false,
            numImagesToPreload: 4,
            arrowsNavAutohide: true,
            arrowsNavHideOnTouch: true,
            keyboardNavEnabled: true
        });

        // ROYAL SLIDER THUMBS ARROW POSITION (.gallery)
        $('.gallery .royalSlider .rsThumbsArrow')
            .css({
                position: 'absolute',
top:  'auto',
                height: 30,
                bottom: 72,
                left: 12
            }).appendTo('.gallery .royalSlider').filter('.gallery .rsThumbsArrowRight')
            .css({
                left: 'auto',
                right: 12
            });
    </script>