﻿@Code
    ViewData("Title") = "ErrorMessage"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="classes-page">

    <div class="body clearfix">
        <div class="page" id="pagebook">
            <h2>Something went wrong.</h2>
            <div class="form-wrapper booking">


                @If ViewBag.ErrorMessage = Nothing Then
                Else
                    @<h3>Error message</h3>
                    @<p>@ViewBag.ErrorMessage</p>
                End If

                @If ViewBag.JsonRequest = Nothing Then
                Else
                    @<h3>JSON Request</h3>
                    @<pre>@ViewBag.JsonRequest</pre>
                End If

                @If ViewBag.JsonResponse = Nothing Then
                Else
                    @<h3>JSON Response</h3>
                    @<pre>@ViewBag.JsonResponse</pre>
                End If

            </div>
        </div>
    </div>
</div>

