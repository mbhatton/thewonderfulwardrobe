﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.ConfirmedViewData
@Code
    ViewData("Title") = "Confirmed"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="classes-page">

    <div class="body clearfix">
        <div class="page" id="pagebook">
            <h2>Please Wait ...</h2>
            <div class="form-wrapper booking">
                <form action="/paypal/capture" method="post">
                    <input type="hidden" name="authorizationId" value="@Model.AuthorizationId" />
                    @*<button type="submit" class="btn pink-btn button">Confirm</button>*@
                </form>
            </div>
        </div>
    </div>
</div>
@Scripts.Render("~/bundles/jquery")

<script type="text/javascript" language="javascript">
    $(this.document).ready(function () {
        var form = $("form");
        form.submit();
    });
</script>