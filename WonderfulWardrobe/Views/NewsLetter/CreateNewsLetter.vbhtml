﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.NewsLetterViewModel
@Code
    ViewData("Title") = "CreateNewsLetter"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code
<script src="~/Scripts/jquery-2.0.2.min.js"></script>
<script src="~/Scripts/tinymce/tinymce.min.js"></script>
<script src="~/Scripts/tinymce/tiny_mce.js"></script>
<script src="~/Scripts/tinymce/themes/advanced/editor_template.js"></script>
<script src="~/Scripts/tinymce/plugins/lists/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/autolink/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/layer/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/spellchecker/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/style/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/table/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/pagebreak/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/advhr/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/advimage/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/advlink/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/autoresize/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/autosave/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/bbcode/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/contextmenu/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/directionality/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/emotions/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/example/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/example_dependency/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/fullpage/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/fullscreen/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/iespell/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/inlinepopups/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/insertdatetime/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/legacyoutput/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/media/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/searchreplace/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/print/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/paste/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/noneditable/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/visualchars/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/xhtmlxtras/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/nonbreaking/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/template/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/save/editor_plugin.js"></script>
<script src="~/Scripts/tinymce/plugins/preview/editor_plugin.js"></script>
@Scripts.Render("~/bundles/jqueryval")
@*<script type="text/javascript" src="<your installation path>/tinymce/tinymce.min.js"></script>*@
<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode: "textareas",
        theme: "advanced",
        plugins: "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,

        // Skin options
        skin: "o2k7",
        skin_variant: "silver",

        // Example content CSS (should be your site CSS)
       // content_css: "~/Content/assets/css/style.css",
        // Drop lists for link/image/media/template dialogs

        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",
        //template_external_list_url: "js/template_list.js",
        //external_link_list_url: "js/link_list.js",
        //external_image_list_url: "js/image_list.js",
        //media_external_list_url: "js/media_list.js",

        // Replace values for the template plugin
        template_replace_values: {
            username: "Some User",
            staffid: "991234"
        }
    });
</script>

<form method="post" name="newslettercontent" id="newslettercontent" action="SaveNewsLetter">
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-cogs"></i>Create News Letter</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body no-more-tables">

                    <div class="messages">
                        <div class="messages error">
                            <p style="color:red;">@ViewBag.Message</p>
                        </div>
                    </div>

                    <div class="row-fluid" style="float: none;">
                        <div class="span5" style="float: none;">

                            <label for="title">Title:</label>
                            @Html.TextBoxFor(Function(m) m.Title, New With { _
            Key .[class] = "span12 m-wrap" _
            })

                            <label for="content">NewsLetter:</label>
                            @Html.TextAreaFor(Function(m) m.content, New With { _
                                    Key .[style] = "width:auto;height:auto" _
                                    })
                           <input type="hidden" id="valid" name="valid" value="" />

                            <br />
                            <br />
                            <input type="submit" name="submit" class="btn formsubmit" value="Save" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
<script>
    var validator = jQuery("#newslettercontent").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            Title: "required",
            content: "required",
            
        },
        messages: {
            Title: "*Required Field",
            content: "*Required Field",
            
        },
        submitHandler: function (form) {
            var editorcontent = tinyMCE.activeEditor.getContent();
            if (editorcontent == "") {
                errors = {
                    valid: "*Required Field"
                }
                validator.showErrors(errors);
                return false;
            }
            else
            {
                form.submit();
            }
           


        }
    });
   
</script>
    
