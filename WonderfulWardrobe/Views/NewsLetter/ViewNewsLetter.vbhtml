﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.NewsLetterViewModel
@Code
    ViewData("Title") = "ViewNewsLetter"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code



<div class="row-fluid">
    <div class="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i>News Letter</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body no-more-tables">
                <div class="messages">
                    <div class="messages error">
                        
                    </div>
                </div>
                <table class="table-bordered table-striped table-condensed t-cus">
                    <tr>
                        <th>
                            @Model.Title
                        </th>
                    </tr>
                        <tr>
                            <td>
                                @Html.HiddenFor(Function(m) m.content)
                                <div id="contentdata"></div>                              
                            </td>
                        </tr>
                </table>
               
            </div>
        </div>
        <div style="padding-bottom:10px;">
            @Html.ActionLink("Back to List", "NewsLettersList")
        </div>
    </div>
</div>
<script>
    var data = $("#content").val();
    $("#contentdata").html(data);
</script>