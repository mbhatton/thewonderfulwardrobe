﻿

@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.NewsLetterViewModel
@Code
    ViewData("Title") = "NewsLettersList"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code


<div class="row-fluid">
    <div class="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i>News Letter List</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body no-more-tables">
                <div class="messages">
                    <div class="messages error">
                        <p style="color:red;">@ViewBag.Message</p>
                    </div>
                </div>

                <table class="table-bordered table-striped table-condensed t-cus">
                    <tr>
                        <th>
                            Id
                        </th>
                        <th>
                            Title
                        </th>
                        <th>
                            Created On
                        </th>
                        <th>
                            Is Published
                        </th>
                        <th>
                            Action
                        </th>
                       
                    </tr>

                    @For Each items As NewsLetterViewModel In Model.NewsLetterList
                        @:<tr>
                            @:<td>
                                @items.Id
                                @:</td>

                            @:<td>
                                @items.Title
                                @:</td>
                            @:<td>
                                @items.CreatedOn.ToString("MMM dd yyy")
                                @:</td>
 @:<td>
                                @items.IsPublish
                                @:</td>
 @:<td>

                         @Html.ActionLink("View", "ViewNewsLetter", "NewsLetter", New With { _
                                            Key .id = items.Id _
                                            }, Nothing)@:&nbsp;&nbsp;

                               @Html.ActionLink("Edit", "EditNewsLetter", "NewsLetter", New With { _
                                            Key .id = items.Id _
                                            }, Nothing)@:&nbsp;&nbsp;
                    
                                       @Html.ActionLink("Send", "SendNewsLetter", "NewsLetter", New With { _
                                            Key .id = items.Id _
                                            }, Nothing)@:&nbsp;&nbsp;

                                        @Html.ActionLink("Delete", "DeleteNewsLetter", "NewsLetter", New With { _
                                            Key .id = items.Id _
                                            }, Nothing)
                                        @:</td>

                            @:</tr>
                            Next
                </table>
            </div>
        </div>
    </div>
</div>
<script>

</script>