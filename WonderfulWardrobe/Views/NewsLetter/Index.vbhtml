﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.NewsLetterViewModel
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("NewsLetterUserList")
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>NewsLetter Subscribers</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>
                                    EmailId
                                </th>

                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As NewsLetterViewModel In Model.NewsLetterList
                                @:<tr>
                                    @:<td>
                                       @items.Name
                                        @:</td>
                                    @:<td>
                                         @items.EmailId
                                        @:</td>
                                        
                                    @:<td>
                                        @Html.ActionLink("Delete", "Delete", "NewsLetter", New With { _
                                            Key .id = items.Id _
                                            }, Nothing)
                                        @:</td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using

