﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title - Wondeful Wardrobe</title>
    @Styles.Render("~/Content/css")
    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/Jquerymin")
    @Scripts.Render("~/bundles/jqueryroyalslider")
    @Scripts.Render("~/bundles/icheck")
    @Scripts.Render("~/bundles/jqueryjscrollpane")
    @Scripts.Render("~/bundles/mousewheel")
    @Scripts.Render("~/bundles/validate")
    @Scripts.Render("~/bundles/common")
    <link rel="shortcut icon" href="~/Content/images/favicon.ico" />
</head>
<body>
    <div class="wrapper">
        <header>
            <div class="logo">
                <a href="/" title="The Wonderful Wardrobe">
                    <img src="~/Content/images/logo.png" alt="The Wonderful Wardrobe" />
                </a>
            </div><!-- .logo ends -->
        </header><!-- header ends -->
        <div class="banner clearfix">
            <div class="slider-wrapper">
                <div id="gallery-1" class="royalSlider rsDefault">
                    <a class="rsImg" href="~/Content/banner/1.jpg" data-rsbigimg="~/Content/banner/1.jpg"><img class="rsTmb" src="~/Content/banner/thumb/thumb-1.png" alt="classes" /></a>
                    <a class="rsImg" href="~/Content/banner/2.jpg" data-rsbigimg="~/Content/banner/2.jpg" title="Classes"><img class="rsTmb" src="~/Content/banner/thumb/thumb-2.png" alt="parties" /></a>
                    <a class="rsImg" href="~/Content/banner/3.jpg" data-rsbigimg="~/Content/banner/3.jpg" title="Groups"><img class="rsTmb" src="~/Content/banner/thumb/thumb-3.png" alt="groups" /></a>
                </div><!-- #gallery ends -->
            </div><!-- .slider-wrapper -->
            <div class="newsletter">
                <h2>Newsletter Signup</h2>
                <p>Keep up-to-date with the latest news from Wonderful Wardrobe</p>
                <form action="~/NewsLetter/NewsLetter" method="post" id="newsletter-signup-form">
                    <input type="text" name="name" placeholder="Name*" />
                    <input type="email" name="email" placeholder="Email*" />
                    <input type="submit" class="btn blue-btn" value="Sign me up" />
                </form>
                <p>*Required</p>
                <div class="character">
                    <img src="~/Content/images/character.png" width="120" height="115" alt="Character" />
                </div><!-- .character ends -->
            </div><!-- .newletter ends -->
        </div><!-- .banner ends -->
        <nav>
            <a href="#menu" id="menu-icon" class="blog">
                <span>&nbsp;</span>
                <span>&nbsp;</span>
                <span>&nbsp;</span>
            </a>
            <ul class="clearfix" id="menu">
                <li class="active"><a class="home" @Html.ActionLink("Home", "Index", "Home")</a></li>
                <li><a class="gallery" @Html.ActionLink("Gallery", "Index", "Gallery")</a></li>
                <li><a class="classes" @Html.ActionLink("Classes", "Index", "Classes")</a></li>
                <li><a class="parties" @Html.ActionLink("Parties", "Index", "Parties")</a></li>
                <li><a class="groups" @Html.ActionLink("Groups", "Index", "Group")</a></li>
                <li><a class="blog" @Html.ActionLink("Blogs", "Index", "Blog")</a></li>
                <li><a class="contact" @Html.ActionLink("Contact", "Index", "Contact")</a></li>
               
            </ul>
        </nav><!-- nav ends -->
   
        <div >
            @RenderBody()
            @*<hr />*@
            <footer>
                <span class="copyrights">Copyright 2013 - <a href="#" title="The Wonderful Wardrobe">Wonderful Wardrobe</a></span>
                <p> <a class="home" @Html.ActionLink("Home", "Index", "Home")</a> |  <a class="classes" @Html.ActionLink("Classes", "Index", "Classes")</a> | <a class="parties" @Html.ActionLink("Parties", "Index", "Parties")</a> | <a class="groups" @Html.ActionLink("Groups", "Index", "Group")</a> | <a class="contact" @Html.ActionLink("Contact", "Index", "Contact")</a> | <a href="#" title="Sitemap">Sitmap</a></p>
            </footer>
        </div>
    </div>
   
    
    @RenderSection("scripts", required:=False)
    <script type="text/javascript">
        $('.scroll-pane').jScrollPane();
    </script>


</body>
</html>
