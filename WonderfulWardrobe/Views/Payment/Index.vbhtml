﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.PaymentViewModel
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code


        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Payment</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    OrderId
                                </th>
                                <th>
                                    User Name
                                </th>
                                <th>
                                    Payment Type
                                </th>
                                <th>
                                    Total Amount
                                </th>
                                <th>
                                        Paid Amount
                                    </th>
                                    <th>
                                        Payment For
                                    </th>
                                <th>
                                   Payment Date
                                </th>
                            </tr>

                            @For Each items As PaymentViewModel In Model.PaymentList
                                @:<tr>
 @:<td>
                                        @items.OrderId
                                        @:</td>

                                @:<td>
                                        @items.UserName
                                        @:</td>
 @:<td>
                                        @items.PaymentType
                                        @:</td>
                                    @:<td>
                                        @<span> &#163;</span>@items.TotalAmount.ToString("N2")
                                        @:</td>
 @:<td>
                                        @<span> &#163;</span>@items.PayAmount.ToString("N2")
                                        @:</td>
                                    @:<td>
                                        @items.PaymentFor.ToString()
                                        @:</td>
 @:<td>
                                        @items.CreatedOn.ToString("MMM dd yyy")
                                        @:</td>
                                   
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
   




