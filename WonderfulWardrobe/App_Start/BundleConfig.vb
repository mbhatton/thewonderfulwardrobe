﻿Imports System.Web.Optimization

Public Module BundleConfig
    ' For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    Public Sub RegisterBundles(ByVal bundles As BundleCollection)

        bundles.Add(New ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-{version}.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate*"))

        bundles.Add(New ScriptBundle("~/bundles/Jquerymin").Include(
                 "~/Scripts/jquery-1.8.3.min.js"))
        bundles.Add(New StyleBundle("~/Content/css").Include(
                  "~/Content/style.css", "~/Content/royalslider.css"))
        bundles.Add(New ScriptBundle("~/bundles/jqueryroyalslider").Include(
                  "~/Scripts/jquery.royalslider.min.js"))
        bundles.Add(New ScriptBundle("~/bundles/icheck").Include(
             "~/Scripts/icheck.js"))
        bundles.Add(New ScriptBundle("~/bundles/jqueryjscrollpane").Include(
                 "~/Scripts/jquery.jscrollpane.min.js"))
        bundles.Add(New ScriptBundle("~/bundles/mousewheel").Include(
                 "~/Scripts/jquery.mousewheel.js"))
        bundles.Add(New ScriptBundle("~/bundles/validate").Include(
                 "~/Scripts/jquery.validate.min.js"))
        bundles.Add(New ScriptBundle("~/bundles/common").Include(
                 "~/Scripts/common.js"))
        bundles.Add(New ScriptBundle("~/bundles/sticky").Include(
                "~/Scripts/jquery.sticky.js"))
        bundles.Add(New StyleBundle("~/Content/bootstrapmincss").Include(
                 "~/Content/assets/plugins/bootstrap/css/bootstrap.min.css"))
        bundles.Add(New StyleBundle("~/Content/bootstrapresponsivemincss").Include(
                 "~/Content/assets/plugins/bootstrap/css/bootstrap-responsive.min.css"))
        bundles.Add(New StyleBundle("~/Content/fontawesomemin").Include(
                 "~/Content/assets/plugins/font-awesome/css/font-awesome.min.css",
                 "~/Content/assets/css/style-metro.css"))
        bundles.Add(New StyleBundle("~/Content/styleresponsive").Include(
                 "~/Content/assets/css/style.css",
                 "~/Content/assets/css/style-responsive.css"))
        bundles.Add(New StyleBundle("~/Content/bootstrapmodal").Include(
                 "~/Content/assets/css/themes/default.css", "~/Content/assets/plugins/bootstrap-modal/css/bootstrap-modal.css"))

        bundles.Add(New ScriptBundle("~/bundles/jquery-1.10.1").Include(
               "~/Content/assets/plugins/jquery-1.10.1.min.js"))
        bundles.Add(New ScriptBundle("~/bundles/bootstrap.min").Include(
               "~/Content/assets/plugins/bootstrap/js/bootstrap.min.js"))
        bundles.Add(New ScriptBundle("~/bundles/bootstrap-modal").Include(
               "~/Content/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"))
        bundles.Add(New ScriptBundle("~/bundles/bootstrap-modalmanager").Include(
               "~/Content/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"))
        bundles.Add(New ScriptBundle("~/bundles/app").Include(
              "~/Content/assets/scripts/app.js"))
        bundles.Add(New ScriptBundle("~/bundles/ui-modals").Include(
              "~/Content/assets/scripts/ui-modals.js"))
        bundles.Add(New ScriptBundle("~/bundles/jquery-ui-1.10.1.custom.min").Include(
             "~/Content/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"))

    End Sub
End Module

