$(window).load(function() {
	if (window.PIE) {
				$('.banner, .banner .newsletter, .btn, nav li a, .body, aside.left .urgant-notice, aside.left .image, .facebook-box, .jspDrag, .twitter > div, .twitter ul, aside.right .testimonal p, .image-left, .venues li span, .venues li a span, .ticket-detail > div, .term-ticket, #map, #google-map, .ticket-cloumn .row, .logged-in, .ante-meridian > div, .post-meridian > div, .anytime, .parties-prices, .form-wrapper form > div, .blog .post, .contact-page .form-wrapper, .login-page .form-wrapper, .thankyou').each(function() {
				PIE.attach(this);
			});
		}
});

$(document).ready(function() {

	$('#menu-icon').click(function(){
		$ul = $(this).attr('href');
		if($($ul).is(':visible')){
			$($ul).slideUp();
		}
		else {
			$($ul).slideDown();
		}
		return false;
	});
//ROYAL SDLIDER
	$('#gallery-1').royalSlider({
	   controlNavigation: 'thumbnails',
		thumbs: {
		  orientation: 'vertical',
		  appendSpan: true,
		  arrows: false
		},
		transitionType:'fade',
		autoScaleSlider: true, 
		arrowsNav: false,
		keyboardNavEnabled: true,
		updateSliderSize: true,
		autoHeight: true,
		autoScaleSlider:false,
		imageScaleMode:'none',
		imageAlignCenter: false
  	});
  
//GETTING ALT ATTRIBUTE TEXT OF IMAGE AND ASSIGNING TEXT TO SPAN
	$( ".royalSlider img" ).each(function(){
		var title = $(this).attr( "alt" );
		$(this).prev('span').html(title);
	});
// ADDING CLASS TO rsThumbsContainer div 
	$('.rsThumbsContainer div').each(function(i){
		$(this).addClass('slide-' + (i+1) );
	});

 	$('table tbody .close-btn').live('click', function(e){
		e.preventDefault();
		var tr = $(this).closest('tr');
		tr.fadeOut(400, function(){
				tr.remove();
		});
	});
// SIGNUP FORM VALIDATION
		jQuery("#newsletter-signup-form").validate({
			rules: {
				name: "required",
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				name: "",
				email: "",
			},
		 	submitHandler: function(form) {
				form.submit();
			}
		});
		
		
	//Open links in new window
	$("a[rel=external]").each(function(){
		this.target = "_blank";
	});

	//default validate action
	$('form').attr('novalidate',true);

	//printing Icons
	$("a.print").click(function(){
		window.print();
		return false;
	});
});