﻿@*@Code
    ViewData("Title") = "Booking"
End Code

<h2>Booking</h2>*@
@Scripts.Render("~/bundles/icheck")
<div class="classes-page">
    <div class="body clearfix">
        <a href="#" class="facebook-like">Facebook Like</a>
        <div class="page">
            <h1>Your <span>Classes booking</span></h1>
            <p class="note">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore<br /> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br /> consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            <div class="form-wrapper booking">
                <form action="#" method="post" id="children-attending">
                    <div class="clearfix">
                        <span class="seasion-ticket">
                            Seasion TIckets
                        </span>
                        <h5><strong>Boulders</strong> - St Catherines Park, Pengam Rd, Cardiff, CF24 2RZ</h5>
                        <div id="add-show">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="date">Date</th>
                                        <th class="time">Time</th>
                                        <th class="duration">Duration</th>
                                        <th class="price">Price</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Wed 02 Oct 2013</td>
                                        <td>10:00</td>
                                        <td>60 minutes</td>
                                        <td>5.00</td>
                                        <td class="close"><span><a href="#" class="close-btn">Close</a></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- #add-show ends -->
                        <div id="show-detail">
                            <a href="#" class="add-another">Add another+</a>
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="date">Wed 02 Oct 2013</td>
                                        <td class="time">10:00</td>
                                        <td class="duration">60 minutes</td>
                                        <td class="price">5.00</td>
                                        <td class="add-btn"><a href="#" class="btn pink-btn">Add</a></td>
                                    </tr>
                                    <tr>
                                        <td class="date">Wed 02 Oct 2013</td>
                                        <td class="time">10:00</td>
                                        <td class="duration">60 minutes</td>
                                        <td class="price">5.00</td>
                                        <td class="add-btn"><a href="#" class="btn pink-btn">Add</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- #show-detail ends -->
                        <div class="children-attending">
                            <div class="clearfix">
                                <h4>Children attending<span>Please select</span></h4>
                                <a href="#" class="add-another">Add another child+</a>
                            </div>
                            <div class="form-row radio no-of-child">
                                <div class="clearfix">
                                    <input type="radio" id="child_name1" value="child_name1" name="child_name" />
                                    <label for="child_name1">Child Name 1</label>
                                </div>
                                <div class="clearfix">
                                    <input type="radio" id="child_name2" value="child_name2" name="child_name" />
                                    <label for="child_name2">Child Name 2</label>
                                </div>
                            </div>
                            <div class="child one">
                                <div class="form-row">
                                    <label for="child_first_name">First Name:</label>
                                    <input type="text" name="child_first_name" id="child_first_name" />
                                </div>
                                <div class="form-row">
                                    <label for="child_last_name">Last Name:</label>
                                    <input type="text" name="child_last_name" id="child_last_name" />
                                </div>
                                <div class="form-row dob">
                                    <label for="day">Date of Birth:</label>
                                    <input type="text" name="day" id="day" class="mini" />
                                    <span class="seprator">/</span>
                                    <input type="text" name="month" id="month" class="mini" />
                                    <span class="seprator">/</span>
                                    <input type="text" name="year" id="year" class="small" />
                                </div>
                                <div class="form-row radio">
                                    <input type="radio" id="boy" value="Boy" name="gender" />
                                    <label for="boy">Boy</label>
                                    <input type="radio" id="girl" value="Girl1" name="gender" />
                                    <label for="girl">Girl</label>
                                </div>
                                <div class="form-row add-btn">
                                    <input type="button" name="add" class="btn pink-btn button" value="Add" />
                                    <input type="reset" name="reset" value="Cancel" class="cancel" />
                                </div>
                            </div><!-- .child.one ends -->
                            <div class="child two">
                                <div class="form-row">
                                    <label for="child_first_name2">First Name:</label>
                                    <input type="text" name="child_first_name2" id="child_first_name2" />
                                </div>
                                <div class="form-row">
                                    <label for="child_last_name2">Last Name:</label>
                                    <input type="text" name="child_last_name2" id="child_last_name2" />
                                </div>
                                <div class="form-row dob">
                                    <label for="day2">Date of Birth:</label>
                                    <input type="text" name="day2" id="day2" class="mini" />
                                    <span class="seprator">/</span>
                                    <input type="text" name="month2" id="month2" class="mini" />
                                    <span class="seprator">/</span>
                                    <input type="text" name="year2" id="year2" class="small" />
                                </div>
                                <div class="form-row radio">
                                    <input type="radio" id="boy2" value="Boy2" name="gender2" />
                                    <label for="boy2">Boy</label>
                                    <input type="radio" id="girl2" value="Girl2" name="gender2" />
                                    <label for="girl2">Girl</label>
                                </div>
                                <div class="form-row add-btn">
                                    <input type="button" name="add" class="btn pink-btn button" value="Add" />
                                    <input type="reset" name="reset" value="Cancel" class="cancel" />
                                </div>
                            </div><!-- .child.two ends -->
                        </div><!-- .children-attending ends -->
                        <div class="total-amount clearfix">
                            <span class="amount">Total: &#163;5:00</span>
                            <span class="pay-now"><input type="submit" name="paynow" value="Pay Now" class="btn pink-btn" /></span>
                        </div><!-- .total-amount ends -->
                        <p class="validation-warning"><strong>*Validation message</strong></p>
                    </div><!-- DIV ends -->
                </form><!-- #register-from ends -->
            </div><!-- .form-wrapper ends -->
        </div><!-- .page ends -->
        <aside class="right">
            <div class="logged-in">
                <h2>Currently logged in as</h2>
                <div class="user">Matt Hatton</div>
                <div class="logout">@Html.ActionLink("Logout", "Logout", "Account")</div>
            </div><!-- .logged-in -->
            <div class="ticket-cloumn more-tickets">
                <h2>Add more tickets</h2>
                <div class="row one">
                    <span class="ticket-option">Ticket Option</span>
                    <a href="#">View Ticket Options</a>
                </div><!-- .row ends -->
            </div><!-- .ticket-cloumn ends -->
        </aside><!-- .right ends -->
    </div>
    </div>

    <script>
        // UNCHECK ALL RADIO BUTTONS
        var inputs = document.getElementsByTagName("input");
        for (var i = inputs.length - 1; i >= 0; i--) {
            if (inputs[i].getAttribute("type") === "radio") {
                inputs[i].checked = false;
            }
        }
        // ICHECK PLUGIN
        $(".radio input").iCheck({
            radioClass: 'iradio'
        });
        // SHOW TABLE OF SHOW DETAIL DIV
        $('#show-detail .add-another').click(function () {
            $('#show-detail table').show('fast');
            return false;
        });

        // SHOW NUMBER OF CHILD DIV
        $('.children-attending .add-another').click(function () {
            $('.no-of-child').show('fast');
            return false;
        });

        // CLICK ON CHILD NAME RADIO BUTTON ONE
        $('.no-of-child input').on('ifChecked', function (event) {
            if ($(this).val() == "child_name1") {
                $(".child.one").show("fast");
                $(".child.two").hide("fast");
            } else {
                $(".child.two").show("fast");
                $(".child.one").hide("fast");
            }
        });

        // CLICK ON CHILD NAME RADIO BUTTON TWO
        $('.no-of-child input').on('ifChecked', function (event) {
            if ($(this).val() == "child_name2") {
                $(".child.one").hide("fast");
                $(".child.two").show("fast");
            } else {
                $(".child.two").hide("fast");
                $(".child.one").show("fast");
            }
        });

        // FORM VALIDATION
        jQuery("#children-attending").validate({
            errorContainer: $(".validation-warning"),
            rules: {
                child_first_name: "required",
                child_last_name: "required",
                year: "required",
                gender: "required",
                child_first_name2: "required",
                child_last_name2: "required",
                year2: "required",
                gender2: "required"
            },
            messages: {
                child_first_name: "*Required Field",
                child_last_name: "*Required Field",
                year: "*Incorrect Format",
                gender: "*Please select",
                child_first_name2: "*Required Field",
                child_last_name2: "*Required Field",
                year2: "*Incorrect Format",
                gender2: "*Please select"
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    </script>

