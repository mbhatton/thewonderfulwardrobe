﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.ClassViewModel
@Code
    ViewData("Title") = "DetailClass"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

<div class="row-fluid">
    <div class="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i>Class Details</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body no-more-tables">
                <div class="row-fluid" style="float: none;">
                    <div class="span5" style="float: none;">

                        <div class="display-label" style="margin-bottom: 2px;">Term:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Term</div>

                        <div class="display-label" style="margin-bottom: 2px;">Venue:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Venue</div>

                        <div class="display-label" style="margin-bottom: 2px;">StartDateTime:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.StartDateTime</div>

                        <div class="display-label" style="margin-bottom: 2px;">Duration(Minutes):</div>
                        <div class="display-field" style="margin-bottom: 10px;">@Model.Duration</div>

                        <div class="display-label" style="margin-bottom: 2px;">Price:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Price.ToString("N2")</div>

                        <div class="display-label" style="margin-bottom: 2px;">Capacity:</div>
                        <div class="display-field" style="margin-bottom: 10px;">@Model.Capacity</div>


                        @Html.ActionLink("Edit", "EditClass", "AdminClass", New With { _
                                            Key .id = Model.ClassId _
                                            }, Nothing)
                        &nbsp;&nbsp;
                        @Html.ActionLink("Back to List", "ClassesList")

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
