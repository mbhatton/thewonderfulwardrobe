﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.TermsViewModel
@Code
    ViewData("Title") = "TermsList"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("CreateTerms")
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Terms</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        @Html.ActionLink("Create New", "CreateTerms")
                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Discount Amount
                                </th>
                                @*<th>
                                    Start Date
                                </th>
                                <th>
                                    End Date
                                </th>*@
                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As TermsViewModel In Model.TermsList
                                @:<tr>
                                @:<td>
                                @items.Name
                                @:</td>
                                @:<td>
                                @items.DiscountAmount.ToString("N2")
                                @:</td>
                                @*@:<td>
                                @items.StartDate.ToString("MMM dd yyy")
                                @:</td>
                                @:<td>
                                @items.EndDate.ToString("MMM dd yyy")
                                @:</td>*@
                                @:<td>
                                @Html.ActionLink("Edit", "EditTerm", "AdminClass", New With { _
                                            Key .id = items.TermId _
                                            }, Nothing) @:&nbsp;&nbsp;
                                @Html.ActionLink("Details", "DetailTerm", "AdminClass", New With { _
                                            Key .id = items.TermId _
                                            }, Nothing) @:&nbsp;&nbsp;
                                @Html.ActionLink("Delete", "DeleteTerm", "AdminClass", New With { _
                                            Key .id = items.TermId _
                                            }, Nothing)
                                @:</td>
                                @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using

