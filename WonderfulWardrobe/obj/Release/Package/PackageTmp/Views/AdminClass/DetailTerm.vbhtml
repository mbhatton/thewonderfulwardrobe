﻿
@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.TermsViewModel
@Code
    ViewData("Title") = "DetailTerm"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

<div class="row-fluid">
    <div class="span12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i>Term Details</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body no-more-tables">
                <div class="row-fluid" style="float: none;">
                    <div class="span5" style="float: none;">

                        <div class="display-label" style="margin-bottom: 2px;">Name:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.Name</div>

                        <div class="display-label" style="margin-bottom: 2px;">Discount Amount:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.DiscountAmount.ToString("N2")</div>

                        @*<div class="display-label" style="margin-bottom: 2px;">Start Date:</div>
                        <div class="display-field" style="margin-bottom: 5px;">@Model.StartDate.ToString("MMM dd yyy")</div>

                        <div class="display-label" style="margin-bottom: 2px;">End Date:</div>
                        <div class="display-field" style="margin-bottom: 10px;">@Model.EndDate.ToString("MMM dd yyy")</div>*@

                        @Html.ActionLink("Edit", "EditTerm", "AdminClass", New With { _
                                            Key .id = Model.TermId _
                                            }, Nothing)
                        &nbsp;&nbsp;
                        @Html.ActionLink("Back to List", "TermsList")
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

