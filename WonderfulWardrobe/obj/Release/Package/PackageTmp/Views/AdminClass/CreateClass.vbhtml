﻿@ModelType WonderfulWardrobe.Biz.ClassViewModel
@Code
    ViewData("Title") = "CreateClass"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code
<link href="~/Content/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<script src="~/Content/bootstrap-datetimepicker.min.js"></script>
@Scripts.Render("~/bundles/jqueryval")
@Using Html.BeginForm("CreateClass", "AdminClass", FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Create Class</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">

                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <div class="row-fluid" style="float: none;">
                            <div class="span5" style="float: none;">

                                <label for="term">Term:</label>

                                @Html.DropDownListFor(Function(m) m.TermId, DirectCast(ViewBag.TermList, List(Of SelectListItem)), New With { _
                                Key .[class] = "span12 m-wrap" _
                                })
                                
                                <label for="venue">Venue:</label>
                                @Html.DropDownListFor(Function(m) m.VenueId, DirectCast(ViewBag.VenueList, List(Of SelectListItem)), New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="class_StartDateTime">StartDateTime:</label>
                               
                                <div class="input-append span12 m-wrap" id="datetimepicker">
                                    @Html.TextBoxFor(Function(m) m.StartDateTime, New With { _
                                Key .[class] = "span11 m-wrap" _
                                })
                                    <span class="add-on">
                                        <i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar">
                                        </i>
                                    </span>
                                </div>

                                <label for="duration">Duration(Minutes):</label>
                                @Html.TextBoxFor(Function(m) m.Duration, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="capacity">Capicity:</label>
                                @Html.TextBoxFor(Function(m) m.Capacity, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="no_of_weeks">No of Weeks:</label>
                                @Html.TextBoxFor(Function(m) m.Weeks, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="price">Price:</label>
                                @Html.TextBoxFor(Function(m) m.Price, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <br />
                                <br />
                                <input type="submit" name="submit" class="btn" value="Save" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </text>
End Using

<script type="text/javascript">
    $(function () {
        $('#datetimepicker').datetimepicker({
            language: 'en',
            format: 'MM/dd/yyyy HH:mm:ss PP',
            pick12HourFormat: false,
            autoclose: true
        });
    });
</script>



<script>
    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            TermId: "required",
            VenueId: "required",
            StartDateTime: "required",
            Duration: "required",
            Duration: {
                required: true,
                number: true
            },
            Capicity: {
                required: true,
                number: true
            },
            Weeks: {
                required: true,
                number: true
            },
            Price: {
                required: true,
                number: true
            }


        },
        messages: {
            TermId: "*Required Field",
            VenueId: "*Required Field",
            StartDateTime: "*Required Field",
            Duration: "*Required Field and Decimal Value Only",
            Price: "*Required Field and Decimal Value Only",
            Capicity: "*Required Field and Numeric Value Only",
            Weeks: "*Required Field and Numeric Value Only",
        },
        submitHandler: function (form) {
            var TermId = $("#TermId").val();
            var VenueId = $("#VenueId").val();
            var errors;
            if (TermId == "0") {
                errors = {
                    TermId: "*Required Field",

                }
                validator.showErrors(errors);
            }
            if (VenueId == "0") {
                errors = {
                    VenueId: "*Required Field",
                }
                validator.showErrors(errors);
            }
            else {
                form.submit();
            }

        }
    });
</script>


<div style="padding-bottom:10px;">
    @Html.ActionLink("Back to List", "ClassesList")
</div>


