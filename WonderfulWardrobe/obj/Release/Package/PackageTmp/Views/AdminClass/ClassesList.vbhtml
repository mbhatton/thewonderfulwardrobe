﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.ClassViewModel
@Code
    ViewData("Title") = "ClassesList"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

@Using Html.BeginForm("CreateClasses")
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Classes</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">
                        @Html.ActionLink("Create New", "CreateClass")
                        <table class="table-bordered table-striped table-condensed t-cus">
                            <tr>
                                <th>
                                    StartDateTime
                                </th>
                                <th>
                                    Duration
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Capicity
                                </th>
                                <th>
                                    Term
                                </th>
                                <th>
                                    Venue
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>

                            @For Each items As ClassViewModel In Model.ClassesList
                                    Dim time As DateTime = DateTime.Today.Add(New TimeSpan(Convert.ToInt32(items.StartDateTime.TimeOfDay.Hours), Convert.ToInt32(items.StartDateTime.TimeOfDay.Minutes), 0))
                                    Dim myNewTimeString As String = time.ToString("h:mm tt")
                                    Dim day = DateTime.Parse(items.StartDateTime).DayOfWeek.ToString().Substring(0, 3)
                                @:<tr>
                                    @:<td>
                                      @day@:&nbsp;&nbsp; @items.StartDateTime.ToString("MMM dd yyy")&nbsp;&nbsp;@myNewTimeString
                                        @:</td>
                                    @:<td>
                                        @items.Duration 
                                        @:</td>
                                    @:<td>
                                        @items.Price.ToString("N2")
                                        @:</td>
 @:<td>
                                        @items.Capacity 
                                        @:</td>
                                    @:<td>
                                        @items.Term 
                                        @:</td>
                                  @:<td>
                                        @items.Venue  
                                        @:</td>
                                    @:<td>
                                        @Html.ActionLink("Edit", "EditClass", "AdminClass", New With { _
                                            Key .id = items.ClassId _
                                            }, Nothing) @:&nbsp;&nbsp;
                                        @Html.ActionLink("Details", "DetailClass", "AdminClass", New With { _
                                            Key .id = items.ClassId _
                                            }, Nothing) @:&nbsp;&nbsp;
                                        @Html.ActionLink("Delete", "DeleteClass", "AdminClass", New With { _
                                            Key .id = items.ClassId _
                                            }, Nothing)
                                        @:</td>
                                    @:</tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </text>
End Using