﻿@Imports WonderfulWardrobe.Biz
@ModelType WonderfulWardrobe.Biz.VenueViewModel
@Code
    ViewData("Title") = "EditVenue"
    Layout = "~/Views/Shared/_AdminLayout.vbhtml"
End Code

<h2>EditVenue</h2>

@Scripts.Render("~/bundles/jqueryval")

@*@Using Html.BeginForm("UpdateVenue", "AdminClass", FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})*@
   
      @Using (Html.BeginForm("UpdateVenue", "AdminClass", FormMethod.Post, New With { _
    Key .enctype = "multipart/form-data" _
, .class = "form-horizontal", .role = "form"}))
    @<text>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-cogs"></i>Create Terms</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body no-more-tables">

                        <div class="messages">
                            <div class="messages error">
                                <p style="color:red;">@ViewBag.Message</p>
                            </div>
                        </div>

                        <div class="row-fluid" style="float: none;">
                            <div class="span5" style="float: none;">
                                <label for="venue_id">Venue ID:</label>
                                @Html.TextBox("VenueId", Model.VenueId, New With {.readonly = "readonly"})

                                <label for="Venue_day">Day:</label>
                                @Html.DropDownListFor(Function(m) m.Day, DirectCast(ViewBag.listitem, List(Of SelectListItem)), New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="venue_name">Name:</label>
                                @Html.TextBox("Name", Model.Name, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="Venue_logo">Logo:</label>
                                <input type="file" name="file" class="span8 m-wrap" />                               
                                <img src="@Url.Content(Model.Logo)" id="logo" height="100" width="100" style="padding-top:5px" alt="" /> 
                                @*<input type="image" src="@Model.Logo" id="logo" name="logo" height="100" width="100" style="padding-top:5px" />*@
                                @Html.Hidden("logo", Model.Logo)

                                <label for="address1">Address1:</label>
                                @Html.TextBox("Address1", Model.Address1, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="address2">Address2:</label>
                                @Html.TextBox("Address2", Model.Address2, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="address3">Address3:</label>
                                @Html.TextBox("Address3", Model.Address3, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="city">City:</label>
                                @Html.TextBox("City", Model.City, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })

                                <label for="address3">Postal Code:</label>
                                @Html.TextBox("PostalCode", Model.PostalCode, New With { _
                                Key .[class] = "span12 m-wrap" _
                                })
                                <br />
                                <br />
                                <input type="submit" name="submit" class="btn" value="Save" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </text>
End Using
<script>
    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            Name: "required",
            Address1: "required",
            Address2: "required",
            Address3: "required",
            City: "required",
            PostalCode: "required",

        },
        messages: {
            Name: "*Required Field",
            Address1: "*Required Field",
            Address2: "*Required Field",
            Address3: "*Required Field",
            City: "*Required Field",
            PostalCode: "*Required Field",

        },
        submitHandler: function (form) {
            var Day = $("#Day").val();
            if (Day == "0") {
                errors = {
                    Day: "*Required Field",

                }
                validator.showErrors(errors);
            }
            else {
                form.submit();
            }
        }
    });
</script>

