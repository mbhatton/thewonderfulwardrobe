﻿@*@Code
    ViewData("Title") = "Index"
End Code

<h2>Index</h2>*@
<div class="body blog clearfix">
    <div class="page">
        <h1>The Wonderful wardrobe <span>Blog</span></h1>
        <div class="posts">
            <article class="post">
                <div class="post-featured-imge"><img src="~/Content/images/blog/featured-image.jpg" height="187" width="187" alt="Featured Image" /></div>
                <h2>This is the blog post title</h2>
                <div class="post-title-links"><span class="post-title-posted">Postedt on </span><span class="post-title-date"><a href="#" title="Post Date">25.09.2013</a></span> - <span class="post-title-category"><a href="#">News</a></span></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril</p>
            </article>
            <article class="post">
                <h2>This is the much much log blog post title</h2>
                <div class="post-title-links"><span class="post-title-posted">Postedt on </span><span class="post-title-date"><a href="#" title="Post Date">25.09.2013</a></span> - <span class="post-title-category"><a href="#">Classes</a></span></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril</p>
            </article>
            <div class="pagination">
                <a href="#" title="Go to Next Page"> &#60;&#60; </a> Page 1 of 3
                <a href="#" title="Go to Next Page"> &#62;&#62;</a>
            </div><!-- .pagination ends -->
        </div><!-- .post ends -->
    </div><!-- .page ends -->
    <aside class="right">
        <h2>Blog Filter</h2>
        <div class="categories">
            <ul>
                <li><a href="#">&#62; All</a></li>
                <li><a href="#">&#62; Classes</a></li>
                <li><a href="#">&#62; Parties</a></li>
                <li><a href="#">&#62; Groups</a></li>
            </ul>
        </div><!-- .categories ends -->
    </aside><!-- .right ends -->
</div>

