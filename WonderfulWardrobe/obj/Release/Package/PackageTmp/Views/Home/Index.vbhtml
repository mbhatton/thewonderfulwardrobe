﻿<div class="home">
    <div class="body clearfix">
        <a href="#" class="facebook-like">Facebook Like</a>
        <aside class="left clearfix">
            <div class="urgant-notice">
                <h2>Urgent Notice Title</h2>
                <p>We are pleased to announce the return of our summer sessions! These MUST be pre-booked on the classes page and remember the ticket covers all of your children up to the age of 6.</p>
                <p>
            </div><!-- .urgant-notice ends -->
            <div class="image">
                <a href="#">
                    <img src="~/Content/images/1.jpg" width="193" height="205" alt="We do Great parties" />
                    <span class="caption">we do great parties >></span>
                </a>
            </div><!-- .image ends -->
            <div class="image rotate">
                <a href="#">
                    <img src="~/Content/images/2.jpg" width="193" height="205" alt="We do Great parties" />
                </a>
            </div><!-- .image ends -->
        </aside><!-- .left ends -->
        <div class="page">
            <h1><span>Weclome to</span> the Wonderful Wardrobe</h1>
            <p>There are pirates and kings and lots of princesses... butterflies and horses and pink fluffy dresses. Cowboys and firemen and animals galore and so many more costumes your child will adore!</p>
            <p>At the Wonderful Wardrobe we know there's no limitation to your child's imagination. That's why we've developed this unique pre-school programme to introduce music, action songs, dressing up and rhymes to your young child.</p>
            <p>We know that the Wonderful Wardrobe gives your child the best possible start in life, giving them a fantastic outlet for their creativity . It also improves their learning skills, physical development and social skills but most importantly they're having fun!</p>
            <p>We know that every child is special, regardless of their age, background or ability, so that's why we've developed this all singing, all dancing programme for your little ones.</p>
            <div class="facebook-box">
                <h2><span>Wonderful Wardrobe</span> Latest News</h2>
                <a href="#" class="facebook" title="Facebook">Faceebook</a>
                <hr />
                <div class="scroll-pane">
                    <div class="comment clearfix">
                        <div class="avatar">
                            <a href="#"><img src="~/Content/images/avatar.png" width="48" height="55" alt="Avatar" /></a>
                        </div><!-- .avatar ends -->
                        <div class="post">
                            <h3><a href="#">The Wonderful Wardrobe</a></h3>
                            <p>We are at Penarth this morning, it's all Picnics, teddy bears and T shirts. Looking forward to seeing lots of new friends, x</p>
                            <span class="date-time"><a href="#">September 18 at 8:29am</a></span>
                        </div><!-- .post ends -->
                    </div><!-- DIV ends -->
                    <div class="comment clearfix">
                        <div class="avatar">
                            <a href="#"><img src="~/Content/images/avatar.png" width="48" height="55" alt="Avatar" /></a>
                        </div><!-- .avatar ends -->
                        <div class="post">
                            <h3><a href="#">The Wonderful Wardrobe</a></h3>
                            <p>We are at Penarth this morning, it's all Picnics, teddy bears and T shirts. Looking forward to seeing lots of new friends, x</p>
                            <span class="date-time"><a href="#">September 18 at 8:29am</a></span>
                        </div><!-- .post ends -->
                    </div><!-- DIV ends -->
                    <div class="comment clearfix">
                        <div class="avatar">
                            <a href="#"><img src="~/Content/images/avatar.png" width="48" height="55" alt="Avatar" /></a>
                        </div><!-- .avatar ends -->
                        <div class="post">
                            <h3><a href="#">The Wonderful Wardrobe</a></h3>
                            <p>We are at Penarth this morning, it's all Picnics, teddy bears and T shirts. Looking forward to seeing lots of new friends, x</p>
                            <span class="date-time"><a href="#">September 18 at 8:29am</a></span>
                        </div><!-- .post ends -->
                    </div><!-- DIV ends -->
                    <div class="comment clearfix">
                        <div class="avatar">
                            <a href="#"><img src="~/Content/images/avatar.png" width="48" height="55" alt="Avatar" /></a>
                        </div><!-- .avatar ends -->
                        <div class="post">
                            <h3><a href="#">The Wonderful Wardrobe</a></h3>
                            <p>We are at Penarth this morning, it's all Picnics, teddy bears and T shirts. Looking forward to seeing lots of new friends, x</p>
                            <span class="date-time"><a href="#">September 18 at 8:29am</a></span>
                        </div><!-- .post ends -->
                    </div><!-- DIV ends -->
                </div><!-- .scroll-pane ends -->
            </div><!-- .facebook-box ends -->
        </div><!-- .page ends -->
        <aside class="right">
            <div class="twitter">
                <h2>Follow us >></h2>
                <div>
                    <h3>Twitter Feed</h3>
                    <ul>
                        <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                        <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                    </ul>
                </div>
            </div><!-- .twitter ends -->
            <div class="testimonal">
                <p>"We are pleased to announce the return of our summer sessions! These MUST be pre-booked on the classes page and remember the ticket covers all of your children up to the age of 6."</p>
                <h3>Mr Joe Bloggs, Cardiff</h3>
                <div class="tip"></div>
            </div><!-- .testimonal ends -->
        </aside><!-- .right ends -->
    </div>
    </div>
