﻿@*@Code
    ViewData("Title") = "Booking"
End Code

<h2>Booking</h2>*@
@*<style>
    .page {
        width: 75% !important;
    }


    .home .page h2 {
        color: inherit !important;
        font: inherit !important;
    }
</style>*@

<div class="body parties-booking-page clearfix">
    <a href="#" class="facebook-like">Facebook Like</a>
    <div class="page">
        <h1>Your Booking</h1>
        <p class="note">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo fugiat nulla pariatur.</p>
        <div class="form-wrapper">
            <form action="#" method="post" id="parties-booking-detail">
                <h2 class="following-heading">The Following dates are available <em>Please remove unwanted dates </em> <a href="#" title="Undo your list">Undo</a></h2>
                <div class="detail">
                    <div class="form-row">
                        <table>
                            <thead>
                                <tr>
                                    <th class="date">Date</th>
                                    <th class="time">Time</th>
                                    <th class="duration">Duration</th>
                                    <th class="price">Price</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Wed 02 Oct 2013</td>
                                    <td>10:00</td>
                                    <td>1 Hour</td>
                                    <td>&#163; 100.00</td>
                                    <td class="close"><span><a href="#" class="close-btn">Close</a></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="total-amount clearfix">
                            <span class="amount">Total: &#163;100:00</span>
                            <span class="pay-now"><input type="button" name="add" value="Next" class="btn pink-btn" /></span>
                        </div><!-- .total-amount ends -->
                    </div><!-- div.form-row -->
                </div><!-- .detail ends -->
                <h4>Yout Details</h4>
                <div>
                    <div class="form-row">
                        <label for="first_detail_name">First Name:</label>
                        <input type="text" name="first_detail_name" id="first_detail_name" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="last_detail_name">Last Name:</label>
                        <input type="text" name="last_detail_name" id="last_detail_name" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="address1">Address:</label>
                        <input type="text" name="address1" id="address1" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label>&nbsp;</label>
                        <input type="text" name="address2" id="address2" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label>&nbsp;</label>
                        <input type="text" name="address3" id="address3" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="postcode">Postcode:</label>
                        <input type="text" name="postcode" id="postcode" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="telenumber">Tel Number:</label>
                        <input type="tel" name="telenumber" id="telenumber" />
                    </div><!-- .form-row -->
                </div><!-- DIV ends -->
                <h4>Name(s) of the children the party is for</h4>
                <div>
                    <div class="form-row">
                        <label for="first_child_name">First Name:</label>
                        <input type="text" name="first_child_name" id="first_child_name" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="last_child_name">Last Name:</label>
                        <input type="text" name="last_child_name" id="last_child_name" />
                    </div><!-- .form-row -->
                    <div class="form-row dob">
                        <label for="day">Date of Birth</label>
                        <input type="text" name="day" id="day" class="mini" />
                        <span class="seprator">/</span>
                        <input type="text" name="month" id="month" class="mini" />
                        <span class="seprator">/</span>
                        <input type="text" name="year" id="year" class="small" />
                    </div>
                    <table>
                        <tbody>
                            <tr>
                                <td class="child-name">Child name 1</td>
                                <td class="dob"><span>-</span>D.O.B. <span>01/01/2008</span></td>
                                <td class="close"><span><a href="#" class="close-btn">Close</a></span></td>
                            </tr>
                            <tr>
                                <td class="child-name">Child name 1</td>
                                <td class="dob"><span>-</span>D.O.B. <span>01/01/2008</span></td>
                                <td class="close"><span><a href="#" class="close-btn">Close</a></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- DIV ends -->
                <h4>Party Details</h4>
                <div>
                    <div class="form-row">
                        <label for="party_reason">Reason for Party?</label>
                        <textarea name="party_reason" id="party_reason"></textarea>
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="party_location">Party Location:</label>
                        <textarea name="party_location" id="party_location"></textarea>
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="children_attending">Number of children attending:</label>
                        <input type="text" name="children_attending" id="children_attending" class="mini" />
                    </div><!-- .form-row ends -->
                </div><!-- DIV ends -->
                <p class="note">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                <div class="total-amount clearfix">
                    <span class="amount">Total: &#163;100:00 Deposit payment required of &#163;50:00</span>
                    <span class="pay-now"><input type="submit" name="pay_deposit" value="Pay Deposit" class="btn pink-btn" /></span>
                </div><!-- .total-amount ends -->
                <p class="validation-warning"><strong>*Please review errors above</strong></p>
            </form>
        </div><!-- .form-wrapper ends -->
    </div><!-- .page ends -->
    <aside class="right">
        <div class="logged-in">
            <h2>Currently logged in as</h2>
            <div class="user">Matt Hatton</div>
            <div class="logout">@Html.ActionLink("Logout", "Logout", "Account")</div>
        </div><!-- .logged-in -->
    </aside><!-- .right ends -->
</div>
<script>

    // FORM VALIDATION
    jQuery("#parties-booking-detail").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            first_detail_name: "required",
            last_detail_name: "required",
            address1: "required",
            address2: "required",
            address3: "required",
            postcode: "required",
            telenumber: "required",
            first_child_name: "required",
            last_child_name: "required",
            year: "required",
            party_reason: "required",
            party_location: "required",
            children_attending: "required"

        },
        messages: {
            first_detail_name: "*Required Field",
            last_detail_name: "*Required Field",
            address1: "*Required Field",
            address2: "*Required Field",
            address3: "*Required Field",
            postcode: "*Required Field",
            telenumber: "*Required Field",
            first_child_name: "*Required Field",
            last_child_name: "*Required Field",
            year: "*Required Field",
            party_reason: "*Required Field",
            party_location: "*Required Field",
            children_attending: "*Required Field"
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>