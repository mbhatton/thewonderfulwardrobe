﻿@*@Code
    ViewData("Title") = "Index"
End Code

<h2>Index</h2>*@
@*<style>
    .page {
        width:75% !important;
    }
    .party-timming h2 {
        color: #EB6D96 !important;
    }
    .parties-prices h2 {
        color: #BD5999 !important;
    }
</style>*@
<div class="body parties-homepage clearfix">
    <a href="#" class="facebook-like">Facebook Like</a>
    <div class="page">
        <h1>The Wonderful wardrobe <span>Parties</span></h1>
        <p class="note">A Wonderful Wardrobe party is a unique and enchanting experience for boys and girls...</p>
        <div class="intro">
            <img class="image-left" src="~/Content/images/3.jpg" width="187" height="187" alt="Image3" />
            <p>We'll write a special story all about the birthday child and their favourite things, but don't worry it's not a sit down and listen type of story... it's lots of up and about, music, rhymes, action songs and fun, fun, fun.</p>
            <p>The children will have plenty of costumes to choose from The Wonderful Wardrobe tree, whether they want to be pirates or princess, lions or horses, police officers or fairies. Then they'll be transported to a world of make believe... where anything can happen.</p>
            <a href="#" title="Find out more" class="find-more">Find out more >> </a></p>
        </div><!-- intro ends -->
        <div class="party-timming">
            <h2>Our Parties Time Sots</h2>
            <div class="clearfix">
                <div class="ante-meridian"><div><span class="clock">Clock</span><strong>AM <span>-</span></strong>Start at or before 11am</div></div>
                <div class="post-meridian"><div><span class="clock">Clock</span><strong>PM <span>-</span></strong>Start at or after 2pm</div></div>
                <div class="anytime"><div><span class="clock">Clock</span><strong>Anytime <span>-</span></strong>Booked outside above time slots or location above 25 minute radius from us </div></div>
            </div>
        </div><!-- .party-timming ends -->
        <div class="parties-prices">
            <h2>Our Parties Prices</h2>
            <ul class="clearfix">
                <li>1Hour - <span>&#163;100</span></li>
                <li>2Hour - <span>&#163;150</span></li>
            </ul>
            <p>Anytime (2 hour) or location above 25 minute radius from us <span>-</span> &#163;185</p>
        </div> <!-- .partie-prices ends -->
        <div class="form-wrapper">
            <form action="#" method="post" id="parties-homepage">
                <h2>Enquire about our availability for a Party</h2>
                <div>
                    <div class="form-row">
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="name" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="email">Email:</label>
                        <input type="email" name="email" id="email" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="phonenumber">Phone Number:</label>
                        <input type="tel" name="phonenumber" id="phonenumber" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="preferred_date1">1st Preferred date:</label>
                        <input type="text" name="preferred_date1" id="preferred_date1" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="preferred_date2">2nd Preferred date:</label>
                        <input type="text" name="preferred_date2" id="preferred_date2" />
                    </div><!-- .form-row -->
                    <div class="form-row">
                        <label for="preferred_start_time">Preferred Start Time:</label>
                        <input type="text" name="preferred_start_time" id="preferred_start_time" />
                    </div><!-- .form-row -->
                    <div class="form-row radio">
                        <input type="radio" id="1hour" value="1hour" name="hour" />
                        <label for="1hour">1 Hour</label>
                        <input type="radio" id="2hour" value="2hour" name="hour" />
                        <label for="2hour">2 Hour</label>
                    </div><!-- .form-row -->
                    <div class="form-row submit">
                        <input type="submit" id="sumbit" value="Send" class="btn pink-btn" />
                        <p class="validation-warning"><strong>*Please review errors above</strong></p>
                    </div>
                </div>
            </form>
        </div><!-- .form-wrapper ends -->
    </div><!-- .page ends -->
    <aside class="right">
        <div class="twitter">
            <h2>Follow us >></h2>
            <div>
                <h3>Twitter Feed</h3>
                <ul>
                    <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                    <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                </ul>
            </div>
        </div><!-- .twitter ends -->
    </aside><!-- .right ends -->
</div>

<script>
    // ICHECK PLUGIN
    $(".radio input").iCheck({
        radioClass: 'iradio'
    });

    // FORM VALIDATION
    jQuery("#parties-homepage").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            name: "required",
            email: "required",
            phonenumber: "required",
            preferred_date1: "required",
            preferred_date2: "required",
            preferred_start_time: "required",
            hour: "required"
        },
        messages: {
            name: "*Required Field",
            email: "*Required Field",
            phonenumber: "*Required Field",
            preferred_date1: "*Required Field",
            preferred_date2: "*Required Field",
            preferred_start_time: "*Required Field",
            hour: "*Required Field",
        }

    });
</script>