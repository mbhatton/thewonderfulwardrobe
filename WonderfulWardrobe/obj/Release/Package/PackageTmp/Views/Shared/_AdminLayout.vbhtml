﻿
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>@ViewBag.Title - Wondeful Wardrobe</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    @Styles.Render("~/Content/bootstrapmincss")
    @Styles.Render("~/Content/bootstrapresponsivemincss")
    @Styles.Render("~/Content/fontawesomemin")
    @Styles.Render("~/Content/styleresponsive")
    @Styles.Render("~/Content/bootstrapmodal")
    @Scripts.Render("~/bundles/jquery-1.10.1")
    <link rel="shortcut icon" href="~/Content/images/favicon.ico" />
    
</head>

<body class="page-header-fixed">
    <!-- BEGIN HEADER -->
    <div class="header navbar navbar-inverse navbar-fixed-top">
        <!-- BEGIN TOP NAVIGATION BAR -->
        <div class="navbar-inner">
            <div class="container-fluid">
                <!-- BEGIN LOGO -->
                <a class="brand" href="~/AdminHome/Index">
                    <span>WoderfulWardrobe</span>
                </a>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                    <img src="~/Content/assets/img/menu-toggler.png" alt="">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="nav pull-right">
                   
                    @Html.ActionLink("Logout", "Logout", "Account", Nothing, New With { _
                Key .style = "color:#FFFFFF" _
                })
                    <!-- END USER LOGIN DROPDOWN -->
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>  
        </div>
        <!-- END TOP NAVIGATION BAR -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="page-sidebar-menu">
                <li>
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <div class="sidebar-toggler hidden-phone"></div>
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                </li>
                <li>
                    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                  
                    <!-- END RESPONSIVE QUICK SEARCH FORM -->
                </li>
                <li class="active">
                    <a href="~/AdminHome/Index">
                        <i class="icon-home"></i>
                        <span class="title">Home</span>
                    </a>
                </li>
                @If User.IsInRole("SuperAdmin") Then

                     @<li class="">
                    <a href="javascript:;">
                        <i class="icon-tags"></i>
                        <span class="title">Manage Admin</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="~/SuperAdmin/AdminList">
                                <span class="title">Admin</span>
                            </a>
                        </li>                       
                    </ul>
                </li>
                   
                End If
         
               
                <li class="">
                    <a href="javascript:;">
                        <i class="icon-tags"></i>
                        <span class="title">Class</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="~/AdminClass/TermsList">
                                <span class="title">Terms</span>
                            </a>
                        </li>
                        <li>
                            <a href="~/AdminClass/VenuesList">
                                <span class="title">Venues</span>
                            </a>
                        </li>
                        <li>
                            <a href="~/AdminClass/ClassesList">
                                <span class="title">Class</span>
                            </a>
                        </li>
                       
                    </ul>
                </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">

                        <h3 class="page-title">
                            Dashboard
                        </h3>
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="~/AdminHome/Index">Home</a>
                                <span class="icon-angle-right"></span>
                            </li>
                          </ul>
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                
                @RenderBody()
                
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="footer">
        <div class="footer-inner">
            Copyrights 2013, All rights reserved
        </div>
        <div class="footer-tools">
            <span class="go-top">
                <i class="icon-angle-up"></i>
            </span>
        </div>
    </div>
    <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
   
    @Scripts.Render("~/bundles/jquery-ui-1.10.1.custom.min")
    @Scripts.Render("~/bundles/bootstrap.min")
    @Scripts.Render("~/bundles/bootstrap-modal")
    @Scripts.Render("~/bundles/bootstrap-modalmanager")
    @Scripts.Render("~/bundles/app")
    @Scripts.Render("~/bundles/ui-modals")
   
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function () {
            // initiate layout and plugins
            App.init();
            UIModals.init();
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>

</html>
