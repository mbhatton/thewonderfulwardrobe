﻿@ModelType WonderfulWardrobe.Biz.AccountViewModel
@*<div class="row">
    <div class="col-md-8">
        <section id="loginForm">
            @Using Html.BeginForm("Login", "Account", New With { .ReturnUrl = ViewBag.ReturnUrl }, FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})
                @Html.AntiForgeryToken()
                @<text>
                <h4>Use a local account to log in.</h4>
                <hr />
                @Html.ValidationSummary(True)
                <div class="form-group">
                    @Html.LabelFor(Function(m) m.UserName, New With {.class = "col-md-2 control-label"})
                    <div class="col-md-10">
                        @Html.TextBoxFor(Function(m) m.UserName, New With {.class = "form-control"})
                        @Html.ValidationMessageFor(Function(m) m.UserName)
                    </div>
                </div>
                <div class="form-group">
                    @Html.LabelFor(Function(m) m.Password, New With {.class = "col-md-2 control-label"})
                    <div class="col-md-10">
                        @Html.PasswordFor(Function(m) m.Password, New With {.class = "form-control"})
                        @Html.ValidationMessageFor(Function(m) m.Password)
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <div class="checkbox">
                            @Html.CheckBoxFor(Function(m) m.RememberMe)
                            @Html.LabelFor(Function(m) m.RememberMe)
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input type="submit" value="Log in" class="btn btn-default" />
                    </div>
                </div>
                <p>
                    @Html.ActionLink("Register", "Register") if you don't have a local account.
                </p>
                </text>
            End Using
        </section>
    </div>
    <div class="col-md-4">
        <section id="socialLoginForm">
            @Html.Partial("_ExternalLoginsListPartial", New With {.Action = "ExternalLogin", .ReturnUrl = ViewBag.ReturnUrl})
        </section>
    </div>
</div>*@

    <div class="body login-page clearfix">
        <a href="#" class="facebook-like">Facebook Like</a>
        <div class="page">
            <h1>Login</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

            @*<h2>Not Registered? <a href="#">Register here</a></h2>*@
            <h2>Not Registered? <a @Html.ActionLink("Register Here", "Register", "Account")</a></h2>
            <div class="form-wrapper">
                @Using Html.BeginForm("Login", "Account", FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})
                    @<text>
                <div class="form-row" style="padding: 0px 0px 0px 20px;">
                    <label for="email">Email Address:</label>
                    @Html.TextBoxFor(Function(m) m.EmailId)
                </div>
                        <div class="form-row">
                            <label for="email">Password:</label>
                            @Html.PasswordFor(Function(m) m.Password)
                        </div>
                        <div class="form-row">
                            <p style="color:red;">@ViewBag.Message</p>
                            <input type="submit" name="submit" id="submit" value="next" class="btn pink-btn next" />
                        </div>
                    </text>
End Using
                @*</form>*@
            </div><!-- .form-wrapper ends -->
        </div><!-- .page ends -->
        <aside class="right">
            <div class="twitter">
                <h2>Follow us >></h2>
                <div>
                    <h3>Twitter Feed</h3>
                    <ul>
                        <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                        <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                    </ul>
                </div><!-- DIV ends -->
            </div><!-- .twitter ends -->
        </aside><!-- .right ends -->
    </div>

<script>
    // FORM VALIDATION
    jQuery(".form-horizontal").validate({
        rules: {
            EmailId: {
                required: true,
                email: true
            }
        },
        messages: {
            EmailId: "*Please Enter Valid Email Address",
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>
@*@Section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section*@
