﻿
@ModelType WonderfulWardrobe.Biz.AccountViewModel

<div class="classes-page">
    <div class="body clearfix">
        <a href="#" class="facebook-like">Facebook Like</a>
        <div class="page">
            <h1>Registration</h1>
            <p class="note">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore<br /> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br /> consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            <div class="form-wrapper">
               
                    @Using Html.BeginForm("Register", "Account", FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})
                        @<text>
                            <h4>Your details</h4>
                            <div>
                                <div class="form-row">

                                    <label for="first_name">First Name:</label>
                                    @Html.TextBoxFor(Function(m) m.FirstName)

                                </div>
                                <div class="form-row">
                                    <label for="last_name">Last Name:</label>
                                    @Html.TextBoxFor(Function(m) m.LastName)
                                </div>
                                <div class="form-row">
                                    <label for="email_address">Email Address:</label>
                                    @Html.TextBoxFor(Function(m) m.EmailId)
                                </div>
                                <div class="form-row">
                                    <label for="password">Password</label>
                                    @Html.PasswordFor(Function(m) m.Password)
                                </div>
                                <div class="form-row">
                                    <label for="retype_password">Re-type Password</label>
                                    <input type="password" name="retype_password" id="retype_password" />
                                </div>
                                <div class="form-row">
                                    <label for="password_hint">Password Hint</label>
                                    @Html.TextBoxFor(Function(m) m.PasswordHint)
                                </div>
                            </div><!-- DIV ends -->
                            <h4>Childs details</h4>
                            <div>
                                <div class="form-row">
                                    <label>Add a child:</label>
                                    <a href="#" class="add-child">Add+</a>
                                </div>
                                <div class="child-detail">
                                    <div class="form-row">
                                        <label for="child_first_name">First Name:</label>
                                        <input type="text" name="child_first_name" id="child_first_name" />
                                    </div>
                                    <div class="form-row">
                                        <label for="child_last_name">Last Name:</label>
                                        <input type="text" name="child_last_name" id="child_last_name" />

                                    </div>
                                    <div class="form-row dob">
                                        <label for="day">Date of Birth:</label>
                                        <input type="text" name="day" id="day" class="mini" />
                                        <span class="seprator">/</span>
                                        <input type="text" name="month" id="month" class="mini" />
                                        <span class="seprator">/</span>
                                        <input type="text" name="year" id="year" class="small" />
                                    </div>
                                    <div class="form-row radio">
                                        <input type="radio" id="boy" value="Boy" name="gender" checked />
                                        <label for="boy">Boy</label>
                                        <input type="radio" id="girl" value="Girl" name="gender" />
                                        <label for="girl">Girl</label>
                                    </div>
                                    <div class="form-row add-btn">
                                        <input type="button" id="add" name="add" class="btn pink-btn button" value="Add" />
                                    </div>
                                    <div class="child-list" id="child-list">
                                        <table>
                                            <tbody></tbody>
                                        </table>
                                    </div><!-- .child-list -->
                                </div><!-- .child-detail ends -->
                            </div><!-- Div ends -->
                            <div class="form-row submit">
                                <input type="submit" name="submit" class="btn pink-btn next" value="Next" />
                                <p style="color:red;">@ViewBag.Message</p>
                                <p class="validation-warning"><strong>*Please review errors above</strong></p>
                            </div>
                        </text>
                    End Using
</div><!-- .form ends -->
        </div><!-- .page ends -->
        <aside class="right">
            <div class="twitter">
                <h2>Follow us >></h2>
                <div>
                    <h3>Twitter Feed</h3>
                    <ul>
                        <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                        <li><a href="#">TheWWardrobe</a> - 16 Sep Lovely to see so many princesses, pirates, monsters, angels and animals today FLC_Cardiff</li>
                    </ul>
                </div><!-- DIV ends -->
            </div><!-- .twitter ends -->
        </aside><!-- .right ends -->
    </div>
    </div>
<script>

    var inputs = document.getElementsByTagName("input");
    for (var i = inputs.length - 1; i >= 0; i--) {
        if (inputs[i].getAttribute("type") === "radio") {
            inputs[i].checked = true;
        }
    }
    // ICHECK PLUGIN
    $(".radio input").iCheck({
radioClass:  'iradio'
    });
    var i = 1;

    // SHOWS CHILD DETAIL ENDS
    $('.add-child').click(function () {
        $('.child-detail').show();
        return false;
    })

   
    // FORM VALIDATION
    var validator = jQuery(".form-horizontal").validate({
        errorContainer: $(".validation-warning"),
        rules: {
            FirstName: "required",
            LastName: "required",
            EmailId: {
                required: true,
               email:true
            },
            Password: {
                required: true,
                minlength: 8
            },

            retype_password : {
                minlength: 8,
                equalTo : "#Password"
            },
            PasswordHint: "required",  
        },
        messages: {
            FirstName: "*Required Field",
            LastName: "*Required Field",
            EmailId: "*Invalid Email Address",
            Password: "*Minimum of 8 characters with atleast 1 special charecter required",
            retype_password: "*Passwords do not match",
            PasswordHint: "*Required Field",
        },
        submitHandler: function (form) {
            var name = $("#Password").val();
            var errors;
            if (!isLetters(name)) {
            }
            function isLetters(str) {
                if (/^[a-zA-Z0-9- ]*$/.test(str) == false) {
                    form.submit();
                }
                else {
                    errors = {
                        Password: "*Minimum of 8 characters with atleast 1 special charecter required"
                    }
                    validator.showErrors(errors);
                }
            }
           
        }
    });

    $('table tbody .close-btn').live('click', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var name = tr.find('.ss').attr('name');
        var array = name.split('e');
        var count = $('<input type="hidden" id="removecount" name="removecount" value="' + array[array.length - 1] + '" />');
        $('.child-list table').append(count);
        tr.fadeOut(400, function () {
            tr.remove();
        });
    });

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]);
    }
    // ASIGNING VALUES TO DYNAMICALY GANERATED TABLE ROW
    $('.button').click(function () {
        child_fname = $('#child_first_name').val();
        child_lname = $('#child_last_name').val();
        child_day = $('#day').val();
        child_month = $('#month').val();
        child_year = $('#year').val();
        child_gender = $('.radio input:checked').val();
     
        var errors;
        if ($.trim($('#child_first_name').val()) == '') {
            errors = {
                child_first_name: "*Required Field"
            };
            validator.showErrors(errors);

        } else if ($.trim($('#child_last_name').val()) == '') {
            errors = {
                child_last_name: "*Required Field"
            }
            validator.showErrors(errors);

        } else if ($.trim($('#year').val()) == '') {
            errors = {
                year: "*Incorrect Format"
            }
            validator.showErrors(errors);

        } else if (!$('input[name=gender]:checked').val()) {
            errors = {
                gender: "*Please Select"
            }
            validator.showErrors(errors);

        } else if (child_year.length != 4) {
            errors = {
                year: "*Invalid Format"
            }
            validator.showErrors(errors);

        } else if (!isValidDate(child_day + '/' + child_month + '/' + child_year)) {
            errors = {
                year: "*Invalid Format"
            }
            validator.showErrors(errors);

        }else {
            i++;
            var newrow = $('<tr><td class="child-name">Name: <label for="childname' + i + '" style="text-align:left">' + child_fname + ' ' + child_lname + '</label><input class="ss" type="hidden" id="childname' + i + '" name="childname' + i + '" value="' + child_fname + ' ' + child_lname + '" /> </td><td class="dob">Date of Birth:<label for="DOB' + i + '"  style="text-align:left">' + child_day + '/' + child_month + '/' + child_year + '</label><input type="hidden" id="DOB' + i + '" name="DOB' + i + '" value="' + child_day + '/' + child_month + '/' + child_year + '" /></td><td class="gender"> <label for="gender' + i + '" style="text-align:left">' + child_gender + '</label><input type="hidden" id="gender' + i + '" name="gender' + i + '" value="' + child_gender + '" /></td><td class="close"><span><a href="#" class="close-btn">Close</a></span></td></tr> ');
            $('.child-list table').append(newrow);
            $('#child_first_name').val('');
            $('#child_last_name').val('');
            $('#day').val('');
            $('#month').val('');
            $('#year').val('');
           
        }
        var count = $('<input type="hidden" id="count" name="count" value="' + i + '" />');
        $('.child-list table').append(count);
    });
</script>
    @section Scripts
        @Scripts.Render("~/bundles/jqueryval")
    End Section
