﻿Imports Newtonsoft.Json

Public Class TransectionDetails

    <JsonProperty(PropertyName:="id")> _
    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Set(value As String)
            m_Id = value
        End Set
    End Property
    Private m_Id As String


    <JsonProperty(PropertyName:="parent_payment")> _
    Public Property TransactionId() As String
        Get
            Return m_TransactionId
        End Get
        Set(value As String)
            m_TransactionId = value
        End Set
    End Property
    Private m_TransactionId As String

    <JsonProperty(PropertyName:="amount")> _
    Public Property PaidAmount() As Amount
        Get
            Return m_PaidAmount
        End Get
        Set(value As Amount)
            m_PaidAmount = value
        End Set
    End Property
    Private m_PaidAmount As Amount

    Public Class Amount
        <JsonProperty(PropertyName:="currency")> _
        Public Property Currency() As String
            Get
                Return m_Currency
            End Get
            Set(value As String)
                m_Currency = value
            End Set
        End Property
        Private m_Currency As String

        <JsonProperty(PropertyName:="total")> _
        Public Property Total() As String
            Get
                Return m_Total
            End Get
            Set(value As String)
                m_Total = value
            End Set
        End Property
        Private m_Total As String
    End Class

  
   
End Class