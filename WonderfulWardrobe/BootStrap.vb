﻿Imports Autofac
Imports Autofac.Integration.Mvc
Imports System.Web.Mvc
Imports System.Reflection
Imports WonderfulWardrobe.Biz

Public Class BootStrap

    Public Property myContainer() As IContainer
        Get
            Return m_myContainer
        End Get
        Private Set(value As IContainer)
            m_myContainer = value
        End Set
    End Property
    Private m_myContainer As IContainer

    Public Sub Configure()
        Dim builder As New ContainerBuilder()
        OnConfigure(builder)

        If Me.myContainer Is Nothing Then
            Me.myContainer = builder.Build()
        Else
            builder.Update(Me.myContainer)
        End If

        'This tells the MVC application to use myContainer as its dependency resolver
        DependencyResolver.SetResolver(New AutofacDependencyResolver(Me.myContainer))

    End Sub

    Protected Overridable Sub OnConfigure(builder As ContainerBuilder)
        'This is where you register all dependencies

        'The line below tells autofac, when a controller is initialized, pass into its constructor, the implementations of the required interfaces

        builder.RegisterControllers(Assembly.GetExecutingAssembly())

        'The line below tells autofac, everytime an implementation IDAL is needed, pass in an instance of the class DAL
        Dim register As New AutofacRegisterType
        builder = register.RegisterBuilderType(builder)
    End Sub
End Class
