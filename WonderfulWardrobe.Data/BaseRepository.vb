﻿Imports System.Data.SqlClient
Imports System.Web.Configuration

Public Class BaseRepository
    'Protected Shared Sub SetIdentity(Of T)(connection As IDbConnection, setId As Action(Of T))
    '    Dim identity As Dynamic = connection.Query("SELECT @@IDENTITY AS Id").[Single]()
    '    Dim newId As T = DirectCast(identity.Id, T)
    '    setId(newId)
    'End Sub

    Public Function OpenConnection() As IDbConnection
        Dim connection As IDbConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("SqlConnection").ConnectionString)
        connection.Open()
        Return connection
    End Function

End Class
