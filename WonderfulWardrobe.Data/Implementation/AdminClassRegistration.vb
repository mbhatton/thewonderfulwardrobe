﻿Imports WonderfulWardrobe.Model
Imports Dapper
Public Class AdminClassRegistration
    Implements IAdminClassRegistration
    Public Sub CreateTerms(terms As Terms) Implements IAdminClassRegistration.CreateTerms

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Terms(Name, DiscountAmount,TotalAmount, CreatedOn, IsActive) " & "VALUES (@Name, @DiscountAmount,@TotalAmount, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
            Key .Name = terms.Name, _
            Key .DiscountAmount = Convert.ToDecimal(terms.DiscountAmount), _
                 Key .TotalAmount = terms.TotalAmount, _
            Key .CreatedOn = Convert.ToDateTime(terms.CreatedOn), _
            Key .IsActive = True _
            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using
    End Sub


    Public Sub CreateVenues(venues As Venue) Implements IAdminClassRegistration.CreateVenues

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Venues(Name,Logo,Day, Address1, Address2, Address3,City,Longitude,Latitude,PostalCode, CreatedOn, IsActive) " & "VALUES (@Name,@Logo,@Day, @Address1, @Address2, @Address3,@City,@Longitude,@Latitude,@PostalCode, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
            Key .Name = venues.Name, _
                 Key .Logo = venues.Logo, _
                Key .Day = venues.Day, _
            Key .Address1 = venues.Address1, _
            Key .Address2 = venues.Address2, _
            Key .Address3 = venues.Address3, _
                Key .City = venues.City, _
                 Key .Longitude = venues.Longitude, _
                Key .Latitude = venues.Latitude, _
                Key .PostalCode = venues.PostalCode, _
            Key .CreatedOn = Convert.ToDateTime(venues.CreatedOn), _
            Key .IsActive = True _
            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using
    End Sub

    Public Sub CreateClasses(classes As Classes) Implements IAdminClassRegistration.CreateClasses

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Classes(StartDateTime, Duration,Capacity,Weeks, Price, TermId,VenueId, CreatedOn, IsActive) " & "VALUES (@StartDateTime, @Duration,@Capacity,@Weeks, @Price, @TermId,@VenueId, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
            Key .StartDateTime = classes.StartDateTime, _
            Key .Duration = classes.Duration, _
                Key .Capacity = classes.Capacity, _
                Key .Weeks = classes.Weeks, _
            Key .Price = classes.Price, _
            Key .TermId = classes.TermId, _
                Key .VenueId = classes.VenueId, _
            Key .CreatedOn = Convert.ToDateTime(classes.CreatedOn), _
            Key .IsActive = True _
            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)

        End Using
    End Sub


    Public Function GetTermsList() As List(Of Terms) Implements IAdminClassRegistration.GetTermsList
        Dim CurrentDate As DateTime = DateTime.Today
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT TermId,Name from Terms where IsActive=1 "
            Return connection.Query(Of Terms)(query)
        End Using
    End Function

    Public Function GetTermDetailById(TermId As Integer) As Terms Implements IAdminClassRegistration.GetTermDetailById
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT TermId,Name,DiscountAmount,TotalAmount from Terms where IsActive=1 and TermId =" & TermId
            Return connection.Query(Of Terms)(query).SingleOrDefault()
        End Using
    End Function

    Public Function BindTerms() As List(Of Terms) Implements IAdminClassRegistration.BindTerms
        Dim CurrentDate As DateTime = DateTime.Today
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT TermId,Name,DiscountAmount,TotalAmount,CreatedOn,IsActive from Terms where IsActive=1 "
            Return connection.Query(Of Terms)(query)
        End Using
    End Function

    Public Sub DeleteTerms(TermId As Integer) Implements IAdminClassRegistration.DeleteTerms

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Update Terms set IsActive = 0 where TermId = " & TermId
            Dim rowsAffected As Integer = connection.Execute(query)
        End Using
    End Sub

    Public Sub UpdateTerms(terms As Terms) Implements IAdminClassRegistration.UpdateTerms

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "UPDATE Terms " + _
                                        "SET Name = @Name, " + _
                                        "DiscountAmount = @DiscountAmount, " + _
                                        "TotalAmount = @TotalAmount " + _
                                        "WHERE TermId = @TermId"
            Dim parameters = New With _
                             { _
                                .TermId = terms.TermId, _
                                .Name = terms.Name, _
                                .DiscountAmount = terms.DiscountAmount, _
                                 .TotalAmount = terms.TotalAmount _
                            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using
    End Sub

    Public Function BindVenues() As List(Of Venue) Implements IAdminClassRegistration.BindVenues
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT VenueId,Name,Logo,Day,Address1,Address2,Address3,City,Longitude,Latitude,PostalCode,CreatedOn from Venues where IsActive=1 "
            Return connection.Query(Of Venue)(query)
        End Using
    End Function

    Public Function GetVenueDetailById(VenueId As Integer) As Venue Implements IAdminClassRegistration.GetVenueDetailById
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT VenueId,Name,Logo,Day,Address1,Address2,Address3,City,Longitude,Latitude,PostalCode,CreatedOn from Venues where IsActive=1 and VenueId =" & VenueId
            Return connection.Query(Of Venue)(query).SingleOrDefault()
        End Using
    End Function

    Public Sub DeleteVenues(VenueId As Integer) Implements IAdminClassRegistration.DeleteVenues

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Update Venues set IsActive = 0 where VenueId = " & VenueId
            Dim rowsAffected As Integer = connection.Execute(query)
        End Using
    End Sub

    Public Sub UpdateVenues(venue As Venue) Implements IAdminClassRegistration.UpdateVenues

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "UPDATE Venues " + _
                                        "SET Name = @Name, " + _
                                         "Logo = @Logo, " + _
                                         "Day = @Day, " + _
                                        "Address1 = @Address1, " + _
                                        "Address2 = @Address2, " + _
                                        "Address3 = @Address3, " + _
                                         "City = @City, " + _
                                         "Longitude = @Longitude, " + _
                                         "Latitude = @Latitude, " + _
                                        "PostalCode = @PostalCode " + _
                                        "WHERE VenueId = @VenueId"
            Dim parameters = New With _
                             { _
                                .VenueId = venue.VenueId, _
                                .Name = venue.Name, _
                                .Day = venue.Day, _
                                .Logo = venue.Logo, _
                                .Address1 = venue.Address1, _
                                .Address2 = venue.Address2, _
                                .Address3 = venue.Address3, _
                                .City = venue.City, _
                                .Longitude = venue.Longitude, _
                                .Latitude = venue.Latitude, _
                                 .PostalCode = venue.PostalCode _
                            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using
    End Sub


    Public Function BindClasses() As List(Of Classes) Implements IAdminClassRegistration.BindClasses
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT ClassId,StartDateTime,Duration,Capacity,Weeks,Price,(select Name from Terms where Terms.TermId=Classes.TermId) as Term,TermId,(select Name from Venues where Venues.VenueId=Classes.VenueId) as Venue,VenueId from Classes where IsActive=1"
            Return connection.Query(Of Classes)(query)
        End Using
    End Function

    Public Function GetClassDetailById(ClassId As Integer) As Classes Implements IAdminClassRegistration.GetClassDetailById
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT ClassId,StartDateTime,Duration,Capacity,Weeks,Price,(select Name from Terms where Terms.TermId=Classes.TermId) as Term,TermId,(select Name from Venues where Venues.VenueId=Classes.VenueId) as Venue,VenueId from Classes where IsActive=1 and ClassId =" & ClassId
            Return connection.Query(Of Classes)(query).SingleOrDefault()
        End Using
    End Function

    Public Function GetClassDetailByTermId(TermId As Integer) As List(Of Classes) Implements IAdminClassRegistration.GetClassDetailByTermId
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Select * from Classes where IsActive=1 and TermId =" & TermId
            Return connection.Query(Of Classes)(query)
        End Using
    End Function

    Public Function GetClassDetailByVenueId(VenueId As Integer) As List(Of Classes) Implements IAdminClassRegistration.GetClassDetailByVenueId
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Select * from Classes where IsActive=1 and VenueId =" & VenueId
            Return connection.Query(Of Classes)(query)
        End Using
    End Function

    Public Sub DeleteClasses(ClassId As Integer) Implements IAdminClassRegistration.DeleteClasses

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Update Classes set IsActive = 0 where ClassId = " & ClassId
            Dim rowsAffected As Integer = connection.Execute(query)
        End Using
    End Sub

    Public Sub UpdateClasses(cls As Classes) Implements IAdminClassRegistration.UpdateClasses

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "UPDATE Classes " + _
                                        "SET StartDatetime = @StartDatetime, " + _
                                        "Duration = @Duration, " + _
                                        "Price = @Price, " + _
                                        "Capacity = @Capacity, " + _
                                        "Weeks = @Weeks, " + _
                                        "TermId = @TermId, " + _
                                         "VenueId = @VenueId " + _
                                        "WHERE ClassId = @ClassId"
            Dim parameters = New With _
                             { _
                                .ClassId = cls.ClassId, _
                                 .StartDateTime = cls.StartDateTime, _
                                .Duration = cls.Duration, _
                                .Price = cls.Price, _
                                .Capacity = cls.Capacity, _
                                .Weeks = cls.Weeks, _
                                .TermId = cls.TermId, _
                                .VenueId = cls.VenueId _
                            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using
    End Sub


    Public Function BindBookedClasses(OrderId As Integer) As List(Of OrderDetails) Implements IAdminClassRegistration.BindBookedClasses
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "select ClassBooking.OrderId,Family.FirstName,Family.LastName,Classes.StartDateTime,Classes.Duration,(Select Venues.Name FROM Venues Where Venues.VenueId=Classes.VenueId) as Term,(Select Terms.Name FROM Terms Where Terms.TermId=Classes.TermId) as Venue from ClassBooking join Classes on ClassBooking.ClassId=Classes.ClassId join Family on ClassBooking.ChildId=Family.Id where ClassBooking.OrderId= " & OrderId
            Return connection.Query(Of OrderDetails)(query)
        End Using
    End Function

    Public Function BindBookedOrders() As List(Of CreatedOrders) Implements IAdminClassRegistration.BindBookedOrders
        Dim flag = "Class"
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "select OrderId,OrderDate,(select UserName from aspnet_Users where aspnet_Users.UserId=Orders.UserId) as 'UserName' from Orders where IsActive=1 and Flag='" & flag & "'"
            Return connection.Query(Of CreatedOrders)(query)
        End Using
    End Function
End Class
