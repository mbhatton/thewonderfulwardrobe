﻿Imports WonderfulWardrobe.Model
Imports Dapper
Public Class AdminPaymentDetails
    Implements IAdminPaymentDetails
    Public Function BindPayments() As List(Of Payment) Implements IAdminPaymentDetails.BindPayments
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT PaymentId,OrderId,UserId,(select userName from aspnet_Users where aspnet_Users.UserId=Payment.UserId) as UserName,PaymentType,TotalAmount,PayAmount,PaymentFor,CreatedOn from Payment where IsActive=1 "
            Return connection.Query(Of Payment)(query)
        End Using
    End Function
End Class
