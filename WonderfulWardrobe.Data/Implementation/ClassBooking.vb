﻿Imports WonderfulWardrobe.Model
Imports Dapper
Imports System.Web.Security

Public Class ClassBooking
    Implements IClassBooking
    Public Function BindVenues() As List(Of Venue) Implements IClassBooking.BindVenues
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT VenueId,Name,Logo,Day,Address1,Address2,Address3,City,Longitude,Latitude,PostalCode,CreatedOn from Venues where IsActive=1 "
            Return connection.Query(Of Venue)(query)
        End Using
    End Function

    Public Function GetTermByVenueId(VenueId As Integer) As List(Of Terms) Implements IClassBooking.GetTermByVenueId
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim currentdate = DateTime.Today.ToString("yyyy-MM-dd")
            Dim query As String = "SELECT Distinct Classes.TermId,Terms.Name,Terms.DiscountAmount,Terms.TotalAmount from Classes join Terms on Classes.TermId=Terms.TermId where Classes.IsActive=1 and Classes.VenueId =" & VenueId & " and Classes.StartDateTime >'" & currentdate & "'"
            Return connection.Query(Of Terms)(query)
        End Using
    End Function

    Public Function GetClassByTermVenueId(TermId As Integer, VenueId As Integer) As List(Of Classes) Implements IClassBooking.GetClassByTermVenueId
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT ClassId,StartDateTime,Capacity,Weeks,Duration,Price,TermId,VenueId,CreatedOn from Classes where IsActive=1 and TermId =" & TermId & " and VenueId=" & VenueId
            Return connection.Query(Of Classes)(query)
        End Using
    End Function

    Public Function GetClassByClassId(ClassId As Integer) As ClassesBooking Implements IClassBooking.GetClassByClassId
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "select Classes.ClassId,Classes.StartDateTime,Classes.Duration,Classes.Price,Venues.VenueId,Venues.Day,Venues.Name as VenueName,Venues.Logo,Venues.Address1,Venues.Address2,Venues.Address3,Venues.City,Terms.TermId,Terms.Name as TermName,Terms.DiscountAmount from Classes join Venues on Classes.VenueId=Venues.VenueId join Terms on Classes.TermId=Terms.TermId where Classes.IsActive=1 and  Classes.ClassId=" & ClassId
            Return connection.Query(Of ClassesBooking)(query).SingleOrDefault()
        End Using
    End Function
    Public Function BindFamily(UserId As Guid) As List(Of Family) Implements IClassBooking.BindFamily
        Dim base As New BaseRepository()
        Dim _userId = UserId.ToString()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT Id,FirstName,LastName from Family where IsActive=1 and UserId ='" & _userId & "'"
            Return connection.Query(Of Family)(query)
        End Using
    End Function
    Public Function BindClassByTermVenue(termId As Integer, venueId As Integer) As List(Of Classes) Implements IClassBooking.BindClassByTermVenue
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT ClassId,StartDateTime,Capacity,Weeks,Duration,Price,TermId,VenueId,CreatedOn from Classes where IsActive=1 and TermId =" & termId & " and VenueId=" & venueId
            Return connection.Query(Of Classes)(query)
        End Using
    End Function
    Public Sub AddChild(family As Family) Implements IClassBooking.AddChild
        Dim user As MembershipUser
        user = Membership.GetUser()
        Dim userID As String = user.ProviderUserKey.ToString()
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Family(UserId, FirstName, LastName, DOB,Gender, CreatedOn, IsActive) " & "VALUES (@UserId, @FirstName, @LastName, @DOB,@Gender, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
         Key .UserId = userID, _
         Key .FirstName = family.FirstName, _
         Key .LastName = family.LastName, _
         Key .DOB = family.DOB, _
         Key .Gender = family.Gender, _
         Key .CreatedOn = family.CreatedOn, _
         Key .IsActive = family.IsActive _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)

        End Using

    End Sub
    Public Function BindVenueById(VenueId As Integer) As Venue Implements IClassBooking.BindVenueById
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT VenueId,Name,Logo,Day,Address1,Address2,Address3,City,Longitude,Latitude,PostalCode,CreatedOn from Venues where IsActive=1 and VenueId=" & VenueId
            Return connection.Query(Of Venue)(query).SingleOrDefault()
        End Using
    End Function

    Public Function BindTermbyId(TermId As Integer) As Terms Implements IClassBooking.BindTermbyId
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT TermId,Name,DiscountAmount,TotalAmount,CreatedOn from Terms where IsActive=1 and TermId=" & TermId
            Return connection.Query(Of Terms)(query).SingleOrDefault()
        End Using
    End Function


    Function SaveOrder(_order As Order) As String Implements IClassBooking.SaveOrder
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Orders(OrderDate, UserId,Flag, CreatedOn, IsActive) " & "VALUES (@OrderDate, @UserId,@Flag, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
         Key .UserId = _order.UserId, _
             Key .Flag = "Class", _
         Key .OrderDate = _order.OrderDate, _
         Key .CreatedOn = DateTime.Today, _
         Key .IsActive = True _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
            Dim identity As Object = _
        connection.Query("SELECT @@IDENTITY AS Id").SingleOrDefault()
            Dim newId As String = CType(identity.Id, String)
            Return newId
        End Using
    End Function

    Sub SaveBookedClass(_cls As BookedClass) Implements IClassBooking.SaveBookedClass
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO ClassBooking(ClassId,ChildId,OrderId,CreatedOn) " & "VALUES ( @ClassId, @ChildId,@OrderId, @CreatedOn)"
            Dim parameters = New With { _
         Key .BookingId = _cls.BookingId, _
         Key .ClassId = _cls.ClassId, _
         Key .ChildId = _cls.ChildId, _
             Key .OrderId = _cls.OrderId, _
         Key .CreatedOn = DateTime.Today _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)

        End Using
    End Sub

    Sub SavePayment(_payment As Payment) Implements IClassBooking.SavePayment
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Payment(OrderId,UserId,TransactionId,PaymentType,TotalAmount,PayAmount,PaymentFor,CreatedOn,IsActive) " & "VALUES (@OrderId,@UserId,@TransactionId,@PaymentType,@TotalAmount,@PayAmount,@PaymentFor,@CreatedOn,@IsActive)"
            Dim parameters = New With { _
         Key .OrderId = _payment.OrderId, _
         Key .UserId = _payment.UserId, _
              Key .TransactionId = _payment.TransactionId, _
         Key .PaymentType = _payment.PaymentType, _
              Key .TotalAmount = _payment.TotalAmount, _
 Key .PayAmount = _payment.PayAmount, _
              Key .PaymentFor = _payment.PaymentFor, _
              Key .CreatedOn = _payment.CreatedOn, _
             Key .IsActive = _payment.IsActive _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)

        End Using
    End Sub
End Class
