﻿Imports WonderfulWardrobe.Model
Imports Dapper

Public Class NewsLetters
    Implements INewsLetters
    Public Function BindNewsLettersUser() As List(Of Model.NewsLetter) Implements INewsLetters.BindNewsLettersUser
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT Id,Name,EmailId,CreatedOn from NewsLetterSubscribers where IsActive=1 "
            Return connection.Query(Of Model.NewsLetter)(query)
        End Using
    End Function

    Public Sub RegisterNewsLetter(_newsletter As NewsLetter) Implements INewsLetters.RegisterNewsLetter
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO NewsLetterSubscribers(Name, EmailId, CreatedOn, IsActive) " & "VALUES (@Name, @EmailId, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
             Key .EmailId = _newsletter.EmailId, _
             Key .Name = _newsletter.Name, _
             Key .CreatedOn = _newsletter.CreatedOn, _
             Key .IsActive = _newsletter.IsActive _
            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using

    End Sub

    Public Sub DeleteUser(Id As Integer) Implements INewsLetters.DeleteUser
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Update NewsLetterSubscribers set IsActive = 0 where Id = " & Id
            Dim rowsAffected As Integer = connection.Execute(query)
        End Using
    End Sub

    Public Sub SaveNewsLetter(_newsletter As NewsLetterDetail) Implements INewsLetters.SaveNewsLetter
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO NewsLetter(Title, Description,CreatedBy,IsPublish, CreatedOn, IsActive) " & "VALUES (@Title, @Description,@CreatedBy,@IsPublish, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
            Key .CreatedBy = _newsletter.CreatedBy, _
             Key .Title = _newsletter.Title, _
             Key .Description = _newsletter.content, _
                 Key .IsPublish = _newsletter.IsPublish, _
             Key .CreatedOn = _newsletter.CreatedOn, _
             Key .IsActive = _newsletter.IsActive _
            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using

    End Sub

    Public Sub UpdateNewsLetter(_newsletter As NewsLetterDetail, Id As Integer) Implements INewsLetters.UpdateNewsLetter
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "UPDATE NewsLetter " + _
                                        "SET Title = @Title, " + _
                                        "Description = @Description, " + _
                                        "CreatedBy = @CreatedBy," + _
                                         "IsPublish = @IsPublish, " + _
                                          "CreatedOn = @CreatedOn, " + _
                                           "IsActive = @IsActive " + _
                                        "WHERE Id = @Id"
            Dim parameters = New With { _
                Key .Id = Id, _
            Key .CreatedBy = _newsletter.CreatedBy, _
             Key .Title = _newsletter.Title, _
             Key .Description = _newsletter.content, _
                 Key .IsPublish = _newsletter.IsPublish, _
             Key .CreatedOn = _newsletter.CreatedOn, _
             Key .IsActive = _newsletter.IsActive _
            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using
       

    End Sub

    Public Function BindNewsLettersList() As List(Of Model.NewsLetterDetail) Implements INewsLetters.BindNewsLettersList
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT Id,Title,Description as content,CreatedOn,CreatedBy,IsPublish,IsActive from NewsLetter where IsActive=1 "
            Return connection.Query(Of Model.NewsLetterDetail)(query)
        End Using
    End Function

    Public Sub DeleteNewsLetter(Id As Integer) Implements INewsLetters.DeleteNewsLetter
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Update NewsLetter set IsActive = 0 where Id = " & Id
            Dim rowsAffected As Integer = connection.Execute(query)
        End Using
    End Sub

    Public Function GetNewsLetterDetail(Id As Integer) As Model.NewsLetterDetail Implements INewsLetters.GetNewsLetterDetail
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT Id,Title,Description as content,CreatedOn,CreatedBy,IsPublish,IsActive from NewsLetter where IsActive=1 and Id= " & Id
            Return connection.Query(Of Model.NewsLetterDetail)(query).SingleOrDefault()
        End Using
    End Function

    Public Sub UpdateNewsLetterTable(Id As Integer) Implements INewsLetters.UpdateNewsLetterTable
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Update NewsLetter set IsPublish = 1 where Id = " & Id
            Dim rowsAffected As Integer = connection.Execute(query)
        End Using
    End Sub
End Class
