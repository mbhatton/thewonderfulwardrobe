﻿Imports System.Web.Security
Imports WonderfulWardrobe.Model
Imports Dapper
Imports System.Web
Imports System.Security.Principal
Imports System.Xml

Public Class AccountRegister
    Implements IAccountRegister

    Public Function Register(parent As Parent) As MembershipCreateStatus Implements IAccountRegister.Register
        Dim user As MembershipUser
        Dim status As MembershipCreateStatus
        user = Membership.CreateUser(parent.EmailId, parent.Password, parent.EmailId, "Password Hint", parent.PasswordHint, True, _
    status)
        Return status
    End Function

    Public Sub SaveParentDetails(parentDetails As Parent) Implements IAccountRegister.SaveParentDetails
        Dim user As MembershipUser
        user = Membership.GetUser()
        Dim userID As String = user.ProviderUserKey.ToString()
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Parent(UserId, FirstName, LastName, EmailId, CreatedOn, IsActive) " & "VALUES (@UserId, @FirstName, @LastName, @EmailId, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
             Key .UserId = userID, _
             Key .FirstName = parentDetails.FirstName, _
             Key .LastName = parentDetails.LastName, _
             Key .EmailId = parentDetails.EmailId, _
             Key .CreatedOn = parentDetails.CreatedOn, _
             Key .IsActive = parentDetails.IsActive _
            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using

    End Sub
    Public Sub SaveFamilyDetails(familyDetails As List(Of Family)) Implements IAccountRegister.SaveFamilyDetails
        Dim user As MembershipUser
        user = Membership.GetUser()
        Dim userID As String = user.ProviderUserKey.ToString()
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Family(UserId, FirstName, LastName, DOB,Gender, CreatedOn, IsActive) " & "VALUES (@UserId, @FirstName, @LastName, @DOB,@Gender, @CreatedOn, @IsActive)"
            For Each item As Family In familyDetails
                Dim parameters = New With { _
             Key .UserId = userID, _
             Key .FirstName = item.FirstName, _
             Key .LastName = item.LastName, _
             Key .DOB = item.DOB, _
             Key .Gender = item.Gender, _
             Key .CreatedOn = item.CreatedOn, _
             Key .IsActive = item.IsActive _
            }
                Dim rowsAffected As Integer = connection.Execute(query, parameters)
            Next
        End Using

    End Sub
    Public Function Login(parentDetails As Parent) As Boolean Implements IAccountRegister.Login

        Dim Status As Boolean = Membership.ValidateUser(parentDetails.EmailId, parentDetails.Password)
        Return Status

    End Function

    Public Function SetAuthCookieMain(userName As String) As String Implements IAccountRegister.SetAuthCookieMain
        Dim userData As String = String.Join("|", Roles.GetRolesForUser(userName))
        Dim ticket As New FormsAuthenticationTicket(1, userName, DateTime.Now, DateTime.Now.AddMinutes(30), True, userData, _
            FormsAuthentication.FormsCookiePath)
        ' the path for the cookie
        ' Encrypt the ticket using the machine key
        Dim encryptedTicket As String = FormsAuthentication.Encrypt(ticket)

        ' Add the cookie to the request to save it
        Dim cookie As New HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
        cookie.HttpOnly = True
        HttpContext.Current.Response.Cookies.Add(cookie)
        Dim Role As String = Roles.GetRolesForUser(userName)(0).ToString()
        Return Role
    End Function

    Public Sub CreateUserRole(emailId As String) Implements IAccountRegister.CreateUserRole
        Dim user As MembershipUser
        user = Membership.GetUser()
        Dim role() = Roles.GetRolesForUser()
        If role.Length > 0 Then
            If role(0) = "SuperAdmin" Then
                Roles.AddUserToRole(emailId, "Admin")
            Else
                Roles.AddUserToRole(emailId, "User")
                Dim identity As New GenericIdentity(emailId)
                Dim principal As New RolePrincipal(identity)
                System.Threading.Thread.CurrentPrincipal = principal
                HttpContext.Current.User = principal
            End If
        Else
            Roles.AddUserToRole(emailId, "User")
            Dim identity As New GenericIdentity(emailId)
            Dim principal As New RolePrincipal(identity)
            System.Threading.Thread.CurrentPrincipal = principal
            HttpContext.Current.User = principal
        End If

        

    End Sub

    Public Function BindAdminList() As List(Of Users) Implements IAccountRegister.BindAdminList
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT aspnet_Users.UserId,aspnet_Users.UserName as FirstName,aspnet_Users.UserName as EmailId from aspnet_Users join aspnet_UsersInRoles on aspnet_Users.UserId=aspnet_UsersInRoles.UserId where aspnet_UsersInRoles.RoleId='2ae6d10e-02fd-4d3b-a1a0-7d98250bfdac'"
            Return connection.Query(Of Users)(query)
        End Using
    End Function

    
End Class

