﻿Imports WonderfulWardrobe.Model
Imports Dapper

Public Class Galeries
    Implements IGaleries
    Public Function BindGallery() As List(Of Gallery) Implements IGaleries.BindGallery
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT Id,ImageName,Location,CreatedOn from Gallery where IsActive=1 "
            Return connection.Query(Of Gallery)(query)
        End Using
    End Function

    Public Sub UploadImage(gallery As Gallery) Implements IGaleries.UploadImage

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Gallery(ImageName, Location,CreatedOn, IsActive) " & "VALUES (@ImageName, @Location, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
            Key .ImageName = gallery.ImageName, _
            Key .Location = gallery.Location, _
            Key .CreatedOn = gallery.CreatedOn, _
            Key .IsActive = gallery.IsActive _
            }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using
    End Sub

    Public Sub DeleteImage(Id As Integer) Implements IGaleries.DeleteImage

        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Update Gallery set IsActive = 0 where Id = " & Id
            Dim rowsAffected As Integer = connection.Execute(query)
        End Using
    End Sub
End Class
