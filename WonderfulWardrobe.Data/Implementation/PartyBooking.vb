﻿Imports WonderfulWardrobe.Model
Imports Dapper
Imports System.Web.Security

Public Class PartyBooking
    Implements IPartyBooking
    Public Sub CreatePartyEnquiry(_enquiry As PartyEnquiry) Implements IPartyBooking.CreatePartyEnquiry
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "INSERT INTO PartyEnquiry(Name, Email, PhoneNumber, PreferedDate1,PreferedDate2,PreferedStartTime,Duration, CreatedOn,IsProcessed, IsActive) " & "VALUES (@Name, @Email, @PhoneNumber, @PreferedDate1,@PreferedDate2,@PreferedStartTime,@Duration, @CreatedOn,@IsProcessed, @IsActive)"
            Dim parameters = New With { _
         Key .Name = _enquiry.Name, _
         Key .Email = _enquiry.Email, _
         Key .PhoneNumber = _enquiry.PhoneNumber, _
         Key .PreferedDate1 = _enquiry.PreferedDate1, _
              Key .PreferedDate2 = _enquiry.PreferedDate2, _
              Key .PreferedStartTime = _enquiry.PreferedStartTime, _
              Key .Duration = _enquiry.Duration, _
         Key .CreatedOn = _enquiry.CreatedOn, _
              Key .IsProcessed = False, _
         Key .IsActive = _enquiry.IsActive _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)

        End Using
    End Sub

    Public Function BindPartyEnquiryList() As List(Of PartyEnquiry) Implements IPartyBooking.BindPartyEnquiryList
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT EnquiryId,Name,Email,PhoneNumber,PreferedDate1,PreferedDate2,PreferedStarttime,Duration,IsActive from PartyEnquiry where IsActive=1 and IsProcessed=0"
            Return connection.Query(Of PartyEnquiry)(query)
        End Using
    End Function

    Public Function EnquiryDetails(enquiryId As Integer) As PartyEnquiry Implements IPartyBooking.EnquiryDetails
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "select EnquiryId,Name,Email,PhoneNumber,PreferedDate1,PreferedDate2,PreferedStarttime,Duration,IsActive from PartyEnquiry where IsActive=1 and EnquiryId= " & enquiryId
            Return connection.Query(Of PartyEnquiry)(query).SingleOrDefault()
        End Using
    End Function

    Public Sub SaveSuggestion(partyenquiry As List(Of Suggstion), enquiryid As Integer) Implements IPartyBooking.SaveSuggestion
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "INSERT INTO Suggestion(EnquiryId, SuggestedDate, SuggestedTime, SuggestedDuration,Price,RequiredAmount, CreatedOn, IsActive) " & "VALUES (@EnquiryId, @SuggestedDate, @SuggestedTime, @SuggestedDuration,@Price,@RequiredAmount, @CreatedOn, @IsActive)"
            For Each item As Suggstion In partyenquiry
                Dim parameters = New With { _
                     Key .EnquiryId = enquiryid, _
             Key .SuggestedDate = item.SuggestedDate, _
             Key .SuggestedTime = item.SuggestedTime, _
             Key .SuggestedDuration = item.SuggestedDuration, _
             Key .Price = item.SuggestedPrice, _
                         Key .RequiredAmount = item.RequiredAmount, _
             Key .CreatedOn = item.CreatedOn, _
             Key .IsActive = item.IsActive _
            }
                Dim rowsAffected As Integer = connection.Execute(query, parameters)
            Next
        End Using
    End Sub
    Public Sub UpdateEnquiry(enquiryid As Integer) Implements IPartyBooking.UpdateEnquiry
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "Update PartyEnquiry set IsProcessed=1 where EnquiryId= " & enquiryid

            Dim parameters = New With { _
         Key .EnquiryId = enquiryid _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using
    End Sub

    Public Function BindprocessedEnquiryList() As List(Of PartyEnquiry) Implements IPartyBooking.BindprocessedEnquiryList
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT EnquiryId,Name,Email,PhoneNumber,PreferedDate1,PreferedDate2,PreferedStarttime,Duration,IsActive from PartyEnquiry where IsActive=1 and IsProcessed=1"
            Return connection.Query(Of PartyEnquiry)(query)
        End Using
    End Function

    Public Function ProcessedEnquiryDetails(EnquiryId As Integer) As List(Of Suggstion) Implements IPartyBooking.ProcessedEnquiryDetails
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT SuggestionId,EnquiryId,SuggestedDate,SuggestedTime,SuggestedDuration,Price,RequiredAmount,IsActive from Suggestion where IsActive=1 and EnquiryId= " & EnquiryId
            Return connection.Query(Of Suggstion)(query)
        End Using
    End Function

    Function SaveOrder(_order As Order) As String Implements IPartyBooking.SaveOrder
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Orders(OrderDate, UserId,Flag, CreatedOn, IsActive) " & "VALUES (@OrderDate, @UserId,@Flag, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
         Key .UserId = _order.UserId, _
              Key .Flag = "Party", _
         Key .OrderDate = _order.OrderDate, _
         Key .CreatedOn = DateTime.Today, _
         Key .IsActive = True _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
            Dim identity As Object = _
        connection.Query("SELECT @@IDENTITY AS Id").SingleOrDefault()
            Dim newId As String = CType(identity.Id, String)
            Return newId
        End Using
    End Function


    Sub SavePayment(_payment As Payment) Implements IPartyBooking.SavePayment
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Payment(OrderId,UserId,TransactionId,PaymentType,TotalAmount,PayAmount,PaymentFor,CreatedOn,IsActive) " & "VALUES (@OrderId,@UserId,@TransactionId,@PaymentType,@TotalAmount,@PayAmount,@PaymentFor,@CreatedOn,@IsActive)"
            Dim parameters = New With { _
         Key .OrderId = _payment.OrderId, _
         Key .UserId = _payment.UserId, _
              Key .TransactionId = _payment.TransactionId, _
         Key .PaymentType = _payment.PaymentType, _
              Key .TotalAmount = _payment.TotalAmount, _
 Key .PayAmount = _payment.PayAmount, _
              Key .PaymentFor = _payment.PaymentFor, _
              Key .CreatedOn = _payment.CreatedOn, _
             Key .IsActive = _payment.IsActive _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)

        End Using
    End Sub

    Sub SaveHostDetails(_host As PartyHostDetails) Implements IPartyBooking.SaveHostDetails
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO PartyHostDetails(HostId,FirstName,LastName,Address1,Address2,Address3,PostalCode,TelePhone,CreatedOn,IsActive) " & "VALUES (@HostId,@FirstName,@LastName,@Address1,@Address2,@Address3,@PostalCode,@TelePhone,@CreatedOn,@IsActive)"
            Dim parameters = New With { _
         Key .HostId = _host.HostId, _
         Key .FirstName = _host.FirstName, _
              Key .LastName = _host.LastName, _
         Key .Address1 = _host.Address1, _
              Key .Address2 = _host.Address2, _
 Key .Address3 = _host.Address3, _
              Key .PostalCode = _host.PostalCode, _
              Key .TelePhone = _host.TelePhone, _
              Key .CreatedOn = DateTime.Today, _
             Key .IsActive = True _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)

        End Using
    End Sub

    Public Function SavePartyChilds(familyDetails As Family) As String Implements IPartyBooking.SavePartyChilds
        Dim user As MembershipUser
        user = Membership.GetUser()
        Dim userID As String = user.ProviderUserKey.ToString()
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Family(UserId, FirstName, LastName, DOB,Gender, CreatedOn, IsActive) " & "VALUES (@UserId, @FirstName, @LastName, @DOB,@Gender, @CreatedOn, @IsActive)"

            Dim parameters = New With { _
         Key .UserId = userID, _
         Key .FirstName = familyDetails.FirstName, _
         Key .LastName = familyDetails.LastName, _
         Key .DOB = familyDetails.DOB, _
         Key .Gender = familyDetails.Gender, _
         Key .CreatedOn = familyDetails.CreatedOn, _
         Key .IsActive = familyDetails.IsActive _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
            Dim identity As Object = _
        connection.Query("SELECT @@IDENTITY AS Id").SingleOrDefault()
            Dim newId As String = CType(identity.Id, String)
            Return newId
        End Using

    End Function

    Sub SavePartyChildren(_partyChild As PartyChildrenDetails) Implements IPartyBooking.SavePartyChildren
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO PartyChildren(ChildId,OrderId) " & "VALUES (@ChildId,@OrderId)"
            Dim parameters = New With { _
         Key .ChildId = _partyChild.ChildId, _
             Key .OrderId = _partyChild.OrderId _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
        End Using
    End Sub

    Public Sub SaveParty(_party As List(Of Party)) Implements IPartyBooking.SaveParty
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "INSERT INTO Party(OrderId,HostId, Duration, Time, PartyDate,Price,Location,Reason,NoOfChildren, CreatedOn, IsActive) " & "VALUES (@OrderId,@HostId,@Duration, @Time, @PartyDate,@Price,@Location,@Reason,@NoOfChildren, @CreatedOn, @IsActive)"
            For Each item As Party In _party
                Dim parameters = New With { _
                     Key .OrderId = item.OrderId, _
             Key .HostId = item.HostId, _
             Key .Duration = item.Duration, _
             Key .Time = item.Time, _
             Key .PartyDate = item.PartyDate, _
                         Key .Price = item.Price, _
                         Key .Location = item.Location, _
                         Key .Reason = item.Reason, _
                         Key .NoOfChildren = item.NoOfChildren, _
             Key .CreatedOn = item.CreatedOn, _
             Key .IsActive = item.IsActive _
            }
                Dim rowsAffected As Integer = connection.Execute(query, parameters)
            Next
        End Using
    End Sub

    Public Function BindBookedPartyList() As List(Of Party) Implements IPartyBooking.BindBookedPartyList
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT PartyId,OrderId,HostId,(select UserName from aspnet_Users where UserId=Party.HostId) as UserName,Duration,Time,PartyDate,Price,Location,Reason,NoOfChildren,(select sum(Payment.PayAmount) from Payment where Payment.OrderId=Party.OrderId) as PaidAmount,IsActive from Party where IsActive=1"
            Return connection.Query(Of Party)(query)
        End Using
    End Function

    Public Function PartyDetails(partyId As Integer) As Party Implements IPartyBooking.PartyDetails
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT PartyId,OrderId,HostId,(select UserName from aspnet_Users where UserId=Party.HostId) as UserName,Duration,Time,PartyDate,Price,Location,Reason,NoOfChildren,(select sum(Payment.PayAmount) from Payment where Payment.OrderId=Party.OrderId) as PaidAmount,IsActive from Party where IsActive=1 and PartyId= " & partyId
            Return connection.Query(Of Party)(query).SingleOrDefault()
        End Using
    End Function

    Public Function partyChildren(OrderId As Integer) As List(Of PartyChildrenDetails) Implements IPartyBooking.partyChildren
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT Id,OrderId,ChildId from PartyChildren where OrderId= " & OrderId
            Return connection.Query(Of PartyChildrenDetails)(query)
        End Using
    End Function

    Public Function ChildDetail(childId As Integer) As Family Implements IPartyBooking.ChildDetails
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT Id,FirstName,LastName,DOB,Gender from Family where IsActive=1 and Id= " & childId
            Return connection.Query(Of Family)(query).SingleOrDefault()
        End Using
    End Function

    Public Function HostDetails(hostId As Guid) As PartyHostDetails Implements IPartyBooking.HostDetails
        Dim base As New BaseRepository()
        Dim Id As String = hostId.ToString()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT FirstName,LastName,Address1,Address2,Address3,TelePhone from PartyHostDetails where IsActive=1 and HostId= '" & Id & "'"
            Return connection.Query(Of PartyHostDetails)(query).FirstOrDefault()
        End Using
    End Function

    Public Function AddChild(family As Family) As String Implements IPartyBooking.AddChild
        Dim user As MembershipUser
        user = Membership.GetUser()
        Dim userID As String = user.ProviderUserKey.ToString()
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "INSERT INTO Family(UserId, FirstName, LastName, DOB,Gender, CreatedOn, IsActive) " & "VALUES (@UserId, @FirstName, @LastName, @DOB,@Gender, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
         Key .UserId = userID, _
         Key .FirstName = family.FirstName, _
         Key .LastName = family.LastName, _
         Key .DOB = family.DOB, _
         Key .Gender = family.Gender, _
         Key .CreatedOn = family.CreatedOn, _
         Key .IsActive = family.IsActive _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)
            Dim identity As Object = _
        connection.Query("SELECT @@IDENTITY AS Id").SingleOrDefault()
            Dim newId As String = CType(identity.Id, String)
            Return newId

        End Using

    End Function

    Public Function BindFamily(UserId As Guid) As List(Of Family) Implements IPartyBooking.BindFamily
        Dim base As New BaseRepository()
        Dim _userId = UserId.ToString()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT Id,FirstName,LastName,DOB,Gender from Family where IsActive=1 and UserId ='" & _userId & "'"
            Return connection.Query(Of Family)(query)
        End Using
    End Function

    Public Function GetChildInfo(childId As String) As Family Implements IPartyBooking.GetChildInfo
        Dim base As New BaseRepository()
        Dim _childId = Convert.ToInt32(childId)
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT Id,FirstName,LastName,DOB,Gender from Family where IsActive=1 and Id =" & _childId
            Return connection.Query(Of Family)(query).SingleOrDefault()
        End Using
    End Function
End Class
