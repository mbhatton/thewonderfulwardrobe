﻿Imports WonderfulWardrobe.Model
Imports Dapper
Public Class ContactUs
    Implements IContactUs

    Public Sub CreateContact(_contact As Contacts) Implements IContactUs.CreateContact
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "INSERT INTO Contact(Name, Email, TelePhone, Message, CreatedOn, IsActive) " & "VALUES (@Name, @Email, @TelePhone, @Message, @CreatedOn, @IsActive)"
            Dim parameters = New With { _
         Key .Name = _contact.Name, _
         Key .Email = _contact.Email, _
         Key .TelePhone = _contact.TelePhone, _
         Key .Message = _contact.Message, _
         Key .CreatedOn = _contact.CreatedOn, _
         Key .IsActive = _contact.IsActive _
        }
            Dim rowsAffected As Integer = connection.Execute(query, parameters)

        End Using
    End Sub

    Public Function BindContactsList() As List(Of Contacts) Implements IContactUs.BindContactsList
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Const query As String = "SELECT ContactId,Name,Email,TelePhone,Message,IsActive from Contact where IsActive=1 "
            Return connection.Query(Of Contacts)(query)
        End Using
    End Function

    Public Function ContactInfo(contactId As Integer) As Contacts Implements IContactUs.ContactInfo
        Dim base As New BaseRepository()
        Using connection As IDbConnection = base.OpenConnection()
            Dim query As String = "SELECT ContactId,Name,Email,TelePhone,Message,IsActive from Contact where IsActive=1 and ContactId=" & contactId
            Return connection.Query(Of Contacts)(query).SingleOrDefault()
        End Using
    End Function
End Class
