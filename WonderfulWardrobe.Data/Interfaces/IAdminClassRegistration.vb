﻿Imports WonderfulWardrobe.Model
Imports System.Web.Security
Public Interface IAdminClassRegistration
    Sub CreateTerms(ByVal _terms As Terms)
    Sub CreateVenues(ByVal _venues As Venue)
    Sub CreateClasses(ByVal _classes As Classes)
    Function GetTermsList() As List(Of Terms)
    Function GetTermDetailById(TermId As Integer) As Terms
    Function BindTerms() As List(Of Terms)

    Sub DeleteTerms(ByVal TermId As Integer)
    Sub UpdateTerms(ByVal _terms As Terms)
    Function BindVenues() As List(Of Venue)
    Sub DeleteVenues(ByVal VenueId As Integer)
    Sub UpdateVenues(ByVal _venue As Venue)
    Function GetVenueDetailById(VenueId As Integer) As Venue
    Function BindClasses() As List(Of Classes)
    Sub DeleteClasses(ByVal ClassId As Integer)
    Sub UpdateClasses(ByVal _class As Classes)
    Function GetClassDetailById(ClassId As Integer) As Classes
    Function GetClassDetailByTermId(TermId As Integer) As List(Of Classes)
    Function GetClassDetailByVenueId(Venue As Integer) As List(Of Classes)
    Function BindBookedClasses(OrderId As Integer) As List(Of OrderDetails)
    Function BindBookedOrders() As List(Of CreatedOrders)
End Interface
