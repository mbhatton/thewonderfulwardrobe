﻿Imports WonderfulWardrobe.Model
Imports System.Web.Security

Public Interface IAccountRegister
    Function Register(ByVal parent As Parent) As MembershipCreateStatus
    Sub SaveParentDetails(ByVal parentDetails As Parent)
    Sub SaveFamilyDetails(ByVal familyDetails As List(Of Family))
    Function Login(ByVal parent As Parent) As Boolean
    Function SetAuthCookieMain(ByVal userName As String) As String
    Sub CreateUserRole(ByVal emailId As String)
    Function BindAdminList() As List(Of Users)

End Interface
