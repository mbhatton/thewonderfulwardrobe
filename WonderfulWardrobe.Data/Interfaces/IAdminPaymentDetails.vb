﻿Imports WonderfulWardrobe.Model

Public Interface IAdminPaymentDetails
    Function BindPayments() As List(Of Payment)
End Interface
