﻿Public Interface INewsLetters
    Function BindNewsLettersUser() As List(Of Model.NewsLetter)
    Sub RegisterNewsLetter(newsletter As Model.NewsLetter)
    Sub DeleteUser(Id As Integer)
    Sub SaveNewsLetter(newsletter As Model.NewsLetterDetail)
    Function BindNewsLettersList() As List(Of Model.NewsLetterDetail)
    Sub DeleteNewsLetter(Id As Integer)
    Function GetNewsLetterDetail(Id As Integer) As Model.NewsLetterDetail
    Sub UpdateNewsLetter(newsletter As Model.NewsLetterDetail, Id As Integer)

    Sub UpdateNewsLetterTable(p1 As Integer)

End Interface
