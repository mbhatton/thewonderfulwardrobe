﻿Imports WonderfulWardrobe.Model

Public Interface IPartyBooking
    Sub CreatePartyEnquiry(ByVal _partyenquiry As PartyEnquiry)

    Function BindPartyEnquiryList() As List(Of PartyEnquiry)

    Function EnquiryDetails(enquiryId As Integer) As PartyEnquiry

    Sub SaveSuggestion(partyenquiry As List(Of Suggstion), enquiryid As Integer)

    Sub UpdateEnquiry(enquiryid As Integer)

    Function BindprocessedEnquiryList() As List(Of PartyEnquiry)

    Function ProcessedEnquiryDetails(enquiryId As Integer) As List(Of Suggstion)

    Function SaveOrder(Order As Order) As String

    Sub SavePayment(Payment As Payment)

    Sub SaveHostDetails(host As PartyHostDetails)

    Function SavePartyChilds(family As Family) As String

    Sub SaveParty(partyList As List(Of Party))

    Sub SavePartyChildren(partychilds As PartyChildrenDetails)

    Function BindBookedPartyList() As List(Of Party)

    Function PartyDetails(partyId As Integer) As Party

    Function partyChildren(orderId As Integer) As List(Of PartyChildrenDetails)

    Function ChildDetails(childId As Integer) As Family

    Function HostDetails(guid As Guid) As PartyHostDetails

    Function AddChild(family As Family) As String

    Function BindFamily(userId As Guid) As List(Of Family)

    Function GetChildInfo(childId As String) As Family

End Interface
