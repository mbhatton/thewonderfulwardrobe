﻿Imports WonderfulWardrobe.Model


Public Interface IContactUs
    Sub CreateContact(ByVal _contact As Contacts)
    Function BindContactsList() As List(Of Contacts)
    Function ContactInfo(contactId As Integer) As Contacts
End Interface
