﻿Imports WonderfulWardrobe.Model

Public Interface IClassBooking
    Function BindVenues() As List(Of Venue)
    Function GetTermByVenueId(VenueId As Integer) As List(Of Terms)
    Function GetClassByTermVenueId(TermId As Integer, VenueId As Integer) As List(Of Classes)
    Function GetClassByClassId(ClassId As Integer) As ClassesBooking
    Function BindFamily(UserId As Guid) As List(Of Family)
    Function BindClassByTermVenue(termId As Integer, venueId As Integer) As List(Of Classes)
    Sub AddChild(family As Family)
    Function BindVenueById(VenueId As Integer) As Venue
    Function BindTermbyId(TermId As Integer) As Terms
    Function SaveOrder(order As Order) As String
    Sub SaveBookedClass(cls As BookedClass)
    Sub SavePayment(payment As Payment)
End Interface
