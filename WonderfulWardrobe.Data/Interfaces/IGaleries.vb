﻿Imports WonderfulWardrobe.Model

Public Interface IGaleries

    Function BindGallery() As List(Of Gallery)
    Sub UploadImage(gallery As Gallery)
    Sub DeleteImage(Id As Integer)

End Interface
